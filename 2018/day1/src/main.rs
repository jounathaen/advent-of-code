use std::fs::File;
use std::collections::HashSet;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};

fn read_freq<R: Read>(io: R) -> Result<Vec<i64>, Error> {
    let br = BufReader::new(io);
    let mut v = vec![];
    for line in br.lines() {
        v.push(line?.trim().parse().map_err(|e| Error::new(ErrorKind::InvalidData, e))?);
    }
    Ok(v)
}


fn get_first_repeat(v: &Vec<i64>) -> Result<i64, Error> {
    let mut val_set = HashSet::new();
    let mut i = 0;
    let mut new_val = 0;
    let l = v.len();
    loop {
        new_val = new_val + v[i % l];
        // println!{"New val {} index {}", new_val, i};
        if val_set.contains(&new_val) {
            return Ok(new_val as i64)
        }

        val_set.insert(new_val);
        i = i + 1;
    }
}


fn main() -> Result<(), Error> {
    let inputvec : Vec<i64> = read_freq(File::open("input.txt")?)?;
    let sum : i64 = inputvec.iter().sum();
    let index = get_first_repeat(&inputvec)?;
    println!("Sum: {}, Repeating freq: {}", sum, index);
    Ok(())
}
