extern crate regex;
extern crate termion;
use self::termion::color;

use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Error;

use regex::Regex;

#[derive(Default, Debug, Clone, PartialEq)]
struct Claim {
    id: usize,
    x: usize,
    y: usize,
    w: usize,
    h: usize
}

type Fabric = [[usize ; 1000] ; 1000];


fn main() -> Result<(), Error> {
    let file = File::open("input.txt")?;

    let mut f : Fabric = [[0; 1000]; 1000];
    let claim_vec = parse_claims(&file)?;
    // println!("claim_vec: {:?}", claim_vec);
    for c in claim_vec.clone() {
        apply_claim(c, &mut f);
    }
    print_fabric(&f);
    println!{"Overlapping parts: {}", count_overlap(&f)?};
    let c = search_valid_claim(&f, &claim_vec)?;
    println!("valid claim: {} at x: {} y: {}", c.id, c.x, c.y );
    Ok(())
}


fn parse_claims(f: &File) -> Result<Vec<Claim>, Error> {
    let bufread = BufReader::new(f);
    let mut claim_vec : Vec<Claim> = Vec::new();
    let re = Regex::new(r"#(\d+) @ (\d{1,4}),(\d{1,4}): (\d{1,4})x(\d{1,4})").unwrap();

    for line in bufread.lines() {
        let l = line?;
        let caps = re.captures(&l).unwrap();
        let c = Claim{ 
            id: caps.get(1).unwrap().as_str().parse().unwrap(),
            x: caps.get(2).unwrap().as_str().parse().unwrap(),
            y: caps.get(3).unwrap().as_str().parse().unwrap(),
            w: caps.get(4).unwrap().as_str().parse().unwrap(),
            h: caps.get(5).unwrap().as_str().parse().unwrap()
        };
        // println!("line: {}, caps: {:?}", l, c);
        claim_vec.push(c);
    }

    Ok(claim_vec)
}

fn apply_claim(c : Claim, f : &mut Fabric){
    // println!("{:?}", c);
    for i in c.x..(c.x+c.w) {
        for j in c.y..(c.y+c.h) {
            f[i][j] += 1;
        }
    }
}

fn claim_is_free(c : &Claim, f : &Fabric) -> bool {
    for i in c.x..(c.x+c.w) {
        for j in c.y..(c.y+c.h) {
            if f[i][j] > 1 {
                return false;
            }
        }
    }
    true
}

fn print_fabric(f: &Fabric) {
    for l in 0..40 {
        for c in 0..40 {
          let x = f[l][c];
          if x == 0 {
              print!("{}.{} ", color::Fg(color::LightCyan), color::Fg(color::Reset));
          }
          else if x == 1 {
              print!("# ");
          }
          else {
              print!("{}{}{} ", color::Fg(color::Red), x, color::Fg(color::Reset));
          }
        }
        println!("");
    }
}

fn count_overlap(f: &Fabric) -> Result<usize, Error> {
    let mut i : usize = 0;
    for l in f.iter() {
        for c in l.iter() {
            if *c > 1 {
                i += 1;
            }
        }
    }
    Ok(i)
}

fn search_valid_claim(f: &Fabric, v : &Vec<Claim>) -> Result<Claim, Error> {
    for c in v {
        if claim_is_free(c, f) {
            return Ok(c.clone());
        }
    }
    Err(std::io::Error::new(std::io::ErrorKind::Other, "No claim Found"))
}

