extern crate regex;
use regex::Regex;
// extern crate termion;
// use termion::color;
// use termion::clear;
use std::io::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
// use std::thread;
// use std::time::Duration;
// use std::io::prelude::*;
use std::collections::HashMap;


fn main() -> Result<(), Error> {
    let f = File::open("input.txt")?;
    let bufread = BufReader::new(f);

    let re = Regex::new(r"Step ([[:alpha:]]) must be finished before step ([[:alpha:]]) can begin\.").unwrap();
    let mut dependencies : HashMap<char, Vec<char>> = HashMap::new();
    // let mut remaining_nodes: Vec<char> = "ABCDEFGIJKLMNOPQRSTUVWXYZ".chars().collect();
    let mut remaining_nodes: Vec<char> = Vec::new();

    for line in bufread.lines() {
        let l = line?;
        if let Some(caps) = re.captures(&l) {
            let p : (char, char) = (caps.get(1).unwrap().as_str().parse().unwrap(), caps.get(2).unwrap().as_str().parse().unwrap());
            if let Some(x) = dependencies.get_mut(&p.0) {
                x.push(p.1);
                remaining_nodes.push(p.1);
            } else {
                dependencies.insert(p.0, vec![p.1]);
                remaining_nodes.push(p.1);
            }
            // println!("{} is dependency of {}", p.0, p.1);
        } else {
            println!("Could not parse: {}", l);
        }
    }
    remaining_nodes.sort();
    remaining_nodes.dedup();

    // Find first node
    let mut possible_next_vals: Vec<char> = dependencies.clone()
        .into_iter()
        .map(|(c, _)| c)
        .collect();
    possible_next_vals.sort();
    possible_next_vals.dedup();
    possible_next_vals.retain(|c| !remaining_nodes.contains(c));

    // println!("HashMap: {:?}", dependencies);
    // println!("Dep nodes: {:?}", remaining_nodes);
    // println!("Poss start: {:?}", possible_next_vals);

    let mut solution: String = "".to_string();

    while !dependencies.is_empty() {
        // take first next possible value
        let c = possible_next_vals[0];
        solution.push(c);
        possible_next_vals.remove(0);
        remaining_nodes.retain(|&x| x != c);
        // check all nodes whcih are depending on that node, if they have other dependencies
        'outer: for d in dependencies.get(&c).unwrap() {
            for r in remaining_nodes.clone() {
                if let Some(rd) = dependencies.get(&r){
                    if rd.contains(d) {
                        continue 'outer;
                    }
                }
            }
            // Does not have other deps, therefore add to pos vals
            possible_next_vals.push(*d);
        }
        possible_next_vals.sort();
        dependencies.remove(&c);
    }
    solution.push(possible_next_vals.pop().unwrap());
    println!("Solution Part 1: {}", solution);

    Ok(())

}
