extern crate regex;
extern crate termion;
use self::termion::color;

use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Error;
use std::collections::HashMap;
use std::collections::BTreeMap;

use regex::Regex;


#[derive(Clone)]
struct Shift {
    id: usize,
    month: usize,
    day: usize,
    time: [bool; 60],
}

type Schedule = HashMap<(usize, usize), Shift>;
type GuardSchedule = BTreeMap<usize, Vec<[bool; 60]>>;

fn main() -> Result<(), Error> {
    let file = File::open("input_sorted.txt")?;
    let (_s, g) = parse_log(&file)?;
    let sleepy_guy = get_guard_most_sleep(&g)?;
    let sleepy_min = get_most_sleepy_min(sleepy_guy ,&g)?;
    println!("Most sleepy guy: {}", sleepy_guy);
    println!("Most sleepy min: {}", sleepy_min);
    println!("Solution: {}", sleepy_guy * sleepy_min);

    Ok(())
}


fn parse_log(f: &File) -> Result<(Schedule, GuardSchedule), Error> {
    let bufread = BufReader::new(f);
    let mut shift_map : Schedule = Schedule::new();

    let guard_shift_re = Regex::new(r"\[1518-(\d{2})-(\d{2}) (\d{2}):(\d{2})\] Guard #(\d+) begins shift").unwrap();
    let guard_falls_asleep_re = Regex::new(r"\[1518-(\d{2})-(\d{2}) (\d{2}):(\d{2})\] falls asleep").unwrap();
    let guard_wakes_up_re = Regex::new(r"\[1518-(\d{2})-(\d{2}) (\d{2}):(\d{2})\] wakes up").unwrap();

    let mut sleeping = false;
    let mut sleep_begin = 0;

    for line in bufread.lines() {
        let l = line?;
        if let Some(caps) = guard_shift_re.captures(&l) {
            let active_guard = caps[5].parse().unwrap();
            let mut month : usize = caps[1].parse().unwrap();
            let mut day : usize = caps[2].parse().unwrap();
            let hour : usize = caps[3].parse().unwrap();
            // let min : usize = caps[4].parse().unwrap();
            if hour == 23 {
                match (day, month) {
                    (31, 1) | (28, 2) | (31, 3) | (30, 4) |
                        (31, 5) | (30, 6) | (31, 7) | (31, 8) |
                        (30, 9) | (31, 10) | (30, 11) | (31, 12) => {day = 1; month += 1;},
                    _ => day += 1,
                }
            }

            assert!(!sleeping);
            assert_eq!(shift_map.contains_key(&(month, day)), false);
            let s : Shift = Shift {id: active_guard, month: month, day: day, time:[false; 60]};
            shift_map.insert((month, day), s);
            // println!("active guard {} {}", active_guard, l);
        }
        else if let Some(caps) = guard_falls_asleep_re.captures(&l) {
            let month : usize = caps[1].parse().unwrap();
            let day : usize = caps[2].parse().unwrap();
            // let hour : usize = caps[3].parse().unwrap();
            let min : usize = caps[4].parse().unwrap();
            assert!(!sleeping);
            assert_eq!(shift_map.contains_key(&(month, day)), true);
            sleep_begin = min;
            sleeping = true;
            // active_guard = caps[5].parse().unwrap();
            // println!("fall asleep {}", l);
        }
        else if let Some(caps) = guard_wakes_up_re.captures(&l) {
            let month : usize = caps[1].parse().unwrap();
            let day : usize = caps[2].parse().unwrap();
            // let hour : usize = caps[3].parse().unwrap();
            let min : usize = caps[4].parse().unwrap();
            assert!(sleeping);
            assert_eq!(shift_map.contains_key(&(month, day)), true);
            sleeping = false;
            for i in sleep_begin..min {
                shift_map.get_mut(&(month, day)).unwrap().time[i] = true;
            }
            // active_guard = caps[5].parse().unwrap();
            // println!("fall asleep {}", l);
        }

        // println!("line: {}, caps: {:?}", l, c);
    }

    print_schedule(&shift_map);

    let mut guard_map : GuardSchedule = GuardSchedule::new();
    for i in shift_map.iter() {
        if let Some(v) = guard_map.get_mut(&i.1.id) {
            v.push(i.1.time);
        } else {
            let v = vec!(i.1.time);
            guard_map.insert(i.1.id, v);
        }
       // guard_map.insert(i.1.clone());
    }

    print_guard_map(&guard_map);

    Ok((shift_map, guard_map))
}


fn get_guard_most_sleep(g: &GuardSchedule) -> Result<usize, Error> {
    let mut sleepy_id: usize = 0;
    let mut sleep_record: usize = 0;
    for i in g {
        let mut sleep_time = 0;
        for v in i.1.into_iter() {
            for t in v.into_iter() {
                if t == &true {
                    sleep_time += 1;
                }
            }
        }
        if sleep_time > sleep_record {
            sleep_record = sleep_time;
            sleepy_id = *i.0;
        }
    }
    Ok(sleepy_id)
}


fn get_most_sleepy_min(g_id: usize, g: &GuardSchedule) -> Result<usize, Error> {
    let t : Vec<[bool; 60]> = g.get(&g_id).unwrap().clone();
    let mut sleepy_min : (usize, usize) = (0,0);
    for i in 0..60 {
        let mut sleep_count : usize = 0;
        for d in t.clone() {
            if d[i] {
                sleep_count += 1;
            }
        }
        if sleep_count > sleepy_min.1 {
            sleepy_min = (i, sleep_count);
        }
        // println!{"Sleep count of {}: {}   | {:?}",i, sleep_count, sleepy_min};
    }
    Ok(sleepy_min.0)
}


fn print_schedule(s: &Schedule) {
    for m in 1..12 {
        println!("\nM: {}\t|000000000001111111112222222222333333333344444444445555555555|", m);
        println!("\t|012345678901234567890123456789012345678901234567890123456789|");
        for d in 1..32 {
            if let Some(e) = s.get(&(m, d)) {
                print!("{}.{}. \t|", d, m);
                for i in e.time.into_iter() {
                    if i == &true {
                        print!("{}Z", color::Fg(color::Red));
                    } else {
                        print!("{}.", color::Fg(color::Cyan));
                    }
                }
                println!("{}| Id: {}", color::Fg(color::Reset), e.id);
            }
        }
    }
}


fn print_guard_map(g: &GuardSchedule) {
    for i in g {
        println!("\nGuard {:?}", i.0);
        for v in i.1.into_iter() {
            print!("{}\t|", i.0);
            for t in v.into_iter() {
                if t == &true {
                    print!("{}Z", color::Fg(color::Red));
                } else {
                    print!("{}.", color::Fg(color::Cyan));
                }
            }
            println!("{}|", color::Fg(color::Reset));
        }
    }
}
