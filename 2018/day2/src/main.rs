use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Error;

fn main() -> Result<(), Error> {
    let (doubles, triples) = part1()?;
    println!("First Part:");
    println!("Nr of IDs with duplicate letters: {}", doubles);
    println!("Nr of IDs with three same letters: {}", triples);
    println!("Solution: {}", doubles*triples);

    println!("Second Part:");
    println!("Solution: {}", part2()?);
    Ok(())
}

fn part1() -> Result<(i64,i64), Error> {
    let f = File::open("input.txt")?;
    let file = BufReader::new(&f);
    let mut doubles = 0;
    let mut triples = 0;
    for line in file.lines() {
        let l = line?;
        let mut v : Vec<(char, u64)> = Vec::new();
        for c in l.chars() {
            if let Some(mut entr) = v.iter_mut().find(|&&mut(i,_)| i==c )  {
                entr.1 += 1;
                continue;
            }
            v.push((c, 1));
        }
        if v.iter().any(|(_, i)| *i==2) {
            doubles += 1;
        }
        if v.iter().any(|(_, i)| *i==3) {
            triples += 1;
        }
        // println!("ID: {} Doubles: {} Triples {}", l, doubles, triples);
    }
    Ok((doubles, triples))
}

fn different_chars(s1: &String, s2: &String) -> usize{
    assert_eq!(s1.len(), s2.len());
    s1.chars().zip(s2.chars()).filter(|(c1, c2)| c1 != c2).count()
}

fn part2() -> Result<String, Error> {
    let f = File::open("input.txt")?;
    let file = BufReader::new(&f);
    let mut v : Vec<String> = Vec::new();
    let mut s1 : String = "".to_string();
    let mut s2 : String = "".to_string();

    'outer: for line in file.lines() {
        s1 = line?;
        for elem in v.iter() {
            s2 = elem.to_string();
            if different_chars(&s1 , &s2) == 1 {
                println!("s1: {}\ns2: {}", s1, s2);
                break 'outer;
            }
        }
        v.push(s1.clone());
    }
    Ok(s1.chars().zip(s2.chars())
       .filter(|(c1, c2)| c1 == c2)
       .map(|(c1, _)| c1)
       .collect::<String>())
}
