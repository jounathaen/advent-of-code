extern crate custom_error;
use custom_error::custom_error;
use std::{
    error,
    fs,
    ops::Sub,
    cmp::Ordering,
    thread,
    time::Duration,
};

custom_error!{#[derive(PartialEq,PartialOrd)] AstroidEror
    ParsingError = "Cannot parse str to Asteriod Map",
    OutOfBounds = "Out of Bounds Access",
}


#[derive(Debug, PartialEq)]
struct AsteroidMap {
    coords: Vec<Point>,
    size_x: usize,
    size_y: usize,
}

impl AsteroidMap {
    fn from(s: &str) -> Result<Self, AstroidEror> {
        let size_x = s.find("\n").unwrap_or(s.len());
        let size_y = s.lines().count();

        let mut map = AsteroidMap{coords: Vec::new(), size_x: size_x, size_y: size_y};
        let s_bytes = s.chars().filter(|c| *c != '\n').collect::<Vec<char>>();
        for y in 0..size_y {
            for x in 0..size_x {
                if let Some(c) = s_bytes.get(y*size_x + x) {
                    match c {
                        '.' => {},
                        '#' => map.coords.push(Point{x: x as isize, y: y as isize}),
                        _ => return Err(AstroidEror::ParsingError),
                    }
                } else {
                    return Err(AstroidEror::ParsingError);
                }
            }
        }
        Ok(map)
    }

    fn visible_from (&self, coord: &Point) -> Vec<Point> {
        let mut visib = Vec::new();
        'outer: for p1 in self.coords.iter() {
            if p1 == coord {
                continue;
            }
            let mut free = true;
            for p2 in self.coords.iter() {
                if p2.lies_between(coord, p1) && p1 != p2 && p2 != coord {
                    free = false;
                    break;
                }
            }
            if free {
                visib.push(p1.clone());
            }
        }
        visib
    }

    fn best_location (&self) -> (usize, Point) {
        let mut max_visibl = 0;
        let mut max_coord = Point{x: 0, y: 0};
        for c in self.coords.iter() {
            let nr_visib = self.visible_from(c).len();
            if nr_visib > max_visibl {
                max_visibl = nr_visib;
                max_coord = c.clone();
            }
        }
        (max_visibl, max_coord)
    }

    fn print (&self, po : &Point) {
        let mut buf = vec![vec!["."; self.size_y]; self.size_x];
        for p in self.coords.iter() {
            buf[p.x as usize][p.y as usize] = "o";
        }
        buf[po.x as usize][po.y as usize] = "#";
        for y in 0..self.size_y {
            for x in 0..self.size_x {
                print!{"{}", buf[x][y]};
            }
            println!{};
        }
    }

    fn vaporize_clockwise (&mut self, origin: &Point) -> Option<Vec<Point>> {
        if self.coords.len() == 0 {
            return None
        }
        self.coords.retain(|&e| e != *origin);
        let mut vaporized: Vec<Point> = Vec::new();
        let mut pm = PolMap::from(&self.coords, &origin);
        pm.asteroids.sort_by(|a, b| (&a.0.angle).partial_cmp(&b.0.angle).unwrap());

        let mut clustered =  pm.cluster();
        for i in 0..clustered.len() {
            vaporized.push(clustered[i].pop().unwrap().1);
        }
        self.coords.retain(|&e| !vaporized.contains(&e));
        Some(vaporized)
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct Point {
    x: isize,
    y: isize,
}

impl Point {
    fn from (toup: (isize, isize)) -> Self {
        Point{x: toup.0, y: toup.1}
    }
    fn is_on_line (&self, l: &Line) -> bool {
        f64::abs(self.y as f64 - (l.m * (self.x as f64)) - l.t) < 0.0000001
    }

    fn lies_between (&self, p1: &Point, p2: &Point) -> bool {
        if p1.x == p2.x {
            if self.x == p1.x {
                if  p1.y < self.y && self.y < p2.y
                    || p1.y > self.y && self.y > p2.y {
                        return true;
                }
            }
        } else {
            let l = Line::from(p1, p2);
            if self.is_on_line(&l) &&
                distance(p1, self) + distance(self, p2) - distance(p1, p2) < 0.0000001 {
                    return true;
            }
        }
        false
    }
}

impl PartialEq<(isize, isize)> for Point {
    fn eq(&self, other: &(isize, isize)) -> bool {
        self.x == other.0 && self.y == other.1
    }
}

impl Sub for Point {
    type Output = Point;
    fn sub(self, other: Point) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}


#[derive(Debug, PartialEq, Clone, Copy)]
struct PolPoint {
    r: f64,
    angle: f64,
}

#[derive(Debug, PartialEq, Clone)]
struct PolMap {
    asteroids: Vec<(PolPoint, Point)>,
    zero: Point,
}

impl PolMap {
    fn from (map: &Vec<Point>, zero: &Point) -> Self {
        let mut pmap = PolMap{asteroids: Vec::new(), zero: zero.clone()};
        for p in map.iter() {
            if p == zero {
                continue;
            }
            let r = distance(p, zero);
            let rel_point = *p - *zero;
            let angle = match (rel_point.x, rel_point.y) {
                (0, std::isize::MIN..=0) => 0.0,
                (0, 0..=std::isize::MAX) => std::f64::consts::PI,
                (std::isize::MIN..=0, _) => {
                    std::f64::consts::PI + f64::acos(rel_point.y as f64 / r as f64)},
                (0..=std::isize::MAX, _) => {
                    std::f64::consts::PI - f64::acos(rel_point.y as f64 / r as f64)},
                (_, _) => unreachable!{"not possible coord"},
            };
            pmap.asteroids.push((PolPoint{r: r, angle: angle}, p.clone()));
        }
        pmap
    }

    fn cluster (&self) -> Vec<Vec<(PolPoint, Point)>> {
        let mut vec = Vec::new();
        let mut pm = self.asteroids.clone();
        pm.sort_by(|a, b| (&a.0.angle).partial_cmp(&b.0.angle).unwrap());
        let mut a = 0;
        while a < self.asteroids.len() {
            let angle = self.asteroids[a].0.angle;
            let mut anglevec = Vec::new();
            let mut i = 0;
            while i+a < self.asteroids.len() && self.asteroids[a + i].0.angle - angle < 0.000001 {
                anglevec.push(self.asteroids[a + i]);
                i += 1;
            }
            anglevec.sort_by(|a, b| (&b.0.r).partial_cmp(&a.0.r).unwrap());
            a += i;
            vec.push(anglevec);
        }
        vec
    }
}


fn distance (p1: &Point, p2: &Point) -> f64 {
    f64::sqrt((p2.x - p1.x).pow(2) as f64 + (p2.y - p1.y).pow(2) as f64)
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct Line {
    m: f64,
    t: f64,
}


impl Line {
    fn from (p1: &Point, p2: &Point) -> Self {
        let m = ((p2.y - p1.y) as f64) / ((p2.x - p1.x) as f64);
        let t = p1.y as f64 - (m * p1.x as f64);
        Line{m: m, t: t}
    }
}

fn part1 (map: &AsteroidMap) -> (usize, Point) {
    let best_loc = map.best_location();
    println!{"best_location: {:?}", best_loc}
    best_loc
}

fn part2 (map: &mut AsteroidMap, best_loc: &Point) {
    let mut vaporized = Vec::new();
    while let Some(vapes) = map.vaporize_clockwise(&best_loc) {
        vaporized.extend(vapes);
        // print!("{}[2J", 27 as char);
        // map.print(&best_loc.1);
        // thread::sleep(Duration::from_millis(500));
    }
    println!{"vape {:?}", vaporized[200 - 1]};
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut map = AsteroidMap::from(&f).unwrap();

    let best_loc = part1(&map);
    part2(&mut map, &best_loc.1);

    Ok(())
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parser_test1 () {
        let asteroids = AsteroidMap::from(concat!{
            ".#..#\n",
            ".....\n",
            "#####\n",
            "....#\n",
            "...##\n"
        }).unwrap();
        assert_eq!{asteroids.coords, vec!{(1,0),(4,0),(0,2),(1,2),(2,2),(3,2),(4,2),(4,3),(3,4),(4,4)}};
    }

    #[test]
    fn parser_test2 () {
        assert_eq!(AsteroidMap::from(".#.#.#\n.#.#").unwrap_err(), AstroidEror::ParsingError);
        assert_eq!(AsteroidMap::from("123456").unwrap_err(), AstroidEror::ParsingError);
        println!();
        AsteroidMap::from(".#.#").unwrap();
        // AsteroidMap::from(".#.#\n#...").unwrap();
    }

    #[test]
    fn line_of_sight_test1 () {
        let asteroids = AsteroidMap::from(concat!{
            ".#..#\n",
            ".....\n",
            "#####\n",
            "....#\n",
            "...##\n"
        }).unwrap();
        let visible = asteroids.visible_from(&Point{x: 3, y: 4});
        for p in vec!{(4,0),(0,2),(1,2),(2,2),(3,2),(4,2),(4,3),(4,4)}.iter() {
            println!{"p: {:?} | visib: {:?}", p, visible}
            assert!{visible.contains(&Point::from(*p))};
        }
        println!{"visible from 1 0 {:?} ", asteroids.visible_from(&Point{x: 1, y: 0})};
        assert_eq!{asteroids.visible_from(&Point{x: 1, y: 0}).len(), 7}
        println!{"visible from 4 0 {:?} ", asteroids.visible_from(&Point{x: 4, y: 0})};
        assert_eq!{asteroids.visible_from(&Point{x: 4, y: 0}).len(), 7}
        println!{"visible from 0 2 {:?} ", asteroids.visible_from(&Point{x: 0, y: 2})};
        assert_eq!{asteroids.visible_from(&Point{x: 0, y: 2}).len(), 6}
        assert_eq!{asteroids.visible_from(&Point{x: 4, y: 2}).len(), 5}
        assert_eq!{asteroids.visible_from(&Point{x: 3, y: 4}).len(), 8}
        assert_eq!{asteroids.best_location(), (8, Point{x: 3, y: 4})};
    }

    #[test]
    fn calc_line_test () {
        assert_eq!{Line::from(&Point{x: 1,y: 1},&Point{x: 3,y: 4}), Line{m: 1.5,  t: -0.5}};
        assert_eq!{Line::from(&Point{x: 3,y: 4},&Point{x: 4,y: 2}), Line{m: -2.0, t: 10.0}};
    }

    #[test]
    fn lies_between_test () {
        let p1 = Point{x:4, y: 0};
        let p2 = Point{x:4, y: 3};
        let p3 = Point{x:4, y: 4};
        let p4 = Point{x:3, y: 3};
        assert!{p2.lies_between(&p1, &p3)};
        assert!{!p3.lies_between(&p1, &p2)};
        assert!{!p1.lies_between(&p2, &p3)};
        assert!{!p4.lies_between(&p1, &p3)};
    }

    #[test]
    fn dist_test () {
        assert_eq!{2.0 * distance(&Point{x:1, y: 1}, &Point{x: 2, y: 2}),
             distance(&Point{x:1, y: 1}, &Point{x: 3, y: 3})};

        assert!{distance(&Point{x:4, y: 0}, &Point{x: 4, y: 2}) < distance(&Point{x:4, y: 0}, &Point{x: 4, y: 4})};
    }

    #[test]
    fn pol_coord_test () {
        let asteroids = AsteroidMap::from(concat!{
            ".#..#\n",
            ".....\n",
            "#####\n",
            "....#\n",
            "...##\n"
        }).unwrap();
        let mut pm = PolMap::from(&asteroids.coords, &Point{x: 2, y: 2});
        pm.asteroids.sort_by(|a, b| (&a.0.angle).partial_cmp(&b.0.angle).unwrap());
        assert_eq!(pm.asteroids[0].1, Point{x: 4, y: 0});
        assert_eq!(pm.asteroids[1].1, Point{x: 3, y: 2});
        assert_eq!(pm.asteroids[2].1, Point{x: 4, y: 2});
        assert_eq!(pm.asteroids[3].1, Point{x: 4, y: 3});
        assert_eq!(pm.asteroids[4].1, Point{x: 4, y: 4});
        assert_eq!(pm.asteroids[5].1, Point{x: 3, y: 4});

    }

    #[test]
    fn vape_test1 () {
        let mut asteroids = AsteroidMap::from(concat!{
            ".#....#####...#..\n",
            "##...##.#####..##\n",
            "##...#...#.#####.\n",
            "..#.....#...###..\n",
            "..#.#.....#....##\n"
        }).unwrap();
        println!{"\n++++++++++++++++++"};
        println!{"{:?}", asteroids};
        asteroids.print(&Point{x: 8, y: 3});
        println!{"vaping..."};
        let vapes = asteroids.vaporize_clockwise(&Point{x: 8, y: 3}).unwrap();
        // asteroids.print(&Point{x: 8, y: 3});
        println!{"{:?}", vapes};
        assert_eq!(vapes[0], Point{x: 8, y: 1});
        assert_eq!(vapes[1], Point{x: 9, y: 0});
        assert_eq!(vapes[2], Point{x: 9, y: 1});
        assert_eq!(vapes[8], Point{x: 15, y: 1});
        assert_eq!(vapes[9], Point{x: 12, y: 2});
        assert_eq!(vapes[10], Point{x: 13, y: 2});
        assert_eq!(vapes[13], Point{x: 12, y: 3});
        assert_eq!(vapes[14], Point{x: 16, y: 4});
        assert_eq!(vapes[15], Point{x: 15, y: 4});
    }

    #[test]
    fn vape_test2 () {
        let mut asteroids = AsteroidMap::from(concat!{
            ".#..##.###...#######\n",
            "##.############..##.\n",
            ".#.######.########.#\n",
            ".###.#######.####.#.\n",
            "#####.##.#.##.###.##\n",
            "..#####..#.#########\n",
            "####################\n",
            "#.####....###.#.#.##\n",
            "##.#################\n",
            "#####.##.###..####..\n",
            "..######..##.#######\n",
            "####.##.####...##..#\n",
            ".#####..#.######.###\n",
            "##...#.##########...\n",
            "#.##########.#######\n",
            ".####.#.###.###.#.##\n",
            "....##.##.###..#####\n",
            ".#.#.###########.###\n",
            "#.#.#.#####.####.###\n",
            "###.##.####.##.#..##\n",
        }).unwrap();
        println!{"\n++++++++++++++++++"};
        // println!{"{:?}", asteroids};
        let lazor = Point{x: 11, y: 13};
        asteroids.print(&lazor);

        // let mut pm = PolMap::from(&asteroids.coords, &lazor);
        // pm.asteroids.sort_by(|a, b| (&a.0.angle).partial_cmp(&b.0.angle).unwrap());
        // for p in pm.asteroids {
            // println!{"{:?}", p};
        // }

        let vapes = asteroids.vaporize_clockwise(&lazor).unwrap();
        // asteroids.print(&Point{x: 8, y: 3});
        // println!{"{:?}", vapes};
        assert_eq!(vapes[0], Point{x: 11, y: 12});
        assert_eq!(vapes[1], Point{x: 12, y: 1});
        assert_eq!(vapes[2], Point{x: 12, y: 2});
        assert_eq!(vapes[9], Point{x: 12, y: 8});
        assert_eq!(vapes[19], Point{x: 16, y: 0});
        assert_eq!(vapes[49], Point{x: 16, y: 9});
        // assert_eq!(vapes[19], Point{x: 12, y: 8});
        // assert_eq!(vapes[19], Point{x: 12, y: 8});
        // assert_eq!(vapes[19], Point{x: 12, y: 8});
        // assert_eq!(vapes[19], Point{x: 12, y: 8});
    }
}
