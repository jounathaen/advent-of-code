use std::{
    fs,
    error,
};

fn calc_fuel(i: isize) -> isize {
    (i/3) - 2
}


fn recursive_fuel_req(fuel : isize) -> isize {
    let mut new_fuel = fuel;
    let mut tmp;
    let mut total_fuel = 0;

    while {tmp = calc_fuel(new_fuel); tmp > 0} {
        new_fuel = tmp;
        total_fuel += new_fuel;
    }
    total_fuel
}


fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut inp_vec = Vec::new();
    for line in f.lines() {
        inp_vec.push(line.parse::<isize>()?);
    }

    let mut normal_fuel = 0;
    let mut additional_fuel = 0;
    for i in inp_vec {
        let tmp = calc_fuel(i);
        normal_fuel += tmp;
        additional_fuel += recursive_fuel_req(tmp);
    }

    println!("Normal Fuel {}", normal_fuel);
    println!("Addidional Fuel {}", additional_fuel);
    println!("Total Fuel {}", normal_fuel + additional_fuel);


    Ok(())
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn calc_fuel_test_1(){
        assert_eq!(calc_fuel(12), 2);
    }

    #[test]
    fn calc_fuel_test_2(){
        assert_eq!(calc_fuel(14), 2);
    }

    #[test]
    fn calc_fuel_test_3(){
        assert_eq!(calc_fuel(1969), 654);
    }

    #[test]
    fn calc_fuel_test_4(){
        assert_eq!(calc_fuel(100756), 33583);
    }

    #[test]
    fn recursive_fuel_test_1(){
        assert_eq!(recursive_fuel_req(14), 2);
    }

    #[test]
    fn recursive_fuel_test_2(){
        assert_eq!(recursive_fuel_req(1969), 966);
    }

    #[test]
    fn recursive_fuel_test_3(){
        assert_eq!(recursive_fuel_req(100756), 50346);
    }

    #[test]
    fn recursive_fuel_test_4(){
        assert_eq!(recursive_fuel_req(15), 3);
    }

    #[test]
    fn recursive_fuel_test_5(){
        assert_eq!(recursive_fuel_req(16), 3);
    }

    #[test]
    fn recursive_fuel_test_6(){
        assert_eq!(recursive_fuel_req(17), 3);
    }
}
