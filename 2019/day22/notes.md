# Solution Thoughts

You cannot simulate the shuffle in realistic time!

Steps for solution:
- Derive formula that describes how the position of one card changes after a shuffle
- Derive formula that does the inverse: Which card gets to a selected position after a shuffle
- Do modular arithmetic, to simplyfy applying the formula 101741582076661 times (Not so easy...)

## Cut n Cards

### Example cut 3
```
Top          Bottom
0 1 2 3 4 5 6 7 8 9   Your deck
3 4 5 6 7 8 9 0 1 2   After cut 3
```

change of position:
`pos -> (pos - 3) % len`

### Example: cut -4
```
Top          Bottom
0 1 2 3 4 5 6 7 8 9   Your deck
6 7 8 9 0 1 2 3 4 5   After cut -4
```
change of position:
`pos -> (pos + 4) % len`

### Mathematical:

new position for a card:
```math
f_n(x) = x - n \mod len
```

*Inverse:* Card at position:
```math
c_n(x) = x + n \mod len
```

## Deal with Increment n

### Examples

#### Deal with incr 3
```
Top          Bottom
0 1 2 3 4 5 6 7 8 9   Your deck
0 7 4 1 8 5 2 9 6 3   After deal with inc 3
```
The multiplicative inverse of 3 is 7

#### Deal with incr 9
```
Top          Bottom
0 1 2 3 4 5 6 7 8 9   Your deck
0 9 8 7 6 5 4 3 2 1   After deal with inc 9
```
The multiplicative inverse of 9 is 9

### Mathematical:

new position for a card:
```math
f_n(x) = x * n \mod len
```

*Inverse:* Card at position:
```math
i_n(x) = n^{-1} * x \mod len
```

## Deal into new Stack

### Example
```
Top          Bottom
0 1 2 3 4 5 6 7 8 9   Your deck
9 8 7 6 5 4 3 2 1 0   After Deal int new Stack
```

`pos -> -pos - 1 % len`

### Mathematical:

new position for a card:
```math
f(x) =  -(x + 1) \mod len
```

*Inverse:* Card at position:
```math
s(x) =  -(x + 1) \mod len
=  -x - 1 \mod len
```

Function is inverse to it self...


## Chaining of functions

A sequence of shuffle instructions can be calculated, by creating a composition of the functions.

### Example 1

```
deal with increment 7
deal into new stack
deal into new stack
Result: 0 3 6 9 2 5 8 1 4 7
```

- $`i7(x) = 3 * x \mod 10`$
- $`s(x) = -1 -x \mod 10`$ 
```math
i7(s(s(x))) = 3 * (-1 -(-1 -x)) \mod 10
```
```math
= 3 * (-1 + 1 + x)) \mod 10 = 3 x \mod 10
```

### Example 2

```
deal with increment 7
deal with increment 9
cut -2
Result: 6 3 0 7 4 1 8 5 2 9
```

- $`i_7(x) = 3 * x \mod 10`$
- $`i_9(x) = 9 * x \mod 10`$
- $`c_{-2}(x) = x - 2 \mod 10`$

```math
i_7(i_9(c_{-2}(x))) = 3 * 9 (x - 2) \mod 10
= 27 x + 6 \mod 10
```

### Example 3

```
cut 6
deal with increment 7
deal into new stack
Result: 3 0 7 4 1 8 5 2 9 6
```
- $`c_6(x) =  6 + x \mod 10`$
- $`i_7(x) =  3 * x \mod 10`$
- $`s(x)  = -(x + 1) \mod 10`$

Card at position: 
```math
c_6(i_7(s(x))) = 6 + (3 * (-x -1)) \mod 10
= -3x + 3 \mod 10
```

### Example 4

```
deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1
Result: 9 2 5 8 1 4 7 0 3 6
```

- $`s_(x)   = - 1 - x \mod 10`$
- $`c_{-2}(x) = - 2 + x \mod 10`$
- $`i_7(x)  =   3 * x \mod 10`$
- $`c_8(x)  =   8 + x \mod 10`$
- $`c_{-4}(x) = - 4 + x \mod 10`$
- $`i_7(x)  =   3 * x \mod 10`$
- $`c_3(x)  =   3 + x \mod 10`$
- $`i_9(x)  =   9 * x \mod 10`$
- $`i_3(x)  =   7 * x \mod 10`$
- $`c_{-1}(x) =  -1 + x \mod 10`$

Card at position: 
```math
s(c_{-2}(i_7(...c_{-1}(x)...))) = -1 - (-2 + (3 * (8 + (-4 + (3 * (3 + (9 * (7 * (x-1))))))))) \mod 10
```
```math
= 9 - 7 * x \mod 10
```


## Doing the whole shit 101741582076661 times...

You always end up with a formula $`f(x) =  a * x + b \mod l`$.
Applying this formula on it self looks like this:

```math
f(x) =  a * x + b \mod l
```
```math
f(f(x)) = a^2 * x + ab + b \mod l \\
```
```math
f(f(f(x))) = a^3 * x + a^2b + ab + b \mod l
```
Now we do this $`n`$ times:

```math
f(f(...f(x)...))) = a^n * x + a^{n-1} b + a^{n-2} b + ... + ab + b \mod l \\
= a^n * x + b * (a^{n-1} + ... + a + 1) \mod l
```

Using the formulas for the [geometric series](https://en.wikipedia.org/wiki/Geometric_series#Formula), this can be simplified to:
```math
f(f(f(...f(x)...))) = a^n * x + b * (a^n - 1) / (a-1)
```


Now all that's left is to implement this without overflows...

I tried to use the `BigInt` crate, but it had some bugs... 
Therefore, I used `i128`, which made stuff a bit harder...
Note: Wolframalpha is surprisingly bad when it comes to large numbers and modulo calculus.
