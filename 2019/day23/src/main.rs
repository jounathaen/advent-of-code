extern crate intcode_sim;
use intcode_sim::{IntcodeSim, Signal};
extern crate crossbeam_channel;
use crossbeam_channel::{Receiver, Sender};
use std::{collections::HashSet, error, fs, thread};

#[derive(Debug, Copy, Clone)]
struct Message {
    addr: usize,
    x: isize,
    y: isize,
}

fn nic_tread(
    id: usize,
    code: Vec<isize>,
    inchannel: Receiver<Message>,
    outchannel: Sender<Message>,
    quitsignal: Receiver<()>,
) {
    let (mut nic, inp, outp) = IntcodeSim::new();
    let nic_thread = thread::spawn(move || {
        nic.load(code.clone());
        nic.execute().unwrap_or(());
    });

    if let Ok(Signal::WaitForInput) = outp.recv() {
        inp.send(id as isize).unwrap();
    } else {
        panic!("NIC does not ask for Address");
    }

    loop {
        if !quitsignal.is_empty() {
            // println!("NIC {} received a quit signal", id);
            drop(nic_thread);
            break;
        }
        match outp.recv().unwrap() {
            Signal::WaitForInput => {
                if inchannel.is_empty() {
                    inp.send(-1).unwrap();
                } else {
                    let msg = inchannel.recv().unwrap();
                    inp.send(msg.x).unwrap();
                    if let Signal::WaitForInput = outp.recv().unwrap() {
                        inp.send(msg.y).unwrap();
                    } else {
                        panic!("NIC should ask for Y val");
                    }
                }
            }
            Signal::Output(dest) => {
                if let Signal::Output(x) = outp.recv().unwrap() {
                    if let Signal::Output(y) = outp.recv().unwrap() {
                        let msg = Message{addr: dest as usize, x, y};
                        outchannel.send(msg).unwrap();
                    } else {
                        panic!("Expected Y val");
                    }
                } else {
                    panic!("Expected X val");
                }
            }
            Signal::ProgramHalted => {
                panic!("Program halted unexpected");
            }
        }
    }
}

fn part1_2(code: &Vec<isize>) {
    let (sync_tx, sync_rx) = crossbeam_channel::bounded::<()>(10);
    let (router_tx, router_rx) = crossbeam_channel::bounded::<Message>(10);
    let mut channels = Vec::new();
    let mut nics = Vec::new();
    for i in 0..50 {
        let (nic_tx, nic_rx) = crossbeam_channel::bounded::<Message>(10);
        channels.push((nic_tx.clone(), nic_rx.clone()));
        nics.push(thread::spawn({
            let router_tx = router_tx.clone();
            let sync_rx = sync_rx.clone();
            let code = code.clone();
            move || {
                nic_tread(i, code, nic_rx, router_tx, sync_rx);
            }
        }));
    }

    let mut first_255_msg = true;
    let mut already_sent_y = HashSet::new();
    'mainloop: loop {
        let msg = router_rx.recv().unwrap();
        if msg.addr <= nics.len() {
            channels[msg.addr].0.send(msg).unwrap();
        } else {
            if msg.addr == 255 {
                if first_255_msg {
                    println!(
                        "First Message for {} has  Y: {}",
                        msg.addr, msg.y
                    );
                    first_255_msg = false;
                }
                // NAT functionality
                // Note: This is not particulary stable, but we get the correct
                // result "most of the time"
                for c in &channels {
                    if !c.0.is_empty() {
                        continue 'mainloop;
                    }
                }
                if let Some(_) = already_sent_y.get(&msg.y) {
                    println!("First redundant Message has Y: {}", msg.y);
                    break;
                }
                already_sent_y.insert(msg.y);
                channels[0].0.send(msg).unwrap();
            } else {
                panic!("Got a unaddressable message {:?}", msg);
            }
        }
    }

    sync_tx.send(()).unwrap();

    for t in nics {
        t.join().unwrap();
    }
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut code = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        code.append(&mut tmp_inp_vec);
    }

    part1_2(&code);

    Ok(())
}
