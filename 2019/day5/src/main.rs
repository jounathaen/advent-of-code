use std::{
    error,
    fs,
    fmt,
    // io,
    // io::Write,
};


#[derive(Debug, PartialEq, Copy, Clone)]
enum ExecutionError {
    UnexpectedOpcode((isize, usize)),
    ParsingError(usize),
}

impl fmt::Display for ExecutionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ExecutionError::UnexpectedOpcode(i) => write!{f, "Invalid Opcode {} at index: {}", i.0, i.1},
            ExecutionError::ParsingError(i) => write!{f, "Could not parse Instruction {}", i},
        }
    }
}

impl error::Error for ExecutionError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}


#[derive(Debug, PartialEq, Copy, Clone)]
enum InstrMode {
    Pos,
    Imm,
}

#[derive(Debug, PartialEq, Copy, Clone)]
struct Computer {
    _next_input: Option<isize>,
    _last_output: Option<isize>,
    pc: usize,
}

#[derive(Debug, PartialEq, Copy, Clone)]
struct Instruction {
    instr: usize,
    param_mode: [InstrMode; 3],
}

impl Default for Computer {
    fn default() -> Self {
        Computer {
            _next_input: None,
            _last_output : None,
            pc: 0,
        }
    }
}

fn parse_opcode (opcode: usize) -> Result<Instruction, ExecutionError> {
    let mut opcode_tmp = opcode;
    let mut instr = Instruction{instr: 0, param_mode: [InstrMode::Pos; 3]};
    instr.instr = opcode_tmp % 100;
    opcode_tmp /= 100;
    for i in 0..3 {
        instr.param_mode[i] = match opcode_tmp % 10 {
            0 => InstrMode::Pos,
            1 => InstrMode::Imm,
            _ => return Err(ExecutionError::ParsingError(opcode)),
        };
        opcode_tmp /= 10;
    }
    Ok(instr)
}


macro_rules! extract_params {
    ($i:expr, $code:expr, $pc:expr, $modes:expr) => {
        {
            let mut params: [isize; $i] = [0; $i];
            for i in 0..$i {
                params[i] = match $modes.param_mode[i] {
                    InstrMode::Pos => $code[$code[$pc+i+1] as usize],
                    InstrMode::Imm => $code[$pc+i+1],
                };
            }
            params
        }
    };
}


impl Computer {
    fn execute(&mut self, code: &mut Vec<isize>) -> Result<(), ExecutionError> {
        self.pc=0;
        loop {
            let opcode = parse_opcode(code[self.pc] as usize)?;
            match opcode.instr {
                1 | 2 => { // Addiditon/Multiplication
                    let parameters = extract_params!{2, &code, self.pc, opcode};

                    if opcode.param_mode[2] == InstrMode::Imm {
                        return Err(ExecutionError::UnexpectedOpcode((opcode.instr as isize, self.pc)));
                    }
                    let target_addr = code[self.pc+3] as usize;
                    match opcode.instr {
                        1 => code[target_addr] = parameters[0] + parameters[1],
                        2 => code[target_addr] = parameters[0] * parameters[1],
                        _ => unreachable!{},
                    };
                    self.pc += 4;
                },
                3 => { // Input
                    let target : usize = match opcode.param_mode[0] {
                        InstrMode::Pos => code[self.pc+1] as usize,
                        InstrMode::Imm => return Err(ExecutionError::UnexpectedOpcode((code[self.pc], self.pc))),
                    };
                    print!("Programm at pc {} requests input for {}: ", self.pc, target);
                    // TODO: Implement an "Interactive" Switch
                    // io::stdout().flush().unwrap();
                    // let mut input = String::new();
                    // io::stdin().read_line(&mut input).unwrap();
                    // code[target] = input.trim().parse::<isize>().unwrap();
                    code[target] = self._next_input.unwrap();
                    println!{"Setting input: {}", self._next_input.unwrap()};
                    self._next_input = None;
                    self.pc += 2;
                },
                4 => { // Output
                    self._last_output = Some(
                        match opcode.param_mode[0] {
                            InstrMode::Pos => code[code[self.pc+1] as usize],
                            InstrMode::Imm => code[self.pc+1],
                        }
                    );
                    println!{"Output: {}", self.get_last_output().unwrap()};
                    self.pc += 2;
                },
                5 | 6 => { // Conditional Jumps
                    let parameters = extract_params!{2, &code, self.pc, opcode};
                    // println!{"condition was {}",  parameters[0]}
                    match (parameters[0], opcode.instr) {
                        (std::isize::MIN..=-1, 5) | (1..=std::isize::MAX, 5)  
                            | (0, 6) => self.pc = parameters[1] as usize, // jump
                        (0, 5) | (_, 6) => self.pc += 3, // don't jump
                        (_, _) => unreachable!{},
                    }
                }
                7 | 8 => {
                    let parameters = extract_params!{2, &code, self.pc, opcode};
                    let target = match opcode.param_mode[2] {
                        InstrMode::Pos => code[self.pc+3] as usize,
                        InstrMode::Imm => self.pc+3 as usize,
                    };
                    code[target] = match opcode.instr {
                        7 => if parameters[0] < parameters[1] {1} else {0},
                        8 => if parameters[0] == parameters[1] {1} else {0},
                        _ => unreachable!{},
                    };
                    self.pc += 4;
                },
                99 => return Ok(()),
                i => return Err(ExecutionError::UnexpectedOpcode((i as isize, self.pc))),
            }
        }
        // Ok(())
    }

    fn set_input(&mut self, inp: isize) {
        self._next_input = Some(inp);
    }

    fn get_last_output(&mut self) -> Option<isize> {
        self._last_output
    }
}


fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut inp_vec = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        inp_vec.append(&mut tmp_inp_vec);
    }

    let mut part1_vec = inp_vec.clone();

    let mut comp = Computer::default();
    comp.set_input(1);
    comp.execute(&mut part1_vec)?;

    comp.set_input(5);
    comp.execute(&mut inp_vec)?;

    Ok(())
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parsing_test_1(){
        assert_eq!( parse_opcode(1002).unwrap(), 
            Instruction{
                instr: 2, param_mode: [
                    InstrMode::Pos,
                    InstrMode::Imm,
                    InstrMode::Pos,
                ],
            });
        assert_eq!( parse_opcode(1).unwrap(), 
            Instruction{
                instr: 1, param_mode: [
                    InstrMode::Pos,
                    InstrMode::Pos,
                    InstrMode::Pos,
                ],
            });
        assert_eq!( parse_opcode(1101).unwrap(), 
            Instruction{
                instr: 1, param_mode: [
                    InstrMode::Imm,
                    InstrMode::Imm,
                    InstrMode::Pos,
                ],
            });
    }

    #[test]
    fn code_test_1(){
        let mut inp : Vec<isize> = vec!{1,9,10,3,2,3,11,0,99,30,40,50};
        let mut comp = Computer::default();
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{3500,9,10,70,2,3,11,0,99,30,40,50});

        inp = vec!{1,0,0,0,99};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{2,0,0,0,99});

        inp = vec!{2,3,0,3,99};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{2,3,0,6,99});

        inp = vec!{2,4,4,5,99,0};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{2,4,4,5,99,9801});

        inp = vec!{1,1,1,4,99,5,6,0,99};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{30,1,1,4,2,5,6,0,99});
    }

    #[test]
    fn code_test_2(){
        let mut inp : Vec<isize> = vec!{1101,100,-1,4,0};
        let mut comp = Computer::default();
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{1101,100,-1,4,99});

        inp = vec!{1002,4,3,4,33};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{1002,4,3,4,99});
    }

    #[test]
    fn code_test_3(){ // Jumping
        // jmp if true
        let mut inp : Vec<isize> = vec!{1105, 1, 7, 1101, 0, 123, 8 ,99, -1};
        let mut comp = Computer::default();
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp[8], -1);

        // no jump because 0
        inp = vec!{1105, 0, 7, 1101, 0, 123, 8 ,99, -1};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp[8], 123);

        // no jump because 0
        inp = vec!{1106, 1, 7, 1101, 0, 123, 8 ,99, -1};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp[8], 123);

        // no jump because 0
        inp = vec!{1106, 0, 7, 1101, 0, 123, 8 ,99, -1};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp[8], -1);
    }

    #[test]
    fn code_test_4(){ // Conditionals
        let mut inp : Vec<isize> = vec!{1107, 1, 2, 5, 99, -1};
        let mut comp = Computer::default();
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp[5], 1);

        inp = vec!{1107, 2, 1, 5, 99, -1};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp[5], 0);

        // Equals
        inp = vec!{1108, 1, 1, 5, 99, -1};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp[5], 1);

        inp = vec!{1108, 1, 2, 5, 99, -1};
        comp.execute(&mut inp).unwrap();
        assert_eq!(inp[5], 0);
    }

    #[test]
    fn code_test_5(){
        let mut inp : Vec<isize> = vec!{3,9,8,9,10,9,4,9,99,-1,8};
        let mut comp = Computer::default();
        comp.set_input(8);
        comp.execute(&mut inp).unwrap();
        assert_eq!(comp.get_last_output().unwrap(), 1);

        inp = vec!{3,9,8,9,10,9,4,9,99,-1,8};
        comp.set_input(9);
        comp.execute(&mut inp).unwrap();
        assert_eq!(comp.get_last_output().unwrap(), 0);
    }

    #[test]
    fn code_test_6(){
        let mut inp : Vec<isize> = vec!{3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9};
        let mut comp = Computer::default();
        comp.set_input(0);
        comp.execute(&mut inp).unwrap();
        assert_eq!(comp.get_last_output().unwrap(), 0);

        inp = vec!{3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9};
        comp.set_input(4);
        comp.execute(&mut inp).unwrap();
        assert_eq!(comp.get_last_output().unwrap(), 1);

        inp = vec!{3,3,1105,-1,9,1101,0,0,12,4,12,99,1};
        comp.set_input(4);
        comp.execute(&mut inp).unwrap();
        assert_eq!(comp.get_last_output().unwrap(), 1);
    }

    #[test]
    fn code_test_7(){
        let mut inp : Vec<isize> = vec!{
            3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99};
        let mut comp = Computer::default();
        comp.set_input(5);
        comp.execute(&mut inp).unwrap();
        assert_eq!(comp.get_last_output().unwrap(), 999);
    }

    #[test]
    fn error_test(){
        let mut inp : Vec<isize> = vec!{98,0,0,0,99};
        let mut comp = Computer::default();
        assert_eq!{comp.execute(&mut inp).unwrap_err(),
            ExecutionError::UnexpectedOpcode((98, 0))};
        inp = vec!{201,0,0,0,99};
        assert_eq!{comp.execute(&mut inp).unwrap_err(),
            ExecutionError::ParsingError(201)};
    }

    #[test]
    fn extract_params_test(){
        let inp : Vec<isize> = vec!{10103,1,2,3};
        let p_opc = parse_opcode(inp[0] as usize).unwrap();
        let parsed = extract_params!{3, &inp, 0, p_opc};
        assert_eq!{parsed, [1, 2, 3]};

        let parsed = extract_params!{2, &inp, 0, p_opc};
        assert_eq!{parsed, [1, 2]};

        let parsed = extract_params!{1, &inp, 0, p_opc};
        assert_eq!{parsed, [1]};
    }
}
