use std::{
    collections::VecDeque,
    error,
    fmt,
    fs,
    ops,
    sync::mpsc,
    thread,
};


#[derive(Debug, PartialEq, Copy, Clone)]
enum ExecutionError {
    UnexpectedOpcode((isize, usize)),
    ParsingError(usize),
    NoProgram(),
}

impl fmt::Display for ExecutionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ExecutionError::UnexpectedOpcode(i) => write!{f, "Invalid Opcode {} at index: {}", i.0, i.1},
            ExecutionError::NoProgram() => write!{f, "No program loaded"},
            ExecutionError::ParsingError(i) => write!{f, "Could not parse Instruction {}", i},
        }
    }
}

impl error::Error for ExecutionError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}


#[derive(Debug, PartialEq, Copy, Clone)]
enum InstrMode {
    Pos,
    Imm,
}

#[derive(Debug)]
struct IntcodeSim {
    input: mpsc::Receiver<isize>,
    code: Vec<isize>,
    output: mpsc::Sender<isize>,
    _pc: usize,
}

#[derive(Debug, PartialEq, Copy, Clone)]
struct Instruction {
    instr: usize,
    param_mode: [InstrMode; 3],
}


fn parse_opcode (opcode: usize) -> Result<Instruction, ExecutionError> {
    let mut opcode_tmp = opcode;
    let mut instr = Instruction{instr: 0, param_mode: [InstrMode::Pos; 3]};
    instr.instr = opcode_tmp % 100;
    opcode_tmp /= 100;
    for i in 0..3 {
        instr.param_mode[i] = match opcode_tmp % 10 {
            0 => InstrMode::Pos,
            1 => InstrMode::Imm,
            _ => return Err(ExecutionError::ParsingError(opcode)),
        };
        opcode_tmp /= 10;
    }
    Ok(instr)
}


macro_rules! extract_params {
    ($i:expr, $code:expr, $pc:expr, $modes:expr) => {
        {
            let mut params: [isize; $i] = [0; $i];
            for i in 0..$i {
                params[i] = match $modes.param_mode[i] {
                    InstrMode::Pos => $code[$code[$pc+i+1] as usize],
                    InstrMode::Imm => $code[$pc+i+1],
                };
            }
            params
        }
    };
}


impl IntcodeSim {
    fn new () -> (IntcodeSim, mpsc::Sender<isize>, mpsc::Receiver<isize>) {
        let (inp_snd, inp_recv) = mpsc::channel();
        let (outp_snd, outp_recv) = mpsc::channel();
        (IntcodeSim {
            input: inp_recv,
            code: Vec::new(),
            output : outp_snd,
            _pc: 0,
        }, inp_snd, outp_recv)
    }

    fn create (inp: mpsc::Receiver<isize>, outp: mpsc::Sender<isize>, code: Vec<isize>) -> (IntcodeSim) {
        IntcodeSim {
            input: inp,
            code: code,
            output : outp,
            _pc: 0,
        }
    }


    fn execute (&mut self) -> Result<(), ExecutionError> {
        if self.code.len() == 0 {
            return Err(ExecutionError::NoProgram());
        }
        self._pc=0;
        loop {
            let opcode = parse_opcode(self.code[self._pc] as usize)?;
            match opcode.instr {
                1 | 2 => { // Addiditon/Multiplication
                    let parameters = extract_params!{2, &self.code, self._pc, opcode};

                    if opcode.param_mode[2] == InstrMode::Imm {
                        return Err(ExecutionError::UnexpectedOpcode((opcode.instr as isize, self._pc)));
                    }
                    let target_addr = self.code[self._pc+3] as usize;
                    match opcode.instr {
                        1 => self.code[target_addr] = parameters[0] + parameters[1],
                        2 => self.code[target_addr] = parameters[0] * parameters[1],
                        _ => unreachable!{},
                    };
                    self._pc += 4;
                },
                3 => { // Input
                    let target : usize = match opcode.param_mode[0] {
                        InstrMode::Pos => self.code[self._pc+1] as usize,
                        InstrMode::Imm => return Err(ExecutionError::UnexpectedOpcode((self.code[self._pc], self._pc))),
                    };
                    // print!("Programm at pc {} requests input for {}: ", self._pc, target);
                    // TODO: Implement an "Interactive" Switch
                    // io::stdout().flush().unwrap();
                    // let mut input = String::new();
                    // io::stdin().read_line(&mut input).unwrap();
                    // self.code[target] = input.trim().parse::<isize>().unwrap();
                    self.code[target] = self.input.recv().unwrap();
                    // println!{"Setting input: {}", self.code[target]};
                    self._pc += 2;
                },
                4 => { // Output
                    self.output.send(
                        match opcode.param_mode[0] {
                            InstrMode::Pos => self.code[self.code[self._pc+1] as usize],
                            InstrMode::Imm => self.code[self._pc+1],
                        }
                    ).unwrap();
                    // println!{"Output: {}", self.output.back().unwrap()};
                    self._pc += 2;
                },
                5 | 6 => { // Conditional Jumps
                    let parameters = extract_params!{2, &self.code, self._pc, opcode};
                    match (parameters[0], opcode.instr) {
                        (std::isize::MIN..=-1, 5) | (1..=std::isize::MAX, 5)
                            | (0, 6) => self._pc = parameters[1] as usize, // jump
                        (0, 5) | (_, 6) => self._pc += 3, // don't jump
                        (_, _) => unreachable!{},
                    }
                }
                7 | 8 => {
                    let parameters = extract_params!{2, &self.code, self._pc, opcode};
                    let target = match opcode.param_mode[2] {
                        InstrMode::Pos => self.code[self._pc+3] as usize,
                        InstrMode::Imm => self._pc+3 as usize,
                    };
                    self.code[target] = match opcode.instr {
                        7 => if parameters[0] < parameters[1] {1} else {0},
                        8 => if parameters[0] == parameters[1] {1} else {0},
                        _ => unreachable!{},
                    };
                    self._pc += 4;
                },
                99 => return Ok(()),
                i => return Err(ExecutionError::UnexpectedOpcode((i as isize, self._pc))),
            }
        }
        // Ok(())
    }
}


fn amp_contr_software (code: &Vec<isize>,  phases: &[isize; 5]) -> isize {
    let (in_tx, a_rx) = mpsc::channel();
    let (a_tx, b_rx) = mpsc::channel();
    let (b_tx, c_rx) = mpsc::channel();
    let (c_tx, d_rx) = mpsc::channel();
    let (d_tx, e_rx) = mpsc::channel();
    let (e_tx, out_rx) = mpsc::channel();

    let mut amp_a = IntcodeSim::create(a_rx, a_tx.clone(), code.clone());
    let mut amp_b = IntcodeSim::create(b_rx, b_tx.clone(), code.clone());
    let mut amp_c = IntcodeSim::create(c_rx, c_tx.clone(), code.clone());
    let mut amp_d = IntcodeSim::create(d_rx, d_tx.clone(), code.clone());
    let mut amp_e = IntcodeSim::create(e_rx, e_tx, code.clone());

    in_tx.send(phases[0]).unwrap();
    a_tx.send(phases[1]).unwrap();
    b_tx.send(phases[2]).unwrap();
    c_tx.send(phases[3]).unwrap();
    d_tx.send(phases[4]).unwrap();

    in_tx.send(0).unwrap();

    amp_a.execute().unwrap();
    amp_b.execute().unwrap();
    amp_c.execute().unwrap();
    amp_d.execute().unwrap();
    amp_e.execute().unwrap();
    out_rx.recv().unwrap()
}


fn amp_contr_software_feedback (code: &Vec<isize>,  phases: &[isize; 5]) -> isize {
    let (in_tx, a_rx) = mpsc::channel();
    let (a_tx, b_rx) = mpsc::channel();
    let (b_tx, c_rx) = mpsc::channel();
    let (c_tx, d_rx) = mpsc::channel();
    let (d_tx, e_rx) = mpsc::channel();
    let (e_tx, out_rx) = mpsc::channel();

    let mut amp_a = IntcodeSim::create(a_rx, a_tx.clone(), code.clone());
    let mut amp_b = IntcodeSim::create(b_rx, b_tx.clone(), code.clone());
    let mut amp_c = IntcodeSim::create(c_rx, c_tx.clone(), code.clone());
    let mut amp_d = IntcodeSim::create(d_rx, d_tx.clone(), code.clone());
    let mut amp_e = IntcodeSim::create(e_rx, e_tx.clone(), code.clone());

    in_tx.send(phases[0]).unwrap();
    a_tx.send(phases[1]).unwrap();
    b_tx.send(phases[2]).unwrap();
    c_tx.send(phases[3]).unwrap();
    d_tx.send(phases[4]).unwrap();

    in_tx.send(0).unwrap();

    let mut threads = Vec::new();
    threads.push(thread::spawn(move|| {
        amp_a.execute().unwrap();
    }));
    threads.push(thread::spawn(move|| {
        amp_b.execute().unwrap();
    }));
    threads.push(thread::spawn(move|| {
        amp_c.execute().unwrap();
    }));
    threads.push(thread::spawn(move|| {
        amp_d.execute().unwrap();
    }));
    threads.push(thread::spawn(move|| {
        amp_e.execute().unwrap();
    }));
    // Feedback
    let output = thread::spawn(move|| {
        loop {
            let feedb = out_rx.recv().unwrap();
            if let Some(_) = in_tx.send(feedb).err() {
                // println!("could not send {} anymore", feedb);
                return feedb;
            }
        }
    });

    for t in threads {
        t.join().unwrap();
    }
    output.join().unwrap()
}


fn generate_permutations (range: std::ops::Range<isize>) -> Vec<[isize; 5]> {
    let mut perms: Vec<[isize; 5]> = Vec::new();
    // let mut tokens: Vec<i8> = vec!{0,1,2,3,4,5};
    for i in range.clone() {
        for j in range.clone() {
            if j == i {
                continue;
            }
            for k in range.clone() {
                if k == i || k == j {
                    continue;
                }
                for l in range.clone() {
                    if l == i || l == j || l == k {
                        continue;
                    }
                    for m in range.clone() {
                        if m == i || m == j || m == k || m == l {
                            continue;
                        }
                        perms.push([i,j,k,l,m]);
                    }
                }
            }
        }
    }
    perms
}


fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut inp_vec = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        inp_vec.append(&mut tmp_inp_vec);
    }

    // Part 1
    // let (mut amp_sim, _, _) = IntcodeSim::new();
    let mut max_thruster_sign = 0;
    // let mut max_thruster_phase = &[0; 5];
    let perms = generate_permutations(0..5);

    for p in perms.iter() {
        let new_sign = amp_contr_software(&inp_vec.clone(), p);
        if new_sign > max_thruster_sign {
            max_thruster_sign = new_sign;
            // max_thruster_phase = p;
        }
    }

    // println!{"Maximum Signal is {} from phase setting {:?}", max_thruster_sign, max_thruster_phase};
    println!{"Maximum Signal is {}", max_thruster_sign};

    // Part 2
    let mut max_thruster_sign = 0;
    // // let mut max_thruster_phase = &[0; 5];
    let perms = generate_permutations(5..10);

    for p in perms.iter() {
        let new_sign = amp_contr_software_feedback(&inp_vec.clone(), p);
        if new_sign > max_thruster_sign {
            max_thruster_sign = new_sign;
            // max_thruster_phase = p;
        }
    }

    // println!{"Maximum Signal is {} from phase setting {:?}", max_thruster_sign, max_thruster_phase};
    println!{"Maximum Signal is {}", max_thruster_sign};

    Ok(())
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parsing_test_1(){
        assert_eq!( parse_opcode(1002).unwrap(),
            Instruction{
                instr: 2, param_mode: [
                    InstrMode::Pos,
                    InstrMode::Imm,
                    InstrMode::Pos,
                ],
            });
        assert_eq!( parse_opcode(1).unwrap(),
            Instruction{
                instr: 1, param_mode: [
                    InstrMode::Pos,
                    InstrMode::Pos,
                    InstrMode::Pos,
                ],
            });
        assert_eq!( parse_opcode(1101).unwrap(),
            Instruction{
                instr: 1, param_mode: [
                    InstrMode::Imm,
                    InstrMode::Imm,
                    InstrMode::Pos,
                ],
            });
    }

    #[test]
    fn code_test_1(){
        let (mut sim, _, _) = IntcodeSim::new();
        sim.code = vec!{1,9,10,3,2,3,11,0,99,30,40,50};
        sim.execute().unwrap();
        assert_eq!(sim.code, vec!{3500,9,10,70,2,3,11,0,99,30,40,50});

        sim.code = vec!{1,0,0,0,99};
        sim.execute().unwrap();
        assert_eq!(sim.code, vec!{2,0,0,0,99});

        sim.code = vec!{2,3,0,3,99};
        sim.execute().unwrap();
        assert_eq!(sim.code, vec!{2,3,0,6,99});

        sim.code = vec!{2,4,4,5,99,0};
        sim.execute().unwrap();
        assert_eq!(sim.code, vec!{2,4,4,5,99,9801});

        sim.code = vec!{1,1,1,4,99,5,6,0,99};
        sim.execute().unwrap();
        assert_eq!(sim.code, vec!{30,1,1,4,2,5,6,0,99});
    }

    #[test]
    fn code_test_2(){
        let (mut sim, _, _) = IntcodeSim::new();
        sim.code = vec!{1101,100,-1,4,0};
        sim.execute().unwrap();
        assert_eq!(sim.code, vec!{1101,100,-1,4,99});

        sim.code = vec!{1002,4,3,4,33};
        sim.execute().unwrap();
        assert_eq!(sim.code, vec!{1002,4,3,4,99});
    }

    #[test]
    fn code_test_3(){ // Jumping
        // jmp if true
        let (mut sim, _, _) = IntcodeSim::new();
        sim.code = vec!{1105, 1, 7, 1101, 0, 123, 8 ,99, -1};
        sim.execute().unwrap();
        assert_eq!(sim.code[8], -1);

        // no jump because 0
        sim.code = vec!{1105, 0, 7, 1101, 0, 123, 8 ,99, -1};
        sim.execute().unwrap();
        assert_eq!(sim.code[8], 123);

        // no jump because 0
        sim.code = vec!{1106, 1, 7, 1101, 0, 123, 8 ,99, -1};
        sim.execute().unwrap();
        assert_eq!(sim.code[8], 123);

        // no jump because 0
        sim.code = vec!{1106, 0, 7, 1101, 0, 123, 8 ,99, -1};
        sim.execute().unwrap();
        assert_eq!(sim.code[8], -1);
    }

    #[test]
    fn code_test_4(){ // Conditionals
        let (mut sim, _, _) = IntcodeSim::new();
        sim.code = vec!{1107, 1, 2, 5, 99, -1};
        sim.execute().unwrap();
        assert_eq!(sim.code[5], 1);

        sim.code = vec!{1107, 2, 1, 5, 99, -1};
        sim.execute().unwrap();
        assert_eq!(sim.code[5], 0);

        // Equals
        sim.code = vec!{1108, 1, 1, 5, 99, -1};
        sim.execute().unwrap();
        assert_eq!(sim.code[5], 1);

        sim.code = vec!{1108, 1, 2, 5, 99, -1};
        sim.execute().unwrap();
        assert_eq!(sim.code[5], 0);
    }

    #[test]
    fn code_test_5(){
        let (mut sim, inp, outp) = IntcodeSim::new();
        sim.code = vec!{3,9,8,9,10,9,4,9,99,-1,8};
        inp.send(8).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1);

        sim.code = vec!{3,9,8,9,10,9,4,9,99,-1,8};
        inp.send(9).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 0);
    }

    #[test]
    fn code_test_6(){
        let (mut sim, inp, outp) = IntcodeSim::new();
        sim.code = vec!{3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9};
        inp.send(0).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 0);

        sim.code = vec!{3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9};
        inp.send(4).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1);

        sim.code = vec!{3,3,1105,-1,9,1101,0,0,12,4,12,99,1};
        inp.send(4).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1);
    }

    #[test]
    fn code_test_7(){
        let (mut sim, inp, outp) = IntcodeSim::new();
        sim.code = vec!{
            3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99};
        inp.send(5).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 999);
    }

    #[test]
    fn error_test(){
        let (mut sim, _, _) = IntcodeSim::new();
        sim.code = vec!{98,0,0,0,99};
        assert_eq!{sim.execute().unwrap_err(),
            ExecutionError::UnexpectedOpcode((98, 0))};
        sim.code = vec!{201,0,0,0,99};
        assert_eq!{sim.execute().unwrap_err(),
            ExecutionError::ParsingError(201)};
    }

    #[test]
    fn acs_test () {
        // let (mut sim, inp, outp) = IntcodeSim::new();
        let prog = vec!{3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0};
        let thruster_sign = amp_contr_software(&prog, &[4,3,2,1,0]);
        assert_eq!{thruster_sign, 43210};

        let prog = vec!{3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0};
        let thruster_sign = amp_contr_software(&prog, &[0,1,2,3,4]);
        assert_eq!{thruster_sign, 54321};

        let prog = vec!{3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0};
        let thruster_sign = amp_contr_software(&prog, &[1,0,4,3,2]);
        assert_eq!{thruster_sign, 65210};
    }

    #[test]
    fn acs_feedb_test () {
        // let (mut sim, inp, outp) = IntcodeSim::new();
        let prog = vec!{3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5};
        let thruster_sign = amp_contr_software_feedback(&prog, &[9,8,7,6,5]);
        assert_eq!{thruster_sign, 139629729};

        let prog = vec!{3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10};
        let thruster_sign = amp_contr_software_feedback(&prog, &[9,7,8,5,6]);
        assert_eq!{thruster_sign, 18216};
    }

    #[test]
    fn extract_params_test(){
        let inp : Vec<isize> = vec!{10103,1,2,3};
        let p_opc = parse_opcode(inp[0] as usize).unwrap();
        let parsed = extract_params!{3, &inp, 0, p_opc};
        assert_eq!{parsed, [1, 2, 3]};

        let parsed = extract_params!{2, &inp, 0, p_opc};
        assert_eq!{parsed, [1, 2]};

        let parsed = extract_params!{1, &inp, 0, p_opc};
        assert_eq!{parsed, [1]};
    }
}
