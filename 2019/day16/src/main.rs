use rayon::prelude::*;
use std::{collections::HashMap, error, fs};

/// Applies the fft to a single line.
/// # Arguments
/// * `v` - The input signal
/// * `iteration` - which iteration inside one phase are we (shifting the base pattern)
/// * `vir_len` - how often is v repeated
fn multiply_list(v: &[i64], iteration: usize, vir_len: usize) -> i64 {
    let mut i = iteration;
    let mut sum = 0;
    let nr_iterations = v.len() * vir_len;
    while i <= nr_iterations {
        for j in i..=(i + (iteration - 1)) {
            if j > nr_iterations {
                break;
            }
            sum += v.get((j - 1) % v.len()).unwrap();
        }
        i += 2 * iteration; // once for the j values and once for the 0 values
        for j in i..=(i + (iteration - 1)) {
            if j > nr_iterations {
                break;
            }
            sum -= v.get((j - 1) % v.len()).unwrap();
        }
        i += 2 * iteration; // once for the j values and once for the 0 values
    }
    sum.abs() % 10
}

/// Appliy fft in parallel for the whole vector and return the new vector
/// # Arguments
/// * `v` - The input signal
/// * `vir_len` - how often is v repeated
fn multiply_phase(v: &[i64], vir_len: usize) -> Vec<i64> {
    (1..=v.len())
        .into_par_iter()
        .map(|i| multiply_list(&v, i, vir_len))
        .collect()
}

/// Recursive Calculation using a cache. Without the cache quite slow.
/// line equals to the position in the final array
/// # Arguments
/// * `v` - The input signal
/// * `vir_len` - How often the input signal is repeated
/// * `iteration` - which iteration inside one phase are we (shifting the base pattern)
/// * `cache` - a cache to speed up calculations
fn calc_recursive(
    v: &[i64],
    vir_len: usize,
    line: usize,
    iteration: usize,
    mut cache: &mut HashMap<(usize, usize), i64>,
) -> i64 {
    let mut sum = 0;
    let line_len = v.len() * vir_len;
    let mut plus_op = true;

    let mut i = line; // we can skip the first 0 fields
    while i <= line_len {
        for j in i..=(i + (line - 1)) {
            if j > line_len {
                break;
            }

            // recursive step: End recursion if we are at iteration 1 or we already computed that
            // value
            let val = if iteration == 1 {
                *v.get((j - 1) % v.len()).unwrap()
            } else {
                if let Some(cached_val) = cache.get(&(j, iteration)) {
                    *cached_val
                } else {
                    let val = calc_recursive(&v, vir_len, j, iteration - 1, &mut cache);
                    cache.insert((j, iteration), val.clone());
                    val
                }
            };

            if plus_op {
                sum += val;
            } else {
                sum -= val;
            }
        }
        plus_op = !plus_op;
        i += 2 * line; // increase by the j values and skip the 0 values
    }
    sum.abs() % 10
}

/// I'm really disappointed that this is the only fast solution :-(
/// # Arguments
/// * `v` - The input signal
/// * `vir_len` - How often the input signal is repeated
/// * `repetitions` - How often the fft is applied repeatedly (in AoC: 100 times)
fn stupid_backwards_calculation(v: &Vec<i64>, vir_len: usize, repetitions: usize) -> i64 {
    // We need 1 additional entry in the repetitions vector because we also store the input value
    // in that field (Could be optimized, but 4 Bytes...)
    let mut sliding_vec = Vec::with_capacity(repetitions + 1);
    for _ in 0..(repetitions + 1) {
        sliding_vec.push(v[v.len() - 1]);
    }
    let offset: usize = {
        let mut offset = 0;
        let mut multiplyer = 1;
        for i in (0..7).rev() {
            offset += v[i] * multiplyer;
            multiplyer *= 10;
        }
        offset as usize
    };
    // This Method only works for the second halve of the values
    assert!(offset > (v.len() * vir_len / 2));
    for i in ((offset + 1 + 8)..(v.len() * vir_len)).rev() {
        sliding_vec[0] = v[(i - 1) % v.len()];
        for j in 1..sliding_vec.len() {
            sliding_vec[j] = (sliding_vec[j] + sliding_vec[j - 1]) % 10;
        }
    }
    let mut sol_vec = Vec::new();
    for i in ((offset + 1)..(offset + 1 + 8)).rev() {
        sliding_vec[0] = v[(i - 1) % v.len()];
        for j in 1..sliding_vec.len() {
            sliding_vec[j] = (sliding_vec[j] + sliding_vec[j - 1]) % 10;
        }
        sol_vec.push(sliding_vec[sliding_vec.len() - 1]);
    }
    let mut sol = 0;
    let mut multiplyer = 1;
    for i in sol_vec.iter() {
        sol += i * multiplyer;
        multiplyer *= 10;
    }
    sol
}

fn part1(v: &Vec<i64>) {
    print!("Solution Pt1: ");
    // Iterative approach is faster than recursive one
    let mut res = v.clone();
    for _ in 0..100 {
        res = multiply_phase(&res, 1);
    }

    // Recursive Approach, not so fast for "small" inputs
    // let mut cache = HashMap::new();
    // let res: Vec<i64> = (1..=8)
    // .into_iter()
    // .map(|i| calc_recursive(&v, 1, i, 100, &mut cache))
    // .collect();

    for i in res.iter().take(8) {
        print!("{}", i);
    }
    println!();
}

fn part2(v: &Vec<i64>) {
    println!(
        "Solution Pt2: {}",
        stupid_backwards_calculation(&v, 10_000, 100)
    );
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let s = fs::read_to_string("input.txt")?;
    let v = s
        .chars()
        .filter_map(|c| c.to_digit(10))
        .map(|res| res as i64)
        .collect();

    part1(&v);
    part2(&v);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn calc_recursive_test() {
        let v = vec![1, 2, 3, 4, 5, 6, 7, 8];
        let mut cache = HashMap::new();

        let res: Vec<i64> = (1..=8)
            .into_iter()
            .map(|i| calc_recursive(&v, 1, i as usize, 1, &mut cache))
            .collect();
        let v_lsg = vec![4, 8, 2, 2, 6, 1, 5, 8];
        assert_eq!(res, v_lsg);

        // cache.clear();
        let res: Vec<i64> = (1..=8)
            .into_iter()
            .map(|i| calc_recursive(&v, 1, i as usize, 2, &mut cache))
            .collect();
        let v_lsg = vec![3, 4, 0, 4, 0, 4, 3, 8];
        assert_eq!(res, v_lsg);

        // cache.clear();
        let res: Vec<i64> = (1..=8)
            .into_iter()
            .map(|i| calc_recursive(&v, 1, i as usize, 3, &mut cache))
            .collect();
        let v_lsg = vec![0, 3, 4, 1, 5, 5, 1, 8];
        assert_eq!(res, v_lsg);
    }

    #[test]
    fn stupid_calc_test() {
        let v = vec![
            0, 3, 0, 3, 6, 7, 3, 2, 5, 7, 7, 2, 1, 2, 9, 4, 4, 0, 6, 3, 4, 9, 1, 5, 6, 5, 4, 7, 4,
            6, 6, 4,
        ];
        assert_eq!(stupid_backwards_calculation(&v, 10_000, 100), 84462026);
        let v = vec![
            0, 2, 9, 3, 5, 1, 0, 9, 6, 9, 9, 9, 4, 0, 8, 0, 7, 4, 0, 7, 5, 8, 5, 4, 4, 7, 0, 3, 4,
            3, 2, 3,
        ];
        assert_eq!(stupid_backwards_calculation(&v, 10_000, 100), 78725270);
        let v = vec![
            0, 3, 0, 8, 1, 7, 7, 0, 8, 8, 4, 9, 2, 1, 9, 5, 9, 7, 3, 1, 1, 6, 5, 4, 4, 6, 8, 5, 0,
            5, 1, 7,
        ];
        assert_eq!(stupid_backwards_calculation(&v, 10_000, 100), 53553731);
    }

    #[test]
    fn recursive_100_depht() {
        let v = vec![
            8, 0, 8, 7, 1, 2, 2, 4, 5, 8, 5, 9, 1, 4, 5, 4, 6, 6, 1, 9, 0, 8, 3, 2, 1, 8, 6, 4, 5,
            5, 9, 5,
        ];
        let mut cache = HashMap::new();
        let res: Vec<i64> = (1..=8)
            .into_iter()
            .map(|i| calc_recursive(&v, 1, i, 100, &mut cache))
            .collect();
        assert_eq!(res, vec![2, 4, 1, 7, 6, 1, 7, 6]);

        let v = vec![
            1, 9, 6, 1, 7, 8, 0, 4, 2, 0, 7, 2, 0, 2, 2, 0, 9, 1, 4, 4, 9, 1, 6, 0, 4, 4, 1, 8, 9,
            9, 1, 7,
        ];
        cache.clear();
        let res: Vec<i64> = (1..=8)
            .into_iter()
            .map(|i| calc_recursive(&v, 1, i, 100, &mut cache))
            .collect();
        assert_eq!(res, vec![7, 3, 7, 4, 5, 4, 1, 8]);

        let v = vec![
            6, 9, 3, 1, 7, 1, 6, 3, 4, 9, 2, 9, 4, 8, 6, 0, 6, 3, 3, 5, 9, 9, 5, 9, 2, 4, 3, 1, 9,
            8, 7, 3,
        ];
        cache.clear();
        let res: Vec<i64> = (1..=8)
            .into_iter()
            .map(|i| calc_recursive(&v, 1, i, 100, &mut cache))
            .collect();
        assert_eq!(res, vec![5, 2, 4, 3, 2, 1, 3, 3]);
    }

    #[test]
    fn recursive_multi_len() {
        let v = vec![1, 2, 3, 4, 5, 6, 7, 8];
        let mut cache = HashMap::new();

        let res: Vec<i64> = (1..=16)
            .into_iter()
            .map(|i| calc_recursive(&v, 2, i as usize, 1, &mut cache))
            .collect();
        let v_lsg = vec![8, 6, 1, 0, 2, 7, 0, 6, 6, 5, 3, 0, 6, 1, 5, 8];
        assert_eq!(res, v_lsg);

        let res: Vec<i64> = (1..=16)
            .into_iter()
            .map(|i| calc_recursive(&v, 4, i as usize, 1, &mut cache))
            .collect();
        let v_lsg = vec![6, 2, 6, 0, 1, 1, 2, 0, 4, 0, 8, 8, 3, 3, 6, 2];
        assert_eq!(res, v_lsg);
    }

    #[test]
    fn multiply_list_test() {
        let v = vec![1, 2, 3, 4, 5, 6, 7, 8];
        assert_eq!(multiply_list(&v, 1, 1), 4);
        assert_eq!(multiply_list(&v, 2, 1), 8);
        assert_eq!(multiply_list(&v, 3, 1), 2);
        assert_eq!(multiply_list(&v, 4, 1), 2);
        assert_eq!(multiply_list(&v, 5, 1), 6);
        assert_eq!(multiply_list(&v, 6, 1), 1);
        assert_eq!(multiply_list(&v, 7, 1), 5);
        assert_eq!(multiply_list(&v, 8, 1), 8);
        let v = vec![4, 8, 2, 2, 6, 1, 5, 8];
        assert_eq!(multiply_list(&v, 1, 1), 3);
        assert_eq!(multiply_list(&v, 2, 1), 4);
        assert_eq!(multiply_list(&v, 3, 1), 0);
        assert_eq!(multiply_list(&v, 4, 1), 4);
        assert_eq!(multiply_list(&v, 5, 1), 0);
        assert_eq!(multiply_list(&v, 6, 1), 4);
        assert_eq!(multiply_list(&v, 7, 1), 3);
        assert_eq!(multiply_list(&v, 8, 1), 8);
    }

    #[test]
    fn multiply_long_list_test() {
        let v = vec![1, 2, 3, 4, 5, 6, 7, 8];
        assert_eq!(multiply_list(&v, 1, 2), 8);
        assert_eq!(multiply_list(&v, 2, 2), 6);
        assert_eq!(multiply_list(&v, 3, 2), 1);
        assert_eq!(multiply_list(&v, 4, 2), 0);
        assert_eq!(multiply_list(&v, 5, 2), 2);
        assert_eq!(multiply_list(&v, 6, 2), 7);
        assert_eq!(multiply_list(&v, 7, 2), 0);
        assert_eq!(multiply_list(&v, 8, 2), 6);

        assert_eq!(multiply_list(&v, 1, 4), 6);
        assert_eq!(multiply_list(&v, 2, 4), 2);
        assert_eq!(multiply_list(&v, 3, 4), 6);
        assert_eq!(multiply_list(&v, 4, 4), 0);
        assert_eq!(multiply_list(&v, 5, 4), 1);
        assert_eq!(multiply_list(&v, 6, 4), 1);
        assert_eq!(multiply_list(&v, 7, 4), 2);
        assert_eq!(multiply_list(&v, 8, 4), 0);
    }

    #[test]
    fn multiply_phase_test() {
        let v = vec![1, 2, 3, 4, 5, 6, 7, 8];
        let mut v = multiply_phase(&v, 1);
        assert_eq!(v, vec![4, 8, 2, 2, 6, 1, 5, 8]);
        v = multiply_phase(&v, 1);
        assert_eq!(v, vec![3, 4, 0, 4, 0, 4, 3, 8]);
        v = multiply_phase(&v, 1);
        assert_eq!(v, vec![0, 3, 4, 1, 5, 5, 1, 8]);
        v = multiply_phase(&v, 1);
        assert_eq!(v, vec![0, 1, 0, 2, 9, 4, 9, 8]);
    }

    #[test]
    fn multiply_phase_2() {
        let mut v = vec![
            8, 0, 8, 7, 1, 2, 2, 4, 5, 8, 5, 9, 1, 4, 5, 4, 6, 6, 1, 9, 0, 8, 3, 2, 1, 8, 6, 4, 5,
            5, 9, 5,
        ];
        for _ in 0..100 {
            v = multiply_phase(&v, 1);
        }
        dbg!(&v);
        assert!(v.iter().take(8).eq(vec![2, 4, 1, 7, 6, 1, 7, 6].iter()));
        v = vec![
            1, 9, 6, 1, 7, 8, 0, 4, 2, 0, 7, 2, 0, 2, 2, 0, 9, 1, 4, 4, 9, 1, 6, 0, 4, 4, 1, 8, 9,
            9, 1, 7,
        ];
        for _ in 0..100 {
            v = multiply_phase(&v, 1);
        }
        assert!(v.iter().take(8).eq(vec![7, 3, 7, 4, 5, 4, 1, 8].iter()));
        v = vec![
            6, 9, 3, 1, 7, 1, 6, 3, 4, 9, 2, 9, 4, 8, 6, 0, 6, 3, 3, 5, 9, 9, 5, 9, 2, 4, 3, 1, 9,
            8, 7, 3,
        ];
        for _ in 0..100 {
            v = multiply_phase(&v, 1);
        }
        assert!(v.iter().take(8).eq(vec![5, 2, 4, 3, 2, 1, 3, 3].iter()));
    }
}
