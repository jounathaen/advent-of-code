extern crate custom_error;
use custom_error::custom_error;
use std::{
    error,
    fs,
    str::FromStr,
};

custom_error!{TreeError
    ParentNotFound{s:String} = "{s} is not in the tree",
    ParsingError{i:usize} = "Cannot parse str to tree at {i}"
}


#[derive(Debug, Clone)]
struct Node {
    parent_id: Option<usize>,
    content: String,
}


#[derive(Default, Debug, Clone)]
struct Tree {
    nodes: Vec<Node>,
}


impl Tree {
    fn get_index (&self, name: &String) -> Option<usize> {
        for i in 0..self.nodes.len() {
            if self.nodes[i].content.eq(name) {
                return Some(i);
            }
        }
        None
    }

    fn insert(&mut self, name: &String, parent: &Option<&String>) -> Result<(), TreeError> {
        // Check if node is already present and we just have to set the parent
        if let Some(n) = self.get_index(name) {
            assert_eq!(self.nodes[n].parent_id, None);
            if let Some(i) = self.get_index(parent.unwrap()) {
                self.nodes[n].parent_id = Some(i);
            } else {
                return Err(TreeError::ParentNotFound{s: parent.unwrap().clone()});
            }
            return Ok(());
        }
        match parent {
            None => self.nodes.push(Node{parent_id: None, content: name.clone()}),
            Some(p) => {
                if let Some(i) = self.get_index(p) {
                    self.nodes.push(Node{parent_id: Some(i), content: name.clone()});
                } else {
                    return Err(TreeError::ParentNotFound{s: (*p).clone()});
                }
            },
        }
        Ok(())
    }

    fn get(&self, id: usize) -> Option<&Node> {
        if id < self.nodes.len() {
            Some(&self.nodes[id])
        } else {
            None
        }
    }

    fn get_from_string(&self, name: &String) -> Option<&Node> {
        self.get(self.get_index(name)?)
    }

    fn count_total_ancestors(&self) -> usize {
        self.nodes.iter().map(|n| n.get_nr_parents(&self)).sum()
    }

}


impl FromStr for Tree {
    type Err = TreeError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut tree = Tree::default();
        for l in s.lines() {
            let elems : Vec<&str> = l.split(")").collect();
            // println!{"str {:?}", elems};
            assert_eq!(elems.len(), 2);
            if let Err(_error) =  tree.insert(&String::from(elems[1]), &Some(&String::from(elems[0]))) {
                // println!{"error {:?}", elems};
                tree.insert(&String::from(elems[0]), &None)?;
                tree.insert(&String::from(elems[1]), &Some(&String::from(elems[0]))).unwrap();
            }
        }
        Ok(tree)
    }
}


impl Node {
    fn get_nr_parents(&self, f: &Tree) -> usize {
        let mut count = 0;
        let mut parent_id = self.parent_id;
        while let Some(temp_id) = parent_id {
            count += 1;
            parent_id = f.get(temp_id).unwrap().parent_id;
        }
        count
    }

    fn get_path_to_root(&self, f: &Tree) -> Vec<String> {
        let mut path = Vec::new();
        path.push(self.content.clone());
        let mut parent_id = self.parent_id;
        while let Some(temp_id) = parent_id {
            path.push(f.get(temp_id).unwrap().content.clone());
            parent_id = f.get(temp_id).unwrap().parent_id;
        }
        // path.reverse();
        path
    }
}


fn min_distance (n1: &Node, n2: &Node, t: &Tree) -> usize {
        let mut p1 = n1.get_path_to_root(&t);
        let mut p2 = n2.get_path_to_root(&t);
        while p1[p1.len() - 1] == p2[p2.len() - 1] {
            p1.pop();
            p2.pop();
        }
        p1.len() + p2.len() - 2
}


fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let planets = Tree::from_str(&f).unwrap();
    // println!{"Tree: {:?}", planets};
    println!{"Nr of Orbits: {}", planets.count_total_ancestors()};

    let n1 = planets.get_from_string(&String::from("YOU")).unwrap();
    let n2 = planets.get_from_string(&String::from("SAN")).unwrap();
    let dist = min_distance(&n1, &n2, &planets);
    println!{"Distance between you and Santa: {}", dist};

    Ok(())
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn tree_insert_test1 () {
        let mut tree = Tree::default();
        tree.insert(&String::from("abc1"), &None).unwrap();
        tree.insert(&String::from("abc2"), &Some(&String::from("abc1"))).unwrap();
        assert_eq!(tree.get_index(&String::from("abc2")), Some(1));
    }

    #[test]
    fn ancestor_test1 () {
        let mut tree = Tree::default();
        tree.insert(&String::from("abc1"), &None).unwrap();
        tree.insert(&String::from("abc2"), &Some(&String::from("abc1"))).unwrap();
        tree.insert(&String::from("abc3"), &Some(&String::from("abc2"))).unwrap();

        let n = tree.get_from_string(&String::from("abc3")).unwrap();
        assert_eq!(n.get_nr_parents(&tree), 2);
    }

    #[test]
    fn str_to_tree_test1 () {
        let inp = 
"COM)B
B)C
C)D
COM)E";
        let tree = Tree::from_str(inp).unwrap();
        println!{"tree {:?}", tree};
        let nodec = tree.get_from_string(&String::from("C")).unwrap();
        assert_eq!(nodec.parent_id.unwrap(), tree.get_index(&String::from("B")).unwrap());

        let nodeb = tree.get_from_string(&String::from("B")).unwrap();
        let nodee = tree.get_from_string(&String::from("E")).unwrap();
        assert_eq!(nodeb.parent_id, nodee.parent_id);
    }

    #[test]
    fn str_to_tree_test2 () {
        let inp = 
"COM)B
C)D
B)C
COM)E";
        let tree = Tree::from_str(inp).unwrap();
        println!{"tree {:?}", tree};
        let nodec = tree.get_from_string(&String::from("C")).unwrap();
        assert_eq!(nodec.parent_id.unwrap(), tree.get_index(&String::from("B")).unwrap());

        let nodeb = tree.get_from_string(&String::from("B")).unwrap();
        let nodee = tree.get_from_string(&String::from("E")).unwrap();
        assert_eq!(nodeb.parent_id, nodee.parent_id);
    }

    #[test]
    fn total_ancestors_count_test () {
        let inp = 
"COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L";
        let tree = Tree::from_str(inp).unwrap();
        assert_eq!(tree.count_total_ancestors(), 42);
    }


    #[test]
    fn path_to_root_test () {
        let inp = 
"COM)B
B)C
C)D
D)E
E)F
B)G";
        let tree = Tree::from_str(inp).unwrap();
        let path = tree.get_from_string(&String::from("F")).unwrap().get_path_to_root(&tree);
        assert_eq!(path, ["F", "E", "D", "C", "B", "COM"]);
    }

    #[test]
    fn min_distance_test () {
        let inp = 
"COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN";
        let tree = Tree::from_str(inp).unwrap();
        let n1 = tree.get_from_string(&String::from("YOU")).unwrap();
        let n2 = tree.get_from_string(&String::from("SAN")).unwrap();
        assert_eq!(min_distance(&n1, &n2, &tree), 4);
    }

}
