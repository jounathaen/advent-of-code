use crossbeam_channel::{bounded, unbounded, Receiver, Sender};
use custom_error::custom_error;
use std::collections::HashMap;

custom_error! {#[derive(PartialEq,PartialOrd)] pub ExecutionError
    UnexpectedOpcode{opc:isize, index:usize} = "Invalid Opcode {opc} at index: {index}",
    NoProgram = "No program loaded",
    ParsingError{instr:isize} = "Could not parse Instruction {instr}",
    WriteModeError{pc:usize} = "Attempting to write in immediate mode at {pc}",
    IOError = "Channel not available anymore",
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Parameter {
    Pos(usize),
    Imm(isize),
    Rel(isize),
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Opcode {
    Add(Parameter, Parameter, Parameter),
    Mul(Parameter, Parameter, Parameter),
    Inp(Parameter),
    Out(Parameter),
    Jit(Parameter, Parameter),
    Jif(Parameter, Parameter),
    Les(Parameter, Parameter, Parameter),
    Equ(Parameter, Parameter, Parameter),
    Bas(Parameter),
    Hlt,
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Signal {
    Output(isize),
    WaitForInput,
    ProgramHalted,
}

#[derive(Debug)]
pub struct IntcodeSim {
    _input: Receiver<isize>,
    _memory: Vec<isize>,
    _dyn_memory: HashMap<usize, isize>,
    _output: Sender<Signal>,
    _pc: usize,
    _base: isize,
}

impl IntcodeSim {
    pub fn new() -> (
        IntcodeSim,
        Sender<isize>,
        Receiver<Signal>,
    ) {
        let (inp_snd, inp_recv) = unbounded();
        let (outp_snd, outp_recv) = bounded(1);
        (
            IntcodeSim {
                _input: inp_recv,
                _memory: Vec::new(),
                _dyn_memory: HashMap::new(),
                _output: outp_snd,
                _pc: 0,
                _base: 0,
            },
            inp_snd,
            outp_recv,
        )
    }

    pub fn new_with_existing_io(
        inp: Receiver<isize>,
        outp: Sender<Signal>,
        memory: Vec<isize>,
    ) -> IntcodeSim {
        IntcodeSim {
            _input: inp,
            _memory: memory,
            _dyn_memory: HashMap::new(),
            _output: outp,
            _pc: 0,
            _base: 0,
        }
    }

    pub fn load(&mut self, memory: Vec<isize>) {
        self._pc = 0;
        self._base = 0;
        self._memory = memory;
        self._dyn_memory.clear();
    }

    pub fn get_memory_as_vec(&self) -> Vec<isize> {
        let mut mem = self._memory.clone();
        for i in self._memory.len()
            ..=*self
                ._dyn_memory
                .keys()
                .max()
                .unwrap_or(&(self._memory.len() - 1))
        {
            mem.push(self._dyn_memory.get(&i).unwrap_or(&0).clone());
        }
        mem
    }

    fn parse_instruction(&self, pc: usize) -> Result<Opcode, ExecutionError> {
        let opcode = self
            ._memory
            .get(pc)
            .unwrap_or_else(|| self._dyn_memory.get(&pc).unwrap());
        let mut params = [Parameter::Pos(0); 3];
        let mut opcode_tmp = opcode / 100;
        for i in 0..3 {
            params[i] = match opcode_tmp % 10 {
                0 => Parameter::Pos(
                    *self
                        ._memory
                        .get(pc + i + 1)
                        .unwrap_or_else(|| self._dyn_memory.get(&(pc + i + 1)).unwrap_or(&0))
                        as usize,
                ),
                1 => Parameter::Imm(
                    *self
                        ._memory
                        .get(pc + i + 1)
                        .unwrap_or_else(|| self._dyn_memory.get(&(pc + i + 1)).unwrap_or(&0)),
                ),
                2 => Parameter::Rel(
                    *self
                        ._memory
                        .get(pc + i + 1)
                        .unwrap_or_else(|| self._dyn_memory.get(&(pc + i + 1)).unwrap_or(&0)),
                ),
                _ => return Err(ExecutionError::ParsingError { instr: *opcode }),
            };
            opcode_tmp /= 10;
        }
        match opcode % 100 {
            1 => Ok(Opcode::Add(params[0], params[1], params[2])),
            2 => Ok(Opcode::Mul(params[0], params[1], params[2])),
            3 => Ok(Opcode::Inp(params[0])),
            4 => Ok(Opcode::Out(params[0])),
            5 => Ok(Opcode::Jit(params[0], params[1])),
            6 => Ok(Opcode::Jif(params[0], params[1])),
            7 => Ok(Opcode::Les(params[0], params[1], params[2])),
            8 => Ok(Opcode::Equ(params[0], params[1], params[2])),
            9 => Ok(Opcode::Bas(params[0])),
            99 => Ok(Opcode::Hlt),
            _ => {
                return Err(ExecutionError::UnexpectedOpcode {
                    opc: *opcode,
                    index: self._pc,
                })
            }
        }
    }

    fn read_mem(&self, param: &Parameter) -> isize {
        match param {
            Parameter::Pos(i) => *self
                ._memory
                .get(*i)
                .unwrap_or_else(|| self._dyn_memory.get(&i).unwrap_or(&0)),
            Parameter::Rel(i) => {
                *self
                    ._memory
                    .get((self._base + i) as usize)
                    .unwrap_or_else(|| {
                        self._dyn_memory
                            .get(&((self._base + i) as usize))
                            .unwrap_or(&0)
                    })
            }
            Parameter::Imm(i) => *i,
        }
    }

    fn write_to(&mut self, param: &Parameter, val: isize) -> Result<(), ExecutionError> {
        let target: usize = match param {
            Parameter::Pos(i) => *i,
            Parameter::Rel(i) => (self._base as isize + i) as usize,
            Parameter::Imm(_) => return Err(ExecutionError::WriteModeError { pc: self._pc }),
        };
        if target < self._memory.len() {
            self._memory[target] = val;
        } else {
            self._dyn_memory.insert(target, val);
        }
        Ok(())
    }

    pub fn execute(&mut self) -> Result<(), ExecutionError> {
        if self._memory.len() == 0 {
            return Err(ExecutionError::NoProgram);
        }
        self._pc = 0;
        loop {
            let instruction = self.parse_instruction(self._pc)?;
            match instruction {
                Opcode::Add(p1, p2, t) => {
                    // Addition
                    self.write_to(&t, self.read_mem(&p1) + self.read_mem(&p2))?;
                    self._pc += 4;
                }
                Opcode::Mul(p1, p2, t) => {
                    // Addition
                    self.write_to(&t, self.read_mem(&p1) * self.read_mem(&p2))?;
                    self._pc += 4;
                }
                Opcode::Inp(t) => {
                    // Input
                    self._output.send(Signal::WaitForInput).ok();
                    if let Ok(inp) = self._input.recv() {
                        self.write_to(&t, inp)?;
                    } else {
                        // println!("Channel is down. Exiting!");
                        return Err(ExecutionError::IOError);
                    }
                    self._pc += 2;
                }
                Opcode::Out(p) => {
                    // Output
                    self._output.send(Signal::Output(self.read_mem(&p))).unwrap();
                    self._pc += 2;
                }
                Opcode::Jit(p1, p2) => {
                    // jump-if-true
                    if self.read_mem(&p1) != 0 {
                        self._pc = self.read_mem(&p2) as usize;
                    } else {
                        self._pc += 3;
                    }
                }
                Opcode::Jif(p1, p2) => {
                    // jump-if-false
                    if self.read_mem(&p1) == 0 {
                        self._pc = self.read_mem(&p2) as usize;
                    } else {
                        self._pc += 3;
                    }
                }
                Opcode::Les(p1, p2, t) => {
                    // Less Than
                    self.write_to(
                        &t,
                        if self.read_mem(&p1) < self.read_mem(&p2) {
                            1
                        } else {
                            0
                        },
                    )?;
                    self._pc += 4;
                }
                Opcode::Equ(p1, p2, t) => {
                    // Equals
                    self.write_to(
                        &t,
                        if self.read_mem(&p1) == self.read_mem(&p2) {
                            1
                        } else {
                            0
                        },
                    )?;
                    self._pc += 4;
                }
                Opcode::Bas(p1) => {
                    // Base Adjust
                    self._base += self.read_mem(&p1);
                    self._pc += 2;
                }
                Opcode::Hlt => {
                    let _ = self._output.send(Signal::ProgramHalted);
                    return Ok(());
                }
            }
        }
    }
}

#[cfg(test)]
mod intcode_sim_test {
    use super::*;

    #[test]
    fn parsing_1() {
        let (mut sim, _, _) = IntcodeSim::new();
        sim.load(vec![1002, 9, 10, 3, 1, 3, 11, 0, 1101, 30, 40, 50]);
        assert_eq!(
            sim.parse_instruction(0).unwrap(),
            Opcode::Mul {
                0: Parameter::Pos(9),
                1: Parameter::Imm(10),
                2: Parameter::Pos(3),
            }
        );
        assert_eq!(
            sim.parse_instruction(4).unwrap(),
            Opcode::Add {
                0: Parameter::Pos(3),
                1: Parameter::Pos(11),
                2: Parameter::Pos(0),
            }
        );
        assert_eq!(
            sim.parse_instruction(8).unwrap(),
            Opcode::Add {
                0: Parameter::Imm(30),
                1: Parameter::Imm(40),
                2: Parameter::Pos(50),
            }
        );
    }

    #[test]
    fn code_add_mult() {
        let (mut sim, _, _) = IntcodeSim::new();
        sim.load(vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]);
        sim.execute().unwrap();
        assert_eq!(
            sim.get_memory_as_vec(),
            vec! {3500,9,10,70,2,3,11,0,99,30,40,50}
        );

        sim.load(vec![1, 0, 0, 0, 99]);
        sim.execute().unwrap();
        assert_eq!(sim.get_memory_as_vec(), vec! {2,0,0,0,99});

        sim.load(vec![2, 3, 0, 3, 99]);
        sim.execute().unwrap();
        assert_eq!(sim.get_memory_as_vec(), vec! {2,3,0,6,99});

        sim.load(vec![2, 4, 4, 5, 99, 0]);
        sim.execute().unwrap();
        assert_eq!(sim.get_memory_as_vec(), vec! {2,4,4,5,99,9801});

        sim.load(vec![1, 1, 1, 4, 99, 5, 6, 0, 99]);
        sim.execute().unwrap();
        assert_eq!(sim.get_memory_as_vec(), vec! {30,1,1,4,2,5,6,0,99});
    }

    #[test]
    fn code_imm_params() {
        let (mut sim, _, _) = IntcodeSim::new();
        sim.load(vec![1101, 100, -1, 4, 0]);
        sim.execute().unwrap();
        assert_eq!(sim.get_memory_as_vec(), vec! {1101,100,-1,4,99});

        sim.load(vec![1002, 4, 3, 4, 33]);
        sim.execute().unwrap();
        assert_eq!(sim.get_memory_as_vec(), vec! {1002,4,3,4,99});
    }

    #[test]
    fn code_jumping() {
        // Jumping
        // jmp if true
        let (mut sim, _, _) = IntcodeSim::new();
        sim.load(vec![1105, 1, 7, 1101, 0, 123, 8, 99, -1]);
        sim.execute().unwrap();
        // assert_eq!(sim._memory[8], -1);
        assert_eq!(sim.read_mem(&Parameter::Pos(8)), -1);

        // no jump because 0
        sim.load(vec![1105, 0, 7, 1101, 0, 123, 8, 99, -1]);
        sim.execute().unwrap();
        // assert_eq!(sim._memory[8], 123);
        assert_eq!(sim.read_mem(&Parameter::Pos(8)), 123);

        // no jump because 0
        sim.load(vec![1106, 1, 7, 1101, 0, 123, 8, 99, -1]);
        sim.execute().unwrap();
        // assert_eq!(sim._memory[8], 123);
        assert_eq!(sim.read_mem(&Parameter::Pos(8)), 123);

        // no jump because 0
        sim.load(vec![1106, 0, 7, 1101, 0, 123, 8, 99, -1]);
        sim.execute().unwrap();
        // assert_eq!(sim._memory[8], -1);
        assert_eq!(sim.read_mem(&Parameter::Pos(8)), -1);
    }

    #[test]
    fn code_conditionals() {
        // Conditionals
        let (mut sim, _, _) = IntcodeSim::new();
        sim.load(vec![1107, 1, 2, 5, 99, -1]);
        sim.execute().unwrap();
        assert_eq!(sim._memory[5], 1);

        sim.load(vec![1107, 2, 1, 5, 99, -1]);
        sim.execute().unwrap();
        assert_eq!(sim._memory[5], 0);

        // Equals
        sim.load(vec![1108, 1, 1, 5, 99, -1]);
        sim.execute().unwrap();
        assert_eq!(sim._memory[5], 1);

        sim.load(vec![1108, 1, 2, 5, 99, -1]);
        sim.execute().unwrap();
        assert_eq!(sim._memory[5], 0);
    }

    #[test]
    fn code_conditionals_2() {
        let (mut sim, inp, outp) = IntcodeSim::new();
        sim.load(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]);
        inp.send(8).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1);

        sim.load(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]);
        inp.send(9).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 0);
    }

    #[test]
    fn code_jump() {
        let (mut sim, inp, outp) = IntcodeSim::new();
        sim.load(vec![
            3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9,
        ]);
        inp.send(0).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 0);

        sim.load(vec![
            3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9,
        ]);
        inp.send(4).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1);

        sim.load(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1]);
        inp.send(4).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1);
    }

    #[test]
    fn code_below_8_prog() {
        let (mut sim, inp, outp) = IntcodeSim::new();
        sim.load(vec![
            3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0,
            0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4,
            20, 1105, 1, 46, 98, 99,
        ]);
        inp.send(5).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 999);

        sim.load(vec![
            3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0,
            0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4,
            20, 1105, 1, 46, 98, 99,
        ]);
        inp.send(8).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1000);
    }

    #[test]
    fn code_base_manip() {
        let (mut sim, _, _) = IntcodeSim::new();
        sim.load(vec![109, 123, 99]);
        sim.execute().unwrap();
        assert_eq!(sim._base, 123);
        sim.load(vec![209, 3, 99, 123]);
        sim.execute().unwrap();
        assert_eq!(sim._base, 123);
    }

    #[test]
    fn code_self_replicating() {
        let (mut sim, _, outp) = IntcodeSim::new();
        sim.load(vec![
            109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99,
        ]);
        // println!{"memory {:?}", sim._memory};
        sim.execute().unwrap();
        let mut outv: Vec<isize> = Vec::new();
        for _ in 0..16 {
            outv.push(outp.recv().unwrap());
        }
        assert_eq!(
            outv,
            vec! {109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99}
        );
    }

    #[test]
    fn code_large_nrs() {
        let (mut sim, _, outp) = IntcodeSim::new();
        sim.load(vec![1102, 34915192, 34915192, 7, 4, 7, 99, 0]);
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1219070632396864);

        sim.load(vec![104, 1125899906842624, 99]);
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1125899906842624);
    }

    #[test]
    fn code_base() {
        let (mut sim, _, outp) = IntcodeSim::new();
        sim.load(vec![109, 19, 204, -34, 99]);
        sim._base = 2000;
        sim.write_to(&Parameter::Pos(1985), 1234).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 1234);
        assert_eq!(sim.get_memory_as_vec().len(), 1985 + 1);
    }

    #[test]
    fn code_high_mem_access() {
        let (mut sim, _, _) = IntcodeSim::new();
        sim.load(vec![1101, 100, 23, 10000, 99]);
        sim.execute().unwrap();
        // assert_eq!(*sim._memory.get(10000).unwrap(), 123);
        assert_eq!(sim.read_mem(&Parameter::Pos(10000)), 123);

        let (mut sim, _, outp) = IntcodeSim::new();
        sim.load(vec![4, 10000, 99]);
        sim.write_to(&Parameter::Pos(10000), 123).unwrap();
        sim.execute().unwrap();
        assert_eq!(outp.recv().unwrap(), 123);
    }

    #[test]
    fn code_rel_target_write() {
        let (mut sim, _, _) = IntcodeSim::new();
        sim.load(vec![109, 20, 21101, 100, 23, 10, 99]);
        sim.execute().unwrap();
        // assert_eq!(*sim._memory.get(&30).unwrap(), 123);
        assert_eq!(sim.read_mem(&Parameter::Pos(30)), 123);
    }

    #[test]
    fn error_test() {
        let (mut sim, _, _) = IntcodeSim::new();
        assert_eq! {sim.execute().unwrap_err(),
        ExecutionError::NoProgram};
        sim.load(vec![98, 0, 0, 0, 99]);
        assert_eq! {sim.execute().unwrap_err(),
        ExecutionError::UnexpectedOpcode{opc: 98, index: 0}};
        sim.load(vec![501, 0, 0, 0, 99]);
        assert_eq! {sim.execute().unwrap_err(),
        ExecutionError::ParsingError{instr: 501}};
        sim.load(vec![10001, 0, 0, 0, 99]);
        assert_eq! {sim.execute().unwrap_err(),
        ExecutionError::WriteModeError{pc: 0}};
        // TODO more errors to test
    }
}
