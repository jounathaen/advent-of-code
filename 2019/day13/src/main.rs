extern crate intcode_sim;

use custom_error::custom_error;
use intcode_sim::{IntcodeSim, Signal};
use std::{collections::HashMap, error, fs, thread, time::Duration};

custom_error! {#[derive(PartialEq,PartialOrd)] ArcadeError
    ParsingError{inp:isize} = "Could not parse Input: {inp}",
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum GameElement {
    Empty,
    Wall,
    Block,
    Paddle,
    Ball,
}

// #[derive(Debug, PartialEq, Clone, Copy)]
// struct GameState {
// paddle_pos: isize,
// ball_pos: isize,
// score: isize,
// }

fn print_screen(elements: &HashMap<(usize, usize), GameElement>) {
    let mut size_x: usize = 5;
    let mut size_y: usize = 5;

    for e in elements.iter() {
        if (e.0).0 > size_x {
            size_x = (e.0).0;
        }
        if (e.0).1 > size_y {
            size_y = (e.0).1;
        }
    }
    size_x += 1;
    size_y += 1;
    let mut buf = vec![vec!["."; size_x]; size_y];
    for p in elements.iter() {
        let x = (p.0).0;
        let y = (p.0).1;
        if x < size_x && y < size_y {
            buf[y][x] = match p.1 {
                GameElement::Empty => " ",
                GameElement::Wall => "+",
                GameElement::Block => "▮",
                GameElement::Paddle => "=",
                GameElement::Ball => "o",
            };
        }
    }
    println! {"> Screen:"};
    for y in 0..size_y {
        for x in 0..size_x {
            print! {"{}", buf[y][x]};
        }
        println! {};
    }
    println! {};
}

fn parse(inp: &[isize; 3]) -> Result<((usize, usize), GameElement), ArcadeError> {
    let elem = match inp[2] {
        0 => GameElement::Empty,
        1 => GameElement::Wall,
        2 => GameElement::Block,
        3 => GameElement::Paddle,
        4 => GameElement::Ball,
        i => return Err(ArcadeError::ParsingError { inp: i }),
    };
    if inp[0] < 0 || inp[1] < 0 {
        return Err(ArcadeError::ParsingError { inp: inp[0] });
    }
    let x = inp[0] as usize;
    let y = inp[1] as usize;
    Ok(((x, y), elem))
}

fn part1(code: Vec<isize>) {
    let (mut arcade, _, outp) = IntcodeSim::new();
    arcade.load(code);
    let arcade_thread = {
        thread::spawn(move || {
            arcade.execute().unwrap();
        })
    };

    let cabinet_thread = {
        thread::spawn(move || {
            let mut elems = HashMap::new();
            let mut outputs = [99; 3];
            let mut ring_index = 0;
            'outer: loop {
                match outp.recv().unwrap() {
                    Signal::Output(i) => {
                        outputs[ring_index] = i;
                        ring_index += 1;
                        if ring_index == 3 {
                            let parsed = parse(&outputs).unwrap();
                            elems.insert(parsed.0, parsed.1);
                            ring_index = 0;
                        }
                    }
                    Signal::WaitForInput => panic!("No Input expected here"),
                    Signal::ProgramHalted => break 'outer,
                }
                // print!("{}[2J", 27 as char);
                // print_screen(&elems);
                // thread::sleep(Duration::from_millis(10));
            }
            elems
        })
    };
    arcade_thread.join().unwrap();
    let elems = cabinet_thread.join().unwrap();
    let nr_block = elems
        .iter()
        .filter(|e| *(e.1) == GameElement::Block)
        .count();
    // print_screen(&elems);
    println! {"Blocks on screen: {:?}", nr_block};
}

fn part2(mut code: Vec<isize>) {
    let (mut arcade, inp, outp) = IntcodeSim::new();
    code[0] = 2;
    arcade.load(code);
    let arcade_thread = {
        thread::spawn(move || {
            arcade.execute().unwrap();
        })
    };

    let cabinet_thread = {
        thread::spawn(move || {
            let mut elems = HashMap::new();
            let mut score = 0;
            let mut ball_x = 0;
            let mut paddle_x = 0;
            let mut outputs = [99; 3];
            let mut ring_index = 0;
            'outer: loop {
                match outp.recv().unwrap() {
                    Signal::Output(i) => {
                        outputs[ring_index] = i;
                        ring_index += 1;
                        if ring_index == 3 {
                            if outputs[0] == -1 && outputs[1] == 0 {
                                score = outputs[2];
                            } else {
                                let parsed = parse(&outputs).unwrap();
                                elems.insert(parsed.0, parsed.1);
                                match parsed.1 {
                                    GameElement::Ball => {
                                        ball_x = outputs[0];
                                    }
                                    GameElement::Paddle => {
                                        paddle_x = outputs[0];
                                    }
                                    _ => {}
                                }
                                // print!("{}[2J", 27 as char);
                                // println! {"score {}", score};
                                // print_screen(&elems);
                                // thread::sleep(Duration::from_millis(10));
                            }
                            ring_index = 0;
                        }
                    }
                    Signal::WaitForInput => {
                        inp.send(if ball_x < paddle_x {
                            -1
                        } else if ball_x > paddle_x {
                            1
                        } else {
                            0
                        })
                        .unwrap();
                    }
                    Signal::ProgramHalted => break 'outer,
                }
            }
            score
        })
    };
    arcade_thread.join().unwrap();
    let score = cabinet_thread.join().unwrap();
    println! {"High Score: {:?}", score};
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut code = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        code.append(&mut tmp_inp_vec);
    }

    part1(code.clone());
    part2(code.clone());

    Ok(())
}
