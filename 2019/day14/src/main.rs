extern crate custom_error;
use custom_error::custom_error;
use std::{cmp::PartialOrd, collections::HashMap, collections::VecDeque, error, fmt, fs};

custom_error! {#[derive(PartialEq,PartialOrd)] NanofactError
    ParsingError = "Cannot parse str to Asteriod Map",
    OutOfBounds = "Out of Bounds Access",
}

#[derive(PartialEq, Clone)]
struct Chemical {
    quantity: i64,
    name: String,
}
impl fmt::Debug for Chemical {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Chemical: {} {}", self.quantity, self.name)
    }
}

#[derive(PartialEq, Clone)]
struct Reaction {
    output: Chemical,
    input: VecDeque<Chemical>,
}

impl Reaction {
    fn from(s: &str) -> Result<Self, NanofactError> {
        let reaction: Vec<VecDeque<Chemical>> = s
            .split("=>")
            .map(|e| {
                e.trim()
                    .split(",")
                    .map(|e| e.trim().split(" ").collect::<Vec<&str>>())
                    .map(|v| Chemical {
                        quantity: v[0].parse::<i64>().unwrap(),
                        name: v[1].trim().to_string(),
                    })
                    .collect()
            })
            .collect();
        Ok(Reaction {
            output: reaction[1][0].clone(),
            input: reaction[0].clone(),
        })
    }

    // return parameter is surplus of output materials
    fn scale_to(&mut self, target: i64) -> Option<Chemical> {
        let scaling_fact = (target + self.output.quantity - 1) / self.output.quantity;
        let surplus = self.output.quantity * scaling_fact - target;
        self.output.quantity *= scaling_fact;
        for c in self.input.iter_mut() {
            c.quantity *= scaling_fact;
        }
        if surplus > 0 {
            Some(Chemical {
                name: self.output.name.clone(),
                quantity: surplus,
            })
        } else {
            None
        }
    }

    fn shorten(&mut self) {
        let mut i = self.input.len();
        // self.input.sort(|a, b| a.name.cmp(&b.name));
        let mut new_inp = VecDeque::new();
        while i > 0 {
            let chem = self.input[0].name.clone();
            let nr_elems = self.input.iter().filter(|c| c.name == chem).count();
            let total_quantity = self
                .input
                .iter()
                .filter(|c| c.name == chem)
                .map(|c| c.quantity)
                .sum();
            self.input.retain(|c| c.name != chem);
            new_inp.push_back(Chemical {
                quantity: total_quantity,
                name: chem,
            });
            i -= nr_elems;
        }
        self.input = new_inp;
    }

    // return parameter is surplus of output materials
    fn scrap_surplus(&mut self, surplus_chemicals: &mut HashMap<String, i64>) {
        for c in self.input.iter_mut() {
            if let Some(x) = surplus_chemicals.get_mut(&c.name) {
                let surplus_taken = *x.min(&mut c.quantity);
                *x -= surplus_taken;
                c.quantity -= surplus_taken;
            }
        }

        surplus_chemicals.retain(|_, c| *c != 0);
        self.input.retain(|c| c.quantity != 0);
    }
}
impl fmt::Debug for Reaction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for e in self.input.iter() {
            write!(f, "{} {}, ", e.quantity, e.name)?;
        }
        write!(f, "=> {} {}", self.output.quantity, self.output.name)
    }
}

fn replace_chemical(
    reaction: &mut Reaction,
    reaction_book: &HashMap<String, Reaction>,
    mut surplus_chemicals: &mut HashMap<String, i64>,
) -> i64 {
    while !(reaction.input.len() == 1 && reaction.input[0].name == "ORE") {
        let chem = &reaction.input[0].name;
        if chem != &"ORE".to_string() {
            let qty = reaction.input[0].quantity;
            let mut new_reaction = reaction_book.get(chem).unwrap().clone();
            // A reaction might have some materials surplus
            if let Some(surplus) = new_reaction.scale_to(qty) {
                if let Some(x) = surplus_chemicals.get_mut(&surplus.name) {
                    // already surplus
                    *x += surplus.quantity;
                } else {
                    surplus_chemicals.insert(surplus.name.clone(), surplus.quantity);
                }
            }
            new_reaction.scrap_surplus(&mut surplus_chemicals);
            for new_i in new_reaction.input.iter() {
                reaction.input.push_back(new_i.clone());
            }
            reaction.input.pop_front();
            reaction.shorten();
        } else {
            reaction.shorten();
            let ore = reaction.input[0].clone();
            reaction.input.pop_front();
            reaction.input.push_back(ore);
        }
    }
    reaction.input[0].quantity
}

fn part_1(reaction_book: &HashMap<String, Reaction>) {
    let mut main_reaction = reaction_book.get(&"FUEL".to_string()).unwrap().clone();
    let mut surplus_chemicals = HashMap::new();
    replace_chemical(&mut main_reaction, &reaction_book, &mut surplus_chemicals);
    println! {"main reac {:?}", main_reaction};
}

fn part_2(reaction_book: &HashMap<String, Reaction>) {
    let main_reaction = reaction_book.get(&"FUEL".to_string()).unwrap().clone();
    let mut fuel_cnt = 1;
    let mut ore_cnt = 0;
    // find upper bound
    while ore_cnt < 1_000_000_000_000 {
        let mut r = main_reaction.clone();
        fuel_cnt *= 5;
        r.scale_to(fuel_cnt);
    }
    // approximate by upper/lower bound
    let mut upper_bound = fuel_cnt;
    let mut lower_bound = fuel_cnt / 5;
    while upper_bound - lower_bound > 1 {
        let mut r = main_reaction.clone();
        fuel_cnt = lower_bound + (upper_bound - lower_bound) / 2;
        r.scale_to(fuel_cnt);
        ore_cnt = replace_chemical(&mut r, &reaction_book, &mut HashMap::new());
        if ore_cnt > 1_000_000_000_000 {
            upper_bound = fuel_cnt;
        } else {
            lower_bound = fuel_cnt;
        }
    }
    println!("max fuel with 1 tril: {}", lower_bound);
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut reaction_book = HashMap::new();
    for line in f.lines() {
        let reaction = Reaction::from(line).unwrap();
        reaction_book.insert(reaction.output.name.clone(), reaction);
    }

    part_1(&reaction_book);
    part_2(&reaction_book);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parser() {
        let reaction = Reaction::from("  3 A, 4 B   => 1 AB ").unwrap();
        assert_eq! {reaction,
        Reaction{
            output: Chemical{
                quantity: 1,
                name: "AB".to_string()
            },
            input: VecDeque::from(vec!{
                Chemical{quantity: 3, name: "A".to_string()},
                Chemical{quantity: 4, name: "B".to_string()}
            }
                   )
        }
        };
    }

    #[test]
    fn scaling() {
        let mut reaction = Reaction::from("3 A, 4 B   => 2 AB").unwrap();
        assert_eq! {reaction.scale_to(4), None};

        reaction = Reaction::from("3 A, 4 B   => 2 AB").unwrap();
        let surplus = Chemical {
            name: "AB".to_string(),
            quantity: 1,
        };
        assert_eq! {reaction.scale_to(3), Some(surplus)};
    }

    #[test]
    fn shortening() {
        let mut reaction = Reaction::from("2 A, 6 B, 3 A, 4 B => 2 AB").unwrap();
        let solution = Reaction::from("5 A, 10 B => 2 AB").unwrap();
        reaction.shorten();
        assert_eq! {reaction, solution};

        let mut reaction = Reaction::from("2 A, 6 B, 2 C, 2 A, 3 A, 4 B => 2 AB").unwrap();
        let solution = Reaction::from("7 A, 10 B, 2 C => 2 AB").unwrap();
        reaction.shorten();
        assert_eq! {reaction, solution};
    }

    #[test]
    fn scrap() {
        let mut reaction_book = HashMap::new();
        let f = concat! {
            "8 ORE => 2 A\n",
            "8 ORE => 3 B\n",
            "7 ORE => 2 C\n",
        };
        for line in f.lines() {
            let reaction = Reaction::from(line).unwrap();
            reaction_book.insert(reaction.output.name.clone(), reaction);
        }
        let mut reaction = Reaction::from("3 C => 2 AB").unwrap();
        let mut scrap: HashMap<String, i64> = HashMap::new();
        scrap.insert("C".to_string(), 2);

        reaction.scrap_surplus(&mut scrap);
        let solution = Reaction::from("1 C => 2 AB").unwrap();
        assert_eq! {reaction, solution};
        replace_chemical(&mut reaction, &reaction_book, &mut scrap);

        let mut sol_scrap: HashMap<String, i64> = HashMap::new();
        sol_scrap.insert("C".to_string(), 1);
        assert_eq! {scrap, sol_scrap};
    }

    #[test]
    fn replace_chemical_1() {
        let mut reaction_book = HashMap::new();
        let f = concat! {
            "8 ORE => 2 A\n",
            "8 ORE => 3 B\n",
            "7 ORE => 2 C\n",
            "4 C, 1 A => 1 FUEL\n",
        };
        for line in f.lines() {
            let reaction = Reaction::from(line).unwrap();
            reaction_book.insert(reaction.output.name.clone(), reaction);
        }
        let mut main_reaction = reaction_book.get(&"FUEL".to_string()).unwrap().clone();
        // println! {"main reac \n{:?}", main_reaction};
        replace_chemical(&mut main_reaction, &reaction_book, &mut HashMap::new());
        main_reaction.shorten();
        let solved_reaction = Reaction::from("22 ORE => 1 FUEL").unwrap();
        println! {"main reac \n{:?}", main_reaction};
        // println! {"solved_reaction reac \n{:?}", main_reaction};
        assert_eq! {main_reaction, solved_reaction};
    }

    #[test]
    fn replace_chemical_2() {
        let mut reaction_book = HashMap::new();
        let f = concat! {
            "8 ORE => 2 A\n",
            "4 ORE => 4 B\n",
            "8 B => 3 D\n",
            "7 A => 2 C\n",
            "4 C, 6 D => 1 FUEL\n",
        };
        for line in f.lines() {
            let reaction = Reaction::from(line).unwrap();
            reaction_book.insert(reaction.output.name.clone(), reaction);
        }
        let mut surplus_chemicals = HashMap::new();
        let mut main_reaction = reaction_book.get(&"FUEL".to_string()).unwrap().clone();
        replace_chemical(&mut main_reaction, &reaction_book, &mut surplus_chemicals);
        let solved_reaction = Reaction::from("72 ORE => 1 FUEL").unwrap();
        // let solved_reaction = Reaction::from("14 A, 16 B => 1 FUEL").unwrap();
        assert_eq! {main_reaction, solved_reaction};
    }

    #[test]
    fn replace_chemical_3() {
        let mut reaction_book = HashMap::new();
        let f = concat! {
            "9 ORE => 2 A\n",
            "8 ORE => 3 B\n",
            "7 ORE => 5 C\n",
            "3 A, 4 B => 1 AB\n",
            "5 B, 7 C => 1 BC\n",
            "4 C, 1 A => 1 CA\n",
            "2 AB, 3 BC, 4 CA => 1 FUEL\n",
        };
        for line in f.lines() {
            let reaction = Reaction::from(line).unwrap();
            reaction_book.insert(reaction.output.name.clone(), reaction);
        }
        let mut main_reaction = reaction_book.get(&"FUEL".to_string()).unwrap().clone();
        replace_chemical(&mut main_reaction, &reaction_book, &mut HashMap::new());
        main_reaction.shorten();
        let solved_reaction = Reaction::from("165 ORE => 1 FUEL").unwrap();
        assert_eq! {main_reaction, solved_reaction};
    }

    #[test]
    fn replace_chemical_4() {
        let mut reaction_book = HashMap::new();
        let f = concat! {
            "157 ORE => 5 NZVS\n",
            "165 ORE => 6 DCFZ\n",
            "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL\n",
            "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ\n",
            "179 ORE => 7 PSHF\n",
            "177 ORE => 5 HKGWZ\n",
            "7 DCFZ, 7 PSHF => 2 XJWVT\n",
            "165 ORE => 2 GPVTF\n",
            "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT\n",
        };
        for line in f.lines() {
            let reaction = Reaction::from(line).unwrap();
            reaction_book.insert(reaction.output.name.clone(), reaction);
        }
        let mut main_reaction = reaction_book.get(&"FUEL".to_string()).unwrap().clone();
        replace_chemical(&mut main_reaction, &reaction_book, &mut HashMap::new());
        let solved_reaction = Reaction::from("13312 ORE => 1 FUEL").unwrap();
        assert_eq! {main_reaction, solved_reaction};
    }

    #[test]
    fn replace_chemical_5() {
        let mut reaction_book = HashMap::new();
        let f = concat! {
            "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG\n",
            "17 NVRVD, 3 JNWZP => 8 VPVL\n",
            "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL\n",
            "22 VJHF, 37 MNCFX => 5 FWMGM\n",
            "139 ORE => 4 NVRVD\n",
            "144 ORE => 7 JNWZP\n",
            "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC\n",
            "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV\n",
            "145 ORE => 6 MNCFX\n",
            "1 NVRVD => 8 CXFTF\n",
            "1 VJHF, 6 MNCFX => 4 RFSQX\n",
            "176 ORE => 6 VJHF\n",
        };
        for line in f.lines() {
            let reaction = Reaction::from(line).unwrap();
            reaction_book.insert(reaction.output.name.clone(), reaction);
        }
        let mut main_reaction = reaction_book.get(&"FUEL".to_string()).unwrap().clone();
        replace_chemical(&mut main_reaction, &reaction_book, &mut HashMap::new());
        let solved_reaction = Reaction::from("180697 ORE => 1 FUEL").unwrap();
        assert_eq! {main_reaction, solved_reaction};
    }
}
