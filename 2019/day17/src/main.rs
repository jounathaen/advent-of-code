extern crate intcode_sim;
use intcode_sim::{IntcodeSim, Signal};
use std::{
    error,
    fmt,
    fs,
    thread,
    // time::Duration
};

#[derive(Debug, PartialEq, Clone, Copy)]
enum Dir {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum ImgElem {
    Scaffold,
    Robot(Dir),
    Space,
    Intersection,
}

#[derive(PartialEq, Clone)]
struct Image {
    pixels: Vec<ImgElem>,
    size_x: isize,
    size_y: isize,
}
impl fmt::Debug for Image {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        dbg! {self.size_x};
        dbg! {self.size_y};
        write!(f, "\n")?;
        for y in 0..self.size_y as usize {
            for x in 0..self.size_x as usize {
                match self.pixels[x + y * self.size_x as usize] {
                    ImgElem::Scaffold => write!(f, "#")?,
                    ImgElem::Space => write!(f, ".")?,
                    ImgElem::Robot(Dir::Down) => write!(f, "v")?,
                    ImgElem::Robot(Dir::Up) => write!(f, "^")?,
                    ImgElem::Robot(Dir::Left) => write!(f, "<")?,
                    ImgElem::Robot(Dir::Right) => write!(f, ">")?,
                    ImgElem::Intersection => write!(f, "O")?,
                };
            }
            write!(f, "\n")?;
        }
        write!(f, "\n")
    }
}
impl Image {
    fn new() -> Self {
        Image {
            pixels: Vec::new(),
            size_x: 0,
            size_y: 0,
        }
    }
    fn get(&self, x: isize, y: isize) -> &ImgElem {
        if x < 0 || y < 0 || x >= self.size_x || y >= self.size_y {
            return &ImgElem::Space;
        }
        self.pixels
            .get((x + y * self.size_x as isize) as usize)
            .unwrap_or(&ImgElem::Space)
    }
    fn set(&mut self, x: isize, y: isize, elem: ImgElem) {
        self.pixels[(x + y * self.size_x) as usize] = elem;
    }
    fn from(code: Vec<isize>) -> Self {
        let (mut droid, _, outp) = IntcodeSim::new();
        droid.load(code);
        let droid_thread = {
            thread::spawn(move || {
                droid.execute().unwrap();
            })
        };

        let mut image: Image = Image::new();
        let mut y = 0;
        let mut x = 0;
        loop {
            match outp.recv().unwrap() {
                Signal::Output(o) => {
                    // print!{"{}", o as u8 as char};
                    match o {
                        10 => {
                            if image.size_x == 0 {
                                image.size_x = x;
                            }
                            y += 1;
                        } // LF
                        35 => image.pixels.push(ImgElem::Scaffold), // #
                        46 => image.pixels.push(ImgElem::Space),    // .
                        60 => image.pixels.push(ImgElem::Robot(Dir::Left)), // <
                        62 => image.pixels.push(ImgElem::Robot(Dir::Right)), // >
                        94 => image.pixels.push(ImgElem::Robot(Dir::Up)), // ^
                        118 => image.pixels.push(ImgElem::Robot(Dir::Down)), // v
                        _ => panic! {"Unexpected Output: {}", o},
                    }
                }
                Signal::ProgramHalted => break,
                Signal::WaitForInput => panic! {"Intcode requests input"},
            }
            x += 1;
        }
        image.size_y = y - 1;

        droid_thread.join().unwrap();
        image
    }
    fn get_robot_pos(&self) -> Option<((isize, isize), Dir)> {
        for i in 1..(self.pixels.len()) {
            if let ImgElem::Robot(dir) = self.pixels[i] {
                return Some(((i as isize % self.size_x, i as isize / self.size_x), dir));
            }
        }
        None
    }
    fn contains_scaf_in_dir(&self, dir: Dir, x: isize, y: isize) -> bool {
        (match dir {
            Dir::Up => self.get(x, y - 1),
            Dir::Down => self.get(x, y + 1),
            Dir::Left => self.get(x - 1, y),
            Dir::Right => self.get(x + 1, y),
        } == &ImgElem::Scaffold)
    }
}

fn path_from_image(image: &Image) -> Vec<String> {
    let ((mut x, mut y), mut dir) = image.get_robot_pos().unwrap();
    let mut path: Vec<String> = Vec::new();

    loop {
        // let mut newimg = image.clone();
        // newimg.set(x, y, ImgElem::Intersection);
        // dbg!{newimg};
        // thread::sleep(Duration::from_millis(50));

        let pos_front = match dir {
            Dir::Up => image.get(x, y - 1),
            Dir::Down => image.get(x, y + 1),
            Dir::Left => image.get(x - 1, y),
            Dir::Right => image.get(x + 1, y),
        };
        if pos_front == &ImgElem::Scaffold {
            // step forward
            match dir {
                Dir::Up => y -= 1,
                Dir::Down => y += 1,
                Dir::Left => x -= 1,
                Dir::Right => x += 1,
            }
            path.push("s".to_string());
        } else {
            // turn
            match dir {
                Dir::Up => {
                    if image.contains_scaf_in_dir(Dir::Left, x, y) {
                        dir = Dir::Left;
                        path.push("L".to_string());
                    } else if image.contains_scaf_in_dir(Dir::Right, x, y) {
                        dir = Dir::Right;
                        path.push("R".to_string());
                    } else {
                        break;
                    }
                }
                Dir::Down => {
                    if image.contains_scaf_in_dir(Dir::Right, x, y) {
                        dir = Dir::Right;
                        path.push("L".to_string());
                    } else if image.contains_scaf_in_dir(Dir::Left, x, y) {
                        dir = Dir::Left;
                        path.push("R".to_string());
                    } else {
                        break;
                    }
                }
                Dir::Left => {
                    if image.contains_scaf_in_dir(Dir::Up, x, y) {
                        dir = Dir::Up;
                        path.push("R".to_string());
                    } else if image.contains_scaf_in_dir(Dir::Down, x, y) {
                        dir = Dir::Down;
                        path.push("L".to_string());
                    } else {
                        break;
                    }
                }
                Dir::Right => {
                    if image.contains_scaf_in_dir(Dir::Down, x, y) {
                        dir = Dir::Down;
                        path.push("R".to_string());
                    } else if image.contains_scaf_in_dir(Dir::Up, x, y) {
                        dir = Dir::Up;
                        path.push("L".to_string());
                    } else {
                        break;
                    }
                }
            }
        }
    }
    path
}

fn shorten_path(path: &Vec<String>) -> Vec<String> {
    let mut shortpath = Vec::new();
    let mut count = 0;
    for c in path.iter() {
        if c == &"s" {
            count += 1;
        } else {
            if count != 0 {
                shortpath.push(count.to_string());
                count = 0;
            }
            shortpath.push(c.to_string());
        }
    }
    if count != 0 {
        shortpath.push(count.to_string());
    }
    shortpath
}

fn split_subvec(mut inp: Vec<String>, slice: &Vec<String>) -> Vec<Vec<String>> {
    let mut splitted = Vec::new();
    let mut i = 0;
    while inp.len() - i >= slice.len() {
        let mut matches = true;
        for j in 0..slice.len() {
            if inp[i as usize + j] != slice[j] {
                matches = false;
                break;
            }
        }
        if matches {
            let (sec_1, rem) = inp.split_at_mut(i);
            let (sec_2, rem) = rem.split_at(slice.len());
            splitted.push(sec_1.to_vec());
            splitted.push(sec_2.to_vec());
            inp = rem.to_vec();
            i = 0;
        // inp.drain(i as usize..i as usize + slice.len());
        } else {
            i += 1;
        }
    }
    splitted.push(inp);
    splitted
}

fn find_segments(path: &Vec<String>) -> Option<Vec<Vec<String>>> {
    // required for removing empty elements
    let empty_vec: Vec<String> = Vec::new();
    // Try a segment beginning with the first steps in path
    for i in (2..20).step_by(2) {
        let mut segm_a = path.clone();
        let remainder = segm_a.split_off(i);
        if segm_a.concat().len() > 20 {
            return None;
        }
        // remaining chunks of the path. Can be sorted and shortened
        let mut splitted = split_subvec(remainder, &segm_a);
        splitted.retain(|e| *e != segm_a && *e != empty_vec);
        splitted.sort();
        splitted.dedup();

        // next segment must be a part of the first remaining chunk
        'segmb_loop: for j in (2..=splitted[0].len()).step_by(2) {
            let mut segm_b = splitted[0].clone();
            segm_b.split_off(j);
            if segm_b.concat().len() > 20 {
                break;
            }
            let mut remaining_segments = Vec::new();
            for s in splitted.iter() {
                let further_splitted = split_subvec(s.clone(), &segm_b);
                for f in further_splitted.iter() {
                    if f.concat().len() > 20 {
                        // remainder too long
                        break 'segmb_loop;
                    }
                    remaining_segments.push(f.clone());
                }
            }
            remaining_segments.retain(|e| *e != segm_b && *e != empty_vec);
            remaining_segments.sort();
            remaining_segments.dedup();
            if remaining_segments.len() == 1 {
                let mut retval = Vec::new();
                retval.push(segm_a);
                retval.push(segm_b);
                retval.push(remaining_segments[0].clone());
                return Some(retval);
            }
        }
    }
    None
}

fn replace_by_segments(path: &Vec<String>, segments: &Vec<Vec<String>>) -> Vec<String> {
    let mut outp = Vec::new();
    let mut i = 0;
    'outer: while i < path.len() {
        for segm in 0..segments.len() {
            let mut matches = true;
            for j in 0..segments[segm].len() {
                if path[i + j] != segments[segm][j] {
                    matches = false;
                    break;
                }
            }
            if matches {
                outp.push(String::from(match segm {
                    0 => "A",
                    1 => "B",
                    2 => "C",
                    _ => unreachable!("Not a valid segment nr"),
                }));
                i += segments[segm].len();
                continue 'outer;
            }
        }
        unreachable!("one of the segments must match");
    }
    outp
}

fn part1(mut image: Image) {
    let mut alignment_params = 0;
    for y in 1..image.size_y {
        let y = y as isize;
        for x in 1..image.size_x {
            let x = x as isize;
            if image.get(x, y) == &ImgElem::Scaffold
                && image.get(x, y - 1) == &ImgElem::Scaffold
                && image.get(x, y + 1) == &ImgElem::Scaffold
                && image.get(x + 1, y) == &ImgElem::Scaffold
                && image.get(x - 1, y) == &ImgElem::Scaffold
            {
                alignment_params += x * y;
                image.set(x, y, ImgElem::Intersection);
            }
        }
    }
    // dbg!{&image};
    println! {"Sum of all alignment parameters is: {}", alignment_params};
}

fn part2(image: Image, mut code: Vec<isize>) {
    let mut path = path_from_image(&image);
    // println! {"path {:?}", path};
    path = shorten_path(&path);
    // println! {"short path {:?}", path};
    let segments = find_segments(&path).unwrap();
    // println! {"segments {:?}", segments};
    let main_routine = replace_by_segments(&path, &segments);
    // println! {"main_routine {:?}", main_routine};

    assert_eq!(code[0], 1);
    code[0] = 2;
    let (mut droid, inp, outp) = IntcodeSim::new();
    droid.load(code);
    let droid_thread = {
        thread::spawn(move || {
            droid.execute().unwrap();
        })
    };

    // Robot prints map once:
    while let Signal::Output(_) = outp.recv().unwrap() {}

    // Send the main Routine
    for b in main_routine.join(",").bytes() {
        inp.send(b as isize).unwrap();
    }
    inp.send(10).unwrap(); // newline
                           // Send the subroutines
    for s in segments.iter() {
        for b in s.join(",").bytes() {
            inp.send(b as isize).unwrap();
        }
        inp.send(10).unwrap(); // newline
    }
    // Skip the IO from the Routines
    for _ in 0..4 {
        while { outp.recv().unwrap() == Signal::WaitForInput } {}
        while let Signal::Output(_) = outp.recv().unwrap() {}
    }

    // Request for video
    inp.send('n' as isize).unwrap(); // no_video
    inp.send(10).unwrap(); // newline
    while { outp.recv().unwrap() == Signal::WaitForInput } {} // Skip output

    // The robot outputs more stuff, but we only need the last value
    let mut stardust = 0;
    while let Signal::Output(out) = outp.recv().unwrap() {
        stardust = out;
    }
    println!("The amount of stardust is {}", stardust);
    droid_thread.join().unwrap();
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut code = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        code.append(&mut tmp_inp_vec);
    }

    let image = Image::from(code.clone());
    // dbg!(&image);

    part1(image.clone());
    part2(image, code);

    Ok(())
}

#[cfg(test)]
mod test {
    macro_rules! vec_of_strings {
        ($($x:expr),*) => (vec![$($x.to_string()),*]);
    }

    use super::*;

    #[test]
    fn get_test() {
        let img = Image {
            pixels: vec![
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Robot(Dir::Down),
            ],
            size_x: 3,
            size_y: 3,
        };
        assert_eq!(img.get(1, 1), &ImgElem::Scaffold);
        assert_eq!(img.get(0, 1), &ImgElem::Space);
        assert_eq!(img.get(1, 0), &ImgElem::Scaffold);
        assert_eq!(img.get(2, 2), &ImgElem::Robot(Dir::Down));
    }

    #[test]
    fn robot_pos() {
        let img = Image {
            pixels: vec![
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Robot(Dir::Down),
            ],
            size_x: 3,
            size_y: 3,
        };
        assert_eq!(img.get_robot_pos(), Some(((2, 2), Dir::Down)));
    }

    #[test]
    fn contains_scaf_in_dir_test() {
        let img = Image {
            pixels: vec![
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Space,
            ],
            size_x: 3,
            size_y: 3,
        };
        println!("{:?}", img);
        assert!(img.contains_scaf_in_dir(Dir::Up, 1, 1));
        assert!(img.contains_scaf_in_dir(Dir::Right, 1, 1));
        assert!(!img.contains_scaf_in_dir(Dir::Down, 1, 1));
    }
}
