extern crate intcode_sim;

use crossbeam_channel::{Receiver, Sender};
// use custom_error::custom_error;
use intcode_sim::{IntcodeSim, Signal};
use std::{collections::HashMap, collections::HashSet, collections::VecDeque, error, fs, thread, time::Duration};

// custom_error! {#[derive(PartialEq,PartialOrd)] ArcadeError
// ParsingError{inp:isize} = "Could not parse Input: {inp}",
// }

#[derive(Debug, PartialEq, Clone, Copy)]
enum MapElement {
    Empty,
    Wall,
    Oxygen,
    Visited,
}

#[derive(Debug, PartialEq, Clone, Copy, Eq, Hash)]
struct Point {
    x: isize,
    y: isize,
}
impl Point {
    fn new(x: isize, y: isize) -> Self {
        Point { x: x, y: y }
    }
    fn get_neighbor(&self, direction: isize) -> Point {
        match direction {
            1 => Point::new(self.x, self.y + 1), // North
            2 => Point::new(self.x, self.y - 1), // South
            3 => Point::new(self.x - 1, self.y), // West
            4 => Point::new(self.x + 1, self.y), // East
            e => panic! {"{} is not a valid direction", e},
        }
    }
}

fn print_map(elements: &HashMap<Point, MapElement>, droid: &Point) {
    let mut size_x: isize = 0;
    let mut size_y: isize = 0;
    let mut size_x_min: isize = 0;
    let mut size_y_min: isize = 0;

    let size_x_max: isize = 60;
    let size_y_max: isize = 30;

    for (p, _e) in elements.iter() {
        if p.x > size_x && p.x < size_x_max {
            size_x = p.x;
        }
        if p.y > size_y && p.y < size_y_max {
            size_y = p.y;
        }
        if p.x < size_x_min && -size_x_max < p.x {
            size_x_min = p.x;
        }
        if p.y < size_y_min && -size_y_max < p.y {
            size_y_min = p.y;
        }
    }
    if droid.x > size_x && droid.x < size_x_max {
        size_x = droid.x;
    }
    if droid.y > size_y && droid.y < size_y_max {
        size_y = droid.y;
    }
    if droid.x < size_x_min && -size_x_max < droid.x {
        size_x_min = droid.x;
    }
    if droid.y < size_y_min && -size_y_max < droid.y {
        size_y_min = droid.y;
    }
    // println! {"Margins: x {} xmin {}, y {} y min {}", size_x, size_x_min, size_y, size_y_min};
    size_x += 1;
    size_y += 1;
    let mut buf = vec![vec!["."; (size_x - size_x_min) as usize]; (size_y - size_y_min) as usize];
    for (p, e) in elements.iter() {
        if -size_x_max < p.x && p.x < size_x_max && -size_y_max < p.y && p.y < size_y_max {
            let x = (p.x - size_x_min) as usize;
            let y = (p.y - size_y_min) as usize;
            // println! {"p: {:?}, x {}, y {}", p, x, y};
            buf[y][x] = match e {
                MapElement::Empty => " ",
                MapElement::Wall => "█",
                MapElement::Oxygen => "O",
                MapElement::Visited => "/",
            };
        }
    }
    println! {"droid {:?}", droid};
    if -size_x_max < droid.x
        && droid.x < size_x_max
        && -size_y_max < droid.y
        && droid.y < size_y_max
    {
        buf[(droid.y - size_y_min) as usize][(droid.x - size_x_min) as usize] = "D";
    }
    buf[-size_y_min as usize][-size_x_min as usize] = "X";
    println! {"> Screen:"};
    for y in 0..(size_y - size_y_min) as usize {
        for x in 0..(size_x - size_x_min) as usize {
            print! {"{}", buf[(size_y - size_y_min) as usize - y -1][x]};
        }
        println! {};
    }
    println! {};
}

fn reverse_dir(i: isize) -> isize {
    match i {
        1 => 2,
        2 => 1,
        3 => 4,
        4 => 3,
        e => panic! {"{} is not a valid direction ", e},
    }
}

// Depth First Searh
fn explore_maze(
    pos: &Point,
    mut map: &mut HashMap<Point, MapElement>,
    droid_snd: &Sender<isize>,
    droid_outp: &Receiver<intcode_sim::Signal>,
) {
    for dir in 1..=4 {
        let neighbor = pos.get_neighbor(dir);
        if map.get(&neighbor).is_none() {
            let outp = droid_outp.recv().unwrap();
            assert_eq! {outp, Signal::WaitForInput};
            droid_snd.send(dir).unwrap();
            let elem = match droid_outp.recv().unwrap() {
                Signal::Output(0) => MapElement::Wall,
                Signal::Output(1) => MapElement::Empty,
                Signal::Output(2) => MapElement::Oxygen,
                e => panic! {"{:?} is not a valid element ", e},
            };
            map.insert(neighbor, elem);
            // Do recursion if elem is not a wall
            match elem {
                MapElement::Wall => {}
                MapElement::Oxygen | MapElement::Empty => {
                    explore_maze(&neighbor, &mut map, droid_snd, droid_outp);
                    let back_dir = reverse_dir(dir);
                    let outp = droid_outp.recv().unwrap();
                    assert_eq! {outp, Signal::WaitForInput}
                    droid_snd.send(back_dir).unwrap();
                    let outp = droid_outp.recv().unwrap();
                    assert!(outp == Signal::Output(1) || outp == Signal::Output(2));
                }
                MapElement::Visited => panic!("Why are you searching an already exlored maze?"),
            }
        }
        // print!("{}[2J", 27 as char);
        // print_map(&map, &pos);
        // thread::sleep(Duration::from_millis(20));
    }
}

// Breadth first search
fn find_oxygen(maze: &mut HashMap<Point, MapElement>, startpoint: Point) -> usize {
    let mut queue = VecDeque::new();
    let mut depth = 0;
    let mut visited: HashSet<Point> = HashSet::new();
    let mut current_node = startpoint;
    while maze.get(&current_node).unwrap() != &MapElement::Oxygen {
        visited.insert(current_node);
        maze.insert(current_node, MapElement::Visited);
        for dir in 1..=4 {
            let neighbor = current_node.get_neighbor(dir);
            if maze.get(&neighbor).unwrap() != &MapElement::Wall && visited.get(&neighbor).is_none() {
                queue.push_back((neighbor, depth+1));
            }
        }
        let pop_val = queue.pop_front().unwrap();
        current_node = pop_val.0;
        depth = pop_val.1;
        // println!("Depth: {}", depth);
        // print_map(&maze, &current_node);
        // thread::sleep(Duration::from_millis(50));
    }
    depth
}

// Breadth first search
fn flood_oxygen(maze: &mut HashMap<Point, MapElement>, startpoint: Point) -> usize {
    let mut queue: VecDeque<(Point, usize)> = VecDeque::new();
    queue.push_back((startpoint, 0));
    let mut depth = 0;
    let mut visited: HashSet<Point> = HashSet::new();
    let mut current_node;
    while queue.len() != 0 {
        let pop_val = queue.pop_front().unwrap();
        current_node = pop_val.0;
        depth = pop_val.1;
        visited.insert(current_node);
        maze.insert(current_node, MapElement::Visited);
        for dir in 1..=4 {
            let neighbor = current_node.get_neighbor(dir);
            if maze.get(&neighbor).unwrap() != &MapElement::Wall && visited.get(&neighbor).is_none() {
                queue.push_back((neighbor, depth+1));
            }
        }
        // println!("Depth: {}", depth);
        // print_map(&maze, &current_node);
        // thread::sleep(Duration::from_millis(30));
    }
    depth
}

fn part1(mut map: HashMap<Point, MapElement>) {
    let depth = find_oxygen(&mut map, Point::new(0,0));
    println!("Found Oxygen at depth {}", depth);
}

fn part2(mut map: HashMap<Point, MapElement>) {
    let oxygen_loc = map.iter().filter(|v| *v.1 == MapElement::Oxygen).take(1).next().unwrap().0.clone();
    let time = flood_oxygen(&mut map, oxygen_loc);
    println!("Oxygen needed {} mins to propagate", time);
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut code = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        code.append(&mut tmp_inp_vec);
    }

    let mut map: HashMap<Point, MapElement> = HashMap::new();
    map.insert(Point::new(0, 0), MapElement::Empty);

    let (mut droid, inp, outp) = IntcodeSim::new();
    droid.load(code);
    let droid_thread = {
        thread::spawn(move || {
            droid.execute().unwrap();
        })
    };

    explore_maze(&Point::new(0, 0), &mut map, &inp, &outp);
    // print_map(&map, &Point::new(0, 0));
    // droid_thread.join().unwrap();

    part1(map.clone());
    part2(map.clone());

    Ok(())
}
