extern crate intcode_sim;
use intcode_sim::IntcodeSim;
use std::{error, fs};

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut inp_vec = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        inp_vec.append(&mut tmp_inp_vec);
    }

    let (mut sim, inp, outp) = IntcodeSim::new();
    sim.load(inp_vec.clone());
    inp.send(1)?;
    sim.execute()?;
    println!("BOOST Program Output: {}", outp.recv()?);

    sim.load(inp_vec);
    inp.send(2)?;
    sim.execute()?;
    println!("BOOST Program Output: {}", outp.recv()?);

    Ok(())
}
