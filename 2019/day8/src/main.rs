extern crate custom_error;
use custom_error::custom_error;
use std::{
    error,
    fs,
};

custom_error!{#[derive(PartialEq,PartialOrd)] ImgError
    ParsingError = "Cannot parse str to image",
    OutOfBounds = "Out of Bounds Access",
}


#[derive(Debug, PartialEq)]
struct Image {
    pixels: Vec<u32>,
    size_x: usize,
    size_y: usize,
}

impl Image {
    fn from(s: &str, x: usize, y: usize) -> Result<Self, ImgError> {
        let mut image = Image{pixels: Vec::new(), size_x: x, size_y: y};
        for l in s.lines() {
            if l.len() % (x * y) != 0 {
                return Err(ImgError::ParsingError);
            }
            image.pixels = l.chars()
                .map(|c| c.to_digit(10).ok_or_else(||ImgError::ParsingError))
                .collect::<Result<Vec<u32>,_>>()?;
        }
        Ok(image)
    }

    fn layer(&self, l: usize) -> Result<impl Iterator<Item=&u32>, ImgError> {
        if l < self.pixels.len() / (self.size_x * self.size_y) {
            Ok(self.pixels.iter().skip(l * self.size_x * self.size_y).take(self.size_x * self.size_y))
        }
        else {
            return Err(ImgError::OutOfBounds{});
        }
    }

    fn nr_layers(&self) -> usize {
        self.pixels.len() / (self.size_x * self.size_y)
    }

    fn render(&self) -> Vec<u32> {
        let mut render: Vec<u32> = vec![2; self.size_x * self.size_y];
        for l in 0..self.nr_layers() {
            for i in 0..(self.size_x * self.size_y) {
                if render[i] == 2 {
                    render[i] = self.pixels[l * (self.size_x * self.size_y) + i];
                }
            }
        }
        render
    }
}

fn display(img: &Vec<u32>, width: usize) {
    for y in 0..(img.len()/width) {
        for x in 0..width {
            match img[y*width + x] {
                0 => print!{" "},
                1 => print!{"▮"},
                2 => print!{"/"},
                _ => unreachable!{"Invalid Image"},
            }
        }
        println!{};
    }
}


fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let image  = Image::from(&f, 25, 6).unwrap();

    let mut min_zero_layer = (0, std::usize::MAX);

    for i in 0..image.nr_layers() {
        let nr_zero : usize = image.layer(i).unwrap().filter(|j| **j == 0).count();
        if nr_zero < min_zero_layer.1 {
            min_zero_layer = (i, nr_zero);
        }
    }
    let outp = image.layer(min_zero_layer.0).unwrap().filter(|j| **j == 1).count()
             * image.layer(min_zero_layer.0).unwrap().filter(|j| **j == 2).count();
    println!{"Min Zero Layer {:?}, Outp: {}", min_zero_layer, outp};

    display(&image.render(), image.size_x);
    Ok(())
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parser_test1 () {
        let img = Image::from("123456789012", 3, 2).unwrap();
        assert_eq!{img.pixels, [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2]}
    }

    #[test]
    fn parser_test2 () {
        assert_eq!(Image::from("12345a7x9012", 3, 2).unwrap_err(), ImgError::ParsingError);
        assert_eq!(Image::from("12345", 3, 2).unwrap_err(), ImgError::ParsingError);
    }

    #[test]
    fn layer_test1 () {
        let img = Image::from("123456789012", 3, 2).unwrap();
        assert_eq!(img.layer(0).unwrap().collect::<Vec<&u32>>(), vec!{&1,&2,&3,&4,&5,&6});
        assert_eq!(img.layer(1).unwrap().collect::<Vec<&u32>>(), vec!{&7,&8,&9,&0,&1,&2});
        assert_eq!(img.layer(2).err().unwrap(), ImgError::OutOfBounds);
    }

    #[test]
    fn render_test1 () {
        let img = Image::from("0222112222120000", 2, 2).unwrap();
        assert_eq!(img.render(), vec!{0,1,1,0});
    }
}
