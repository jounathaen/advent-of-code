extern crate intcode_sim;
use intcode_sim::IntcodeSim;
use std::{error, fs, thread, collections::HashMap};
use std::sync::{
    Arc, 
    mpsc,
    atomic::{AtomicBool, Ordering},
};

#[derive(Debug, PartialEq, Clone, Copy)]
enum Direction {
    Up,
    Left,
    Right,
    Down,
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Color {
    Black,
    White,
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct Robot {
    pos: (isize, isize),
    dir: Direction,
}


fn print_hull (hm: &HashMap<(isize, isize), Color>, robi: &Robot) {
    let size_x: usize = 60;
    let size_y: usize = 10;
    let offset_x: isize = 5;
    let offset_y: isize = 7;

    let mut buf = vec![vec!["."; size_y]; size_x];
    for p in hm.iter() {
        let shift_x: usize = ((p.0).0 + offset_x) as usize;
        let shift_y: usize = ((p.0).1 + offset_y) as usize;
        if shift_x < size_x && shift_y < size_y {
            buf[shift_x][shift_y] = match p.1 {
                Color::Black => " ",
                Color::White => "▮",
            };
        }
    }
    let shift_x: usize = (robi.pos.0 + offset_x) as usize;
    let shift_y: usize = (robi.pos.1 + offset_y) as usize;
    if shift_x < size_x && shift_y < size_y {
        buf[shift_x][shift_y] = match robi.dir {
            Direction::Up => "^",
            Direction::Left => "<",
            Direction::Right => ">",
            Direction::Down => "v",
        };
    }

    buf[offset_x as usize][offset_y as usize] = "X";
    for y in 0..size_y {
        for x in 0..size_x {
            print!{"{}", buf[x][size_y - y - 1]};
        }
        println!{};
    }

}

fn robot_control (inp: &mpsc::Sender<isize>, outp: &mpsc::Receiver<isize>, finished: Arc<AtomicBool>, star_color: Color) -> (HashMap<(isize, isize), Color>, Robot) {
    let mut hull: HashMap<(isize, isize), Color> = HashMap::new();
    let mut robbi = Robot {
        pos: (0, 0),
        dir: Direction::Up,
    };
    hull.insert((0,0), star_color);

    while !finished.load(Ordering::Relaxed) {
        let current_color =
            match hull.get(&robbi.pos).unwrap_or(&Color::Black) {
                Color::Black => 0,
                Color::White => 1,
            };
        // println!("cur_col {:?}", current_color);
        inp.send(current_color).unwrap();

        if let Ok(recieved_col) = outp.recv() {
            let color_to_paint = match recieved_col {
                0 => Color::Black,
                1 => Color::White,
                _ => unreachable!{"Robot should not output this"},
            };
            hull.insert(robbi.pos, color_to_paint);
        } else {
            break;
        }

        if let Ok(rotation) = outp.recv() {
            // 0 means left, 1 means right
            robbi.dir = match (robbi.dir, rotation) {
                (Direction::Up, 0) => Direction::Left,
                (Direction::Up, 1) => Direction::Right,
                (Direction::Left, 0) => Direction::Down,
                (Direction::Left, 1) => Direction::Up,
                (Direction::Down, 0) => Direction::Right,
                (Direction::Down, 1) => Direction::Left,
                (Direction::Right, 0) => Direction::Up,
                (Direction::Right, 1) => Direction::Down,
                _ => unreachable!{"Robot should not output this"},
            };
            // print_hull(&hull, &robbi);
            // println!();
            robbi.pos = match robbi.dir {
                Direction::Up => (robbi.pos.0, robbi.pos.1 + 1),
                Direction::Left => (robbi.pos.0 - 1, robbi.pos.1),
                Direction::Down => (robbi.pos.0, robbi.pos.1 - 1),
                Direction::Right => (robbi.pos.0 + 1, robbi.pos.1),
            };
        } else {
            // println!("-----------");
            break;
        }
    }
    // print_hull(&hull, &robbi);
    (hull, robbi)
}

fn paint_hull (code: Vec<isize>, start_color: Color) {
    let (mut robot_comp, inp, outp) = IntcodeSim::new();
    robot_comp.load(code);
    let finished = Arc::new(AtomicBool::new(false));

    let comp_thread = {
        let finished = Arc::clone(&finished);
        thread::spawn(move|| {
            robot_comp.execute().unwrap();
            finished.store(true, Ordering::Relaxed);
        })
    };

    let pain_thread = {
        let finished = Arc::clone(&finished);
        thread::spawn(move|| {
            robot_control(&inp, &outp, finished, start_color)
        })
    };

    let (hull, robbi) = pain_thread.join().unwrap();
    comp_thread.join().unwrap();

    println!("Number of Painted Tiles: {}", hull.len());
    if start_color == Color::White {
        print_hull(&hull, &robbi);
    }
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut inp_vec = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        inp_vec.append(&mut tmp_inp_vec);
    }

    paint_hull(inp_vec.clone(), Color::Black);
    paint_hull(inp_vec.clone(), Color::White);


    Ok(())
}
