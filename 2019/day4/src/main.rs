extern crate rayon;
use rayon::prelude::*;
use std::{
    error,
};


fn number_to_array (mut i: usize) -> Option<[usize; 6]> {
    if i >= 1000000 {
        return None;
    }
    let mut array = [0; 6];
    array[0] = i / 100000;
    i -= array[0] * 100000;
    array[1] = i / 10000;
    i -= array[1] * 10000;
    array[2] = i / 1000;
    i -= array[2] * 1000;
    array[3] = i / 100;
    i -= array[3] * 100;
    array[4] = i / 10;
    i -= array[4] * 10;
    array[5] = i;
    Some(array)
}


fn has_adjacent_digits (arr: &[usize; 6]) -> bool {
    for i in 0..5 {
        if arr[i] == arr[i+1] {
            return true;
        }
    }
    false
}


fn has_adjacent_digits2 (arr: &[usize; 6]) -> bool {
    let mut count_groups = 0;
    let mut i = 0;
    while i < 6 {
        let mut j = 1;
        while j < 6 - i && arr[i] == arr[i + j] {
            j += 1;
        }
        if j == 2 {
            count_groups += 1;
        }
        i += j;
    }
    count_groups > 0
}


fn does_not_decrease (arr: &[usize; 6]) -> bool {
    for i in 0..5 {
        if arr[i] > arr[i+1] {
            return false;
        }
    }
    true
}

fn fulfills_pwd_crit (i: usize, part2: bool) -> bool {
    if i < 100000 {
        return false;
    }
    let a = number_to_array(i).unwrap();
    if part2 {
        has_adjacent_digits2(&a) && does_not_decrease(&a)
    } else {
        has_adjacent_digits(&a) && does_not_decrease(&a)
    }
}


fn main() -> Result<(), Box<dyn error::Error>> {
    let input_min : usize = 134564;
    let input_max : usize = 585159;

    let nr_pwd1 : usize = (input_min..=input_max)
        .into_par_iter()
        .filter(|i| fulfills_pwd_crit(*i, false))
        .count();

    println!("Nr Pwds: {}", nr_pwd1);

    let nr_pwd2 : usize = (input_min..=input_max)
        .into_par_iter()
        .filter(|i| fulfills_pwd_crit(*i, true))
        .count();

    println!("Nr Pwds Part 2: {}", nr_pwd2);

    Ok(())
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn number_to_array_test1 () {
        assert_eq!(number_to_array(123456), Some([1, 2, 3, 4, 5, 6]));
        assert_eq!(number_to_array(123), Some([0, 0, 0, 1, 2, 3]));
        assert_eq!(number_to_array(1), Some([0, 0, 0, 0, 0, 1]));
        assert_eq!(number_to_array(1234567), None);
    }

    #[test]
    fn adj_crit_test1 () {
        assert_eq!(has_adjacent_digits(&number_to_array(123456).unwrap()), false);
        assert_eq!(has_adjacent_digits(&number_to_array(123356).unwrap()), true);
        assert_eq!(has_adjacent_digits(&number_to_array(123466).unwrap()), true);
        assert_eq!(has_adjacent_digits(&number_to_array(113466).unwrap()), true);
    }

    #[test]
    fn adj_crit_test2 () {
        assert_eq!(has_adjacent_digits2(&number_to_array(123456).unwrap()), false);
        assert_eq!(has_adjacent_digits2(&number_to_array(123356).unwrap()), true);
        assert_eq!(has_adjacent_digits2(&number_to_array(123466).unwrap()), true);
        assert_eq!(has_adjacent_digits2(&number_to_array(113466).unwrap()), true);
        assert_eq!(has_adjacent_digits2(&number_to_array(112233).unwrap()), true);
        assert_eq!(has_adjacent_digits2(&number_to_array(123444).unwrap()), false);
        assert_eq!(has_adjacent_digits2(&number_to_array(111122).unwrap()), true);
    }

    #[test]
    fn no_decr_crit_test1 () {
        assert_eq!(does_not_decrease(&number_to_array(123456).unwrap()), true);
        assert_eq!(does_not_decrease(&number_to_array(654321).unwrap()), false);
        assert_eq!(does_not_decrease(&number_to_array(111111).unwrap()), true);
        assert_eq!(does_not_decrease(&number_to_array(111110).unwrap()), false);
    }

    #[test]
    fn fulfills_pwd_crit_test1 () {
        assert_eq!(fulfills_pwd_crit(111110, false), false);
        assert_eq!(fulfills_pwd_crit(111111, false), true);
        assert_eq!(fulfills_pwd_crit(223450, false), false);
        assert_eq!(fulfills_pwd_crit(123789, false), false);
    }

    #[test]
    fn fulfills_pwd_crit_test2 () {
        assert_eq!(fulfills_pwd_crit(111110, true), false);
        assert_eq!(fulfills_pwd_crit(111123, true), false);
        assert_eq!(fulfills_pwd_crit(223455, true), true);

        assert_eq!(fulfills_pwd_crit(112233, true), true);
        assert_eq!(fulfills_pwd_crit(123444, true), false);
        assert_eq!(fulfills_pwd_crit(111122, true), true);
    }

}
