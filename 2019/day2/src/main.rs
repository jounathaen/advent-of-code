use std::{
    fs,
    fmt,
    error,
};


#[derive(Debug, PartialEq)]
enum ExecutionError {
    WrongOpcode((usize, usize)),
}

impl fmt::Display for ExecutionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ExecutionError::WrongOpcode(i) => write!{f, "Invalid Opcode {} at index: {}", i.0, i.1},
        }
    }
}

impl error::Error for ExecutionError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

fn execute(code:&mut Vec<usize>) -> Result<(), ExecutionError> {
    let mut pc = 0;
    loop {
        match code[pc] {
            1 => {
                let target_addr = code[pc+3];
                code[target_addr] = code[code[pc+1]] + code[code[pc + 2]];
            },
            2 => {
                let target_addr = code[pc+3];
                code[target_addr] = code[code[pc+1]] * code[code[pc + 2]];
            },
            99 => break,
            i => return Err(ExecutionError::WrongOpcode((i, pc))),
        }
        pc += 4;
    }
    Ok(())
}



fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut inp_vec = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<usize> = line
            .split(',')
            .map(|s| s.parse::<usize>().unwrap())
            .collect();
        inp_vec.append(&mut tmp_inp_vec);
    }

    let mut part1_vec = inp_vec.clone();
    part1_vec[1] =  12;
    part1_vec[2] =  2;

    execute(&mut part1_vec)?;

    println!{"The Value at position 0 is {}", inp_vec[0]};

    'outer: for i in 0..100 {
        for j in 0..100 {
            let mut part2_vec = inp_vec.clone();

            part2_vec[1] =  i;
            part2_vec[2] =  j;
            execute(&mut part2_vec)?;
            if part2_vec[0] == 19690720 {
                println!{"Noun: {}, Verb: {}", i, j};
                println!{"Answer is {}", i * 100 + j};
                break 'outer;
            }
        }
    }

    Ok(())
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn code_test_1(){
        let mut inp : Vec<usize> = vec!{1,9,10,3,2,3,11,0,99,30,40,50};
        execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{3500,9,10,70,2,3,11,0,99,30,40,50});
    }

    #[test]
    fn code_test_2(){
        let mut inp : Vec<usize> = vec!{1,0,0,0,99};
        execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{2,0,0,0,99});
    }

    #[test]
    fn code_test_3(){
        let mut inp : Vec<usize> = vec!{2,3,0,3,99};
        execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{2,3,0,6,99});
    }

    #[test]
    fn code_test_4(){
        let mut inp : Vec<usize> = vec!{2,4,4,5,99,0};
        execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{2,4,4,5,99,9801});
    }

    #[test]
    fn code_test_5(){
        let mut inp : Vec<usize> = vec!{1,1,1,4,99,5,6,0,99};
        execute(&mut inp).unwrap();
        assert_eq!(inp, vec!{30,1,1,4,2,5,6,0,99});
    }

    #[test]
    fn error_test(){
        let mut inp : Vec<usize> = vec!{3,0,0,0,99};
        assert_eq!{execute(&mut inp).unwrap_err(),
            ExecutionError::WrongOpcode((3, 0))};
    }
}
