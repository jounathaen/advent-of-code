extern crate custom_error;
use custom_error::custom_error;
use std::{
    collections::{HashMap, HashSet, VecDeque},
    error, fs,
};

custom_error! {#[derive(PartialEq,PartialOrd)] MazeError
    ParsingError = "Cannot parse input to maze",
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum MazeElem {
    Door(char),
    Empty,
    Entry,
    Key(char),
    Wall,
}

#[derive(Debug, PartialEq, Clone, Copy, Eq, Hash)]
struct Coord {
    x: usize,
    y: usize,
}
impl Coord {
    fn new(x: usize, y: usize) -> Self {
        Coord { x, y }
    }
    fn get_adjacent(&self, direction: isize) -> Coord {
        match direction {
            1 => Coord::new(self.x, self.y + 1), // North
            2 => Coord::new(self.x, self.y - 1), // South
            3 => Coord::new(self.x - 1, self.y), // West
            4 => Coord::new(self.x + 1, self.y), // East
            e => panic! {"{} is not a valid direction", e},
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Connection {
    // target: char,
    dist: usize,
    requirements: Vec<char>,
}

struct Maze {
    field: Vec<Vec<MazeElem>>,
    keys: Vec<(char, Coord)>,
    entry: Coord,
    connections: HashMap<char, HashMap<char, Connection>>,
}
impl Maze {
    fn from(f: &str) -> Result<Self, MazeError> {
        let mut field = Vec::new();
        let mut entry = Coord { x: 0, y: 0 };
        let mut keys = Vec::new();
        for (y, line) in f.lines().enumerate() {
            let mut lv = Vec::new();
            for (x, c) in line.chars().enumerate() {
                lv.push(match c {
                    '#' => MazeElem::Wall,
                    '.' => MazeElem::Empty,
                    '@' => {
                        entry = Coord { x, y };
                        MazeElem::Entry
                    }
                    c => {
                        if c.is_uppercase() {
                            MazeElem::Door(c)
                        } else if c.is_lowercase() {
                            keys.push((c, Coord { x, y }));
                            MazeElem::Key(c)
                        } else {
                            panic!("Unknown maze character {}", c);
                        }
                    }
                });
            }
            field.push(lv);
        }
        keys.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());
        let mut m = Maze {
            field,
            keys,
            entry,
            connections: HashMap::new(),
        };
        m.connections.insert('@', m.calculate_connections(&m.entry));
        for (k, choord) in &m.keys {
            m.connections.insert(*k, m.calculate_connections(&choord));
        }
        Ok(m)
    }

    fn get(&self, c: Coord) -> Option<&MazeElem> {
        if let Some(y) = self.field.get(c.y) {
            y.get(c.x)
        } else {
            None
        }
    }

    fn calculate_connections(&self, start: &Coord) -> HashMap<char, Connection> {
        let mut visited: HashSet<Coord> = HashSet::new();
        let mut queue: VecDeque<(Coord, Vec<char>, usize)> = VecDeque::new();
        let mut connections = HashMap::new();
        queue.push_back((*start, Vec::new(), 0));

        while !queue.is_empty() {
            let (coord, requ_keys, depth) = queue.pop_front().unwrap();
            visited.insert(coord);
            for dir in 1..=4 {
                let adj_coord = coord.get_adjacent(dir);
                if let Some(elem) = self.get(adj_coord) {
                    if visited.get(&adj_coord).is_none() {
                        match elem {
                            MazeElem::Door(d) => {
                                let mut requ_keys = requ_keys.clone();
                                requ_keys.push(d.to_ascii_lowercase());
                                queue.push_back((adj_coord, requ_keys, depth + 1));
                            }
                            MazeElem::Key(c) => {
                                connections.insert(
                                    *c,
                                    Connection {
                                        dist: depth + 1,
                                        requirements: requ_keys.clone(),
                                    },
                                );
                                queue.push_back((adj_coord, requ_keys.clone(), depth + 1));
                            }
                            MazeElem::Wall => {}
                            MazeElem::Entry | MazeElem::Empty => {
                                queue.push_back((adj_coord, requ_keys.clone(), depth + 1));
                            }
                        }
                    }
                }
            }
        }
        connections
    }

    fn find_reachable_keys(&self, pos: char, keys: &[char]) -> Vec<(char, usize)> {
        let mut v: Vec<(char, usize)> = self
            .connections
            .get(&pos)
            .unwrap()
            .iter()
            .filter(|(targ, con)| {
                if keys.contains(targ) {
                    return false;
                }
                for r in con.requirements.iter() {
                    if !keys.contains(&r) {
                        return false;
                    }
                }
                true
            })
            .map(|(targ, con)| (*targ, con.dist))
            .collect();
        v.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap());
        v
    }

    fn find_shortest_path(&self) -> usize {
        // fn find_shortest_path(&self, start: &Coord)  {
        let mut queue: VecDeque<(char, Vec<char>, usize)> = VecDeque::new();
        queue.push_back(('@', Vec::new(), 0));

        /*
         * Remaining cache that stores the current distance.
         * If I have X,Y,and Z remaining, and took 20 steps, but
         * in a previous run you had X,Y, and Z left with 18 steps,
         * you can stop right now.
         */
        let mut cache: HashMap<Vec<char>, usize> = HashMap::new();

        let mut shortest_len = std::usize::MAX;

        while !queue.is_empty() {
            let (pos, keys, depth) = queue.pop_back().unwrap();

            for (k, dist) in self.find_reachable_keys(pos, &keys) {
                let remaining: Vec<char> = self
                    .keys
                    .iter()
                    .map(|(k, _)| *k)
                    .filter(|k| !keys.contains(k))
                    .collect();
                if let Some(&d) = cache.get(&remaining) {
                    if d < dist + depth {
                        continue;
                    }
                }
                cache.insert(remaining, dist + depth);

                let mut keys = keys.clone();
                keys.push(k);


                if dist + depth < shortest_len {
                    if keys.len() == self.keys.len() {
                        shortest_len = dist + depth;
                    } else {
                        queue.push_back((k, keys, depth + dist));
                    }
                }
            }
        }
        shortest_len
    }
    }

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let m = Maze::from(&f).unwrap();
    println!("Solution part 1: {}", m.find_shortest_path());
    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parser() {
        let s1 = concat!(
            "########################\n",
            "#...............b.C.D.f#\n",
            "#.######################\n",
            "#.....@.a.B.c.d.A.e.F.g#\n",
            "########################\n"
        )
        .to_string();
        let m = Maze::from(&s1).unwrap();
        assert_eq!(m.field[0][0], MazeElem::Wall);
        assert_eq!(m.field[1][16], MazeElem::Key('b'));
        assert_eq!(m.field[3][6], MazeElem::Entry);
        assert_eq!(m.keys.len(), 7);
        assert_eq!(m.field.len(), 5);
        assert!(m.connections.contains_key(&'g'));
        assert_eq!(m.connections.get(&'a').unwrap().get(&'c').unwrap().dist, 4);
        assert_eq!(
            m.connections
                .get(&'@')
                .unwrap()
                .get(&'a')
                .unwrap()
                .requirements
                .len(),
            0
        );
        assert!(m
            .connections
            .get(&'a')
            .unwrap()
            .get(&'c')
            .unwrap()
            .requirements
            .contains(&'b'));
        assert!(m
            .connections
            .get(&'b')
            .unwrap()
            .get(&'c')
            .unwrap()
            .requirements
            .contains(&'b'));
        assert!(m
            .connections
            .get(&'f')
            .unwrap()
            .get(&'g')
            .unwrap()
            .requirements
            .contains(&'c'));
        assert!(m
            .connections
            .get(&'f')
            .unwrap()
            .get(&'g')
            .unwrap()
            .requirements
            .contains(&'d'));
    }

    #[test]
    fn best_path() {
        let s = concat!(
            "########################\n",
            "#f.D.E.e.C.b.A.@.a.B.c.#\n",
            "######################.#\n",
            "#d.....................#\n",
            "########################\n",
        )
        .to_string();
        let m = Maze::from(&s).unwrap();
        assert_eq!(m.find_shortest_path().1, 86);

        let s = concat!(
            "########################\n",
            "#...............b.C.D.f#\n",
            "#.######################\n",
            "#.....@.a.B.c.d.A.e.F.g#\n",
            "########################\n"
        )
        .to_string();
        let m = Maze::from(&s).unwrap();
        assert_eq!(m.find_shortest_path().1, 132);
        let s = concat!(
            "#################\n",
            "#i.G..c...e..H.p#\n",
            "########.########\n",
            "#j.A..b...f..D.o#\n",
            "########@########\n",
            "#k.E..a...g..B.n#\n",
            "########.########\n",
            "#l.F..d...h..C.m#\n",
            "#################\n",
        )
        .to_string();
        let m = Maze::from(&s).unwrap();
        assert_eq!(m.find_shortest_path().1, 136);
    }

    #[test]
    fn find_reachable_keys_test() {
        let s = concat!(
            "#################\n",
            "#i.G..c...e..H.p#\n",
            "########.########\n",
            "#j.A..b...f..D.o#\n",
            "########@########\n",
            "#k.E..a...g..B.n#\n",
            "########.########\n",
            "#l.F..d...h..C.m#\n",
            "#################\n",
        )
        .to_string();
        let m = Maze::from(&s).unwrap();
        let v = m.find_reachable_keys(&'@', &Vec::new());
        assert!(v.contains(&('c', 5)));
        assert!(v.contains(&('b', 3)));

        let v = m.find_reachable_keys(&'c', &vec!['g']);
        assert!(v.contains(&('b', 6)));
        assert!(v.contains(&('i', 5)));
    }
}
