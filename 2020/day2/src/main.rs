#![allow(soft_unstable)]
#![feature(test)]
extern crate test;
use regex::Regex;
use std::{error, fs};

#[derive(Debug, Default, PartialEq, Eq)]
struct PwLine {
    min: usize,
    max: usize,
    c: char,
    pw: String,
}
impl PwLine {
    fn is_valid_p1(&self) -> bool {
        let count = self.pw.matches(self.c).count();
        count >= self.min && count <= self.max
    }

    fn is_valid_p2(&self) -> bool {
        (self.pw.as_bytes()[self.min - 1] == self.c as u8
            && self.pw.as_bytes()[self.max - 1] != self.c as u8)
            || (self.pw.as_bytes()[self.min - 1] != self.c as u8
                && self.pw.as_bytes()[self.max - 1] == self.c as u8)
    }

    fn from_str(l: &str) -> Self {
        let re = Regex::new(r"(\d+)-(\d+) ([[:alpha:]]): ([[:alpha:]]+)").unwrap();
        let caps = re.captures(&l).expect("Invalid input");
        PwLine {
            min: caps[1].parse().unwrap(),
            max: caps[2].parse().unwrap(),
            c: caps[3].parse().unwrap(),
            pw: caps[4].parse().unwrap(),
        }
    }
}

fn part1(pws: &Vec<PwLine>) -> usize {
    pws.iter().filter(|pw| pw.is_valid_p1()).count()
}

fn part2(pws: &Vec<PwLine>) -> usize {
    pws.iter().filter(|pw| pw.is_valid_p2()).count()
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let pws: Vec<PwLine> = f.lines().map(|l| PwLine::from_str(l)).collect();

    println!("Part 1: {}", part1(&pws));
    println!("Part 2: {}", part2(&pws));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn pw_read_test() {
        assert_eq!(
            PwLine::from_str(&"1-3 a: abcde"),
            PwLine {
                min: 1,
                max: 3,
                c: 'a',
                pw: "abcde".to_string()
            }
        );
        assert_eq!(
            PwLine::from_str(&"1-3 b: cdefg"),
            PwLine {
                min: 1,
                max: 3,
                c: 'b',
                pw: "cdefg".to_string()
            }
        );
        assert_eq!(
            PwLine::from_str(&"2-9 c: ccccccccc"),
            PwLine {
                min: 2,
                max: 9,
                c: 'c',
                pw: "ccccccccc".to_string()
            }
        );
    }

    #[test]
    fn valid_pw1_test() {
        assert_eq!(PwLine::from_str(&"1-3 a: abcde").is_valid_p1(), true);
        assert_eq!(PwLine::from_str(&"1-3 b: cdefg").is_valid_p1(), false);
        assert_eq!(PwLine::from_str(&"2-9 c: ccccccccc").is_valid_p1(), true);
    }
    #[test]
    fn valid_pw2_test() {
        assert_eq!(PwLine::from_str(&"1-3 a: abcde").is_valid_p2(), true);
        assert_eq!(PwLine::from_str(&"1-3 b: cdefg").is_valid_p2(), false);
        assert_eq!(PwLine::from_str(&"2-9 c: ccccccccc").is_valid_p2(), false);
    }

    #[bench]
    fn pt1_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let pws: Vec<PwLine> = f.lines().map(|l| PwLine::from_str(l)).collect();
        b.iter(|| part1(&pws));
    }

    #[bench]
    fn pt2_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let pws: Vec<PwLine> = f.lines().map(|l| PwLine::from_str(l)).collect();
        b.iter(|| part2(&pws));
    }
}
