#![allow(soft_unstable)]
#![feature(test)]
extern crate test;
use std::{error, fs};

fn valid_xmas(prev_nrs: &[isize], nr: isize) -> bool {
    for i in prev_nrs.iter() {
        if let Some(_j) = prev_nrs.iter().position(|&j| j == nr - i && j != *i) {
            return true;
        }
    }
    false
}

fn find_sum_range(inp: &[isize], nr: isize) -> Option<&[isize]> {
    for i in 0..inp.len() - 2 {
        let mut tmpsum = inp[i];
        for j in i + 1..inp.len() - 1 {
            tmpsum += inp[j];
            if tmpsum == nr {
                return Some(&inp[i..=j]);
            } else {
                if tmpsum > nr {
                    continue;
                }
            }
        }
    }
    None
}

fn part1(inp: &[isize], pre_len: usize) -> isize {
    *inp.iter()
        .enumerate()
        .skip(pre_len + 1)
        .find(|(i, &nr)| !valid_xmas(&inp[(i - pre_len - 1)..*i], nr))
        .unwrap()
        .1
}

fn part2(inp: &[isize], target_nr: isize) -> isize {
    let range = find_sum_range(inp, target_nr).unwrap();
    *range.iter().min().unwrap() + *range.iter().max().unwrap()
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let nrs: Vec<isize> = f
        .lines()
        .map(|l| l.parse::<isize>().expect("malformed input file"))
        .collect();
    let pt1_sol = part1(&nrs, 25);
    println!("Part 1: {}", pt1_sol);
    println!("Part 2: {}", part2(&nrs, pt1_sol));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn valid_xmas_test() {
        let testinp: Vec<_> = (1..=25).collect();
        dbg!(testinp.len());
        assert!(valid_xmas(&testinp, 26));
        assert!(valid_xmas(&testinp, 49));
        assert!(!valid_xmas(&testinp, 50));
        assert!(!valid_xmas(&testinp, 100));
        let testinp = [
            35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
            576,
        ];
        for i in 0..8 {
            assert!(valid_xmas(&testinp[i..(i + 5 + 1)], testinp[i + 5 + 1]));
        }
        for i in 9..testinp.len() - 5 - 1 {
            assert!(valid_xmas(&testinp[i..(i + 5 + 1)], testinp[i + 5 + 1]));
        }
    }

    #[test]
    fn pt1_test() {
        let testinp = [
            35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
            576,
        ];
        assert_eq!(part1(&testinp, 5), 127)
    }

    #[test]
    fn find_sum_range_test() {
        let testinp = [
            35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
            576,
        ];
        assert_eq!(find_sum_range(&testinp, 127), Some(&testinp[2..=5]));
    }

    #[test]
    fn pt2_test() {
        let testinp = [
            35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
            576,
        ];
        assert_eq!(part2(&testinp, 127), 62);
    }

    #[bench]
    fn pt1_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let nrs: Vec<isize> = f
            .lines()
            .map(|l| l.parse::<isize>().expect("malformed input file"))
            .collect();
        b.iter(|| part1(&nrs, 25));
    }

    #[bench]
    fn pt2_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let nrs: Vec<isize> = f
            .lines()
            .map(|l| l.parse::<isize>().expect("malformed input file"))
            .collect();
        let pt1_sol = part1(&nrs, 25);
        b.iter(|| part2(&nrs, pt1_sol));
    }
}
