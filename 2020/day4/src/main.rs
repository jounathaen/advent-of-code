#![allow(soft_unstable)]
#![feature(test)]
extern crate test;
//use regex::Regex;
use std::{convert::TryFrom, error, fs};

#[derive(Debug, PartialEq, Eq)]
enum Height {
    Cm(u16),
    Inch(u16),
}
impl TryFrom<&str> for Height {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        if s.len() == 5 && &s[3..5] == "cm" {
            s[0..3]
                .parse::<u16>()
                .map_or(Err("invalid size unit"), |v| {
                    if v >= 150 && v <= 193 {
                        Ok(Self::Cm(v))
                    } else {
                        Err("invalid size")
                    }
                })
        } else if s.len() == 4 && &s[2..4] == "in" {
            s[0..2]
                .parse::<u16>()
                .map_or(Err("invalid size unit"), |v| {
                    if v >= 59 && v <= 76 {
                        Ok(Self::Inch(v))
                    } else {
                        Err("invalid size")
                    }
                })
        } else {
            Err("invalid input")
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
enum EyeColor {
    Amb,
    Blu,
    Brn,
    Gry,
    Grn,
    Hzl,
    Oth,
}
impl TryFrom<&str> for EyeColor {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        match s {
            "amb" => Ok(Self::Amb),
            "blu" => Ok(Self::Blu),
            "brn" => Ok(Self::Brn),
            "gry" => Ok(Self::Gry),
            "grn" => Ok(Self::Grn),
            "hzl" => Ok(Self::Hzl),
            "oth" => Ok(Self::Oth),
            _ => Err("invalid input"),
        }
    }
}

#[derive(Debug, Default, PartialEq, Eq)]
struct PassportPt2 {
    byr: Option<u16>,
    iyr: Option<u16>,
    eyr: Option<u16>,
    hgt: Option<Height>,   // cm vs inch
    hcl: Option<u32>,      // hex color
    ecl: Option<EyeColor>, // hex color
    pid: Option<u64>,
    cid: Option<()>,
}
impl PassportPt2 {
    fn is_valid(&self) -> bool {
        self.byr.is_some()
            && self.iyr.is_some()
            && self.eyr.is_some()
            && self.hgt.is_some()
            && self.hcl.is_some()
            && self.ecl.is_some()
            && self.pid.is_some()
    }
}

fn count_valid_passports_pt2(file: &String) -> usize {
    let mut count = 0;
    let mut entry = PassportPt2::default();
    for l in file.lines() {
        if l.is_empty() {
            if entry.is_valid() {
                count += 1;
            }
            entry = PassportPt2::default();
        } else {
            for field in l.split_whitespace() {
                match &field[0..4] {
                    "byr:" => {
                        entry.byr = field[4..].parse().ok().map_or(None, |v| {
                            if v <= 2002 && v >= 1920 {
                                Some(v)
                            } else {
                                None
                            }
                        })
                    }
                    "iyr:" => {
                        entry.iyr = field[4..].parse().ok().map_or(None, |v| {
                            if v <= 2020 && v >= 2010 {
                                Some(v)
                            } else {
                                None
                            }
                        })
                    }
                    "eyr:" => {
                        entry.eyr = field[4..].parse().ok().map_or(None, |v| {
                            if v <= 2030 && v >= 2020 {
                                Some(v)
                            } else {
                                None
                            }
                        })
                    }
                    "hgt:" => entry.hgt = Height::try_from(&field[4..]).ok(),
                    "ecl:" => entry.ecl = EyeColor::try_from(&field[4..]).ok(),
                    "hcl:" => {
                        entry.hcl = if &field[4..5] == "#" {
                            u32::from_str_radix(&field[5..], 16).map_or(None, |v| {
                                if v > 0xFFFFFF {
                                    None
                                } else {
                                    Some(v)
                                }
                            })
                        } else {
                            None
                        }
                    }
                    "pid:" => {
                        entry.pid = field[4..].parse().ok().map_or(None, |v| {
                            if field[4..].len() == 9 && v <= 999_999_999 {
                                Some(v)
                            } else {
                                None
                            }
                        })
                    }
                    "cid:" => entry.cid = Some(()),
                    _ => panic!("Invalid input: {}", field),
                }
            }
        }
    }
    if entry.is_valid() {
        count += 1;
    }
    count
}

fn part1(file: &String) -> usize {
    #[derive(Default)]
    struct PassportPt1 {
        byr: Option<()>,
        iyr: Option<()>,
        eyr: Option<()>,
        hgt: Option<()>, // cm vs inch
        hcl: Option<()>, // hex color
        ecl: Option<()>, // hex color
        pid: Option<()>,
        cid: Option<()>,
    }
    impl PassportPt1 {
        fn is_valid(&self) -> bool {
            self.byr.is_some()
                && self.iyr.is_some()
                && self.eyr.is_some()
                && self.hgt.is_some()
                && self.hcl.is_some()
                && self.ecl.is_some()
                && self.pid.is_some()
        }
    }

    let mut count = 0;
    let mut entry = PassportPt1::default();
    for l in file.lines() {
        if l.is_empty() {
            if entry.is_valid() {
                count += 1;
            }
            entry = PassportPt1::default();
        } else {
            for field in l.split_whitespace() {
                match &field[0..3] {
                    "byr" => entry.byr = Some(()),
                    "iyr" => entry.iyr = Some(()),
                    "eyr" => entry.eyr = Some(()),
                    "hgt" => entry.hgt = Some(()),
                    "hcl" => entry.hcl = Some(()),
                    "pid" => entry.pid = Some(()),
                    "cid" => entry.cid = Some(()),
                    "ecl" => entry.ecl = Some(()),
                    _ => panic!("Invalid input: {}", field),
                }
            }
        }
    }
    if entry.is_valid() {
        count += 1;
    }
    count
}

fn part2(file: &String) -> usize {
    count_valid_passports_pt2(file)
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;

    println!("Part 1: {}", part1(&f));
    println!("Part 2: {}", part2(&f));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    const TESTDATA: &str = r#"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in
"#;

    #[test]
    fn pt1_test() {
        assert_eq!(part1(&TESTDATA.to_string()), 2);
    }

    #[test]
    fn pt2_invalid_test() {
        let invalid_pw = r#"eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"#;
        assert_eq!(count_valid_passports_pt2(&invalid_pw.to_string()), 0);
    }

    #[test]
    fn pt2_valid_test() {
        let valid_pw = &r#"pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"#;
        assert_eq!(count_valid_passports_pt2(&valid_pw.to_string()), 4);
    }

    //#[bench]
    //fn pt1_bench(b: &mut Bencher) {
    //let f = fs::read_to_string("input.txt").unwrap();
    //let pws: Vec<PwLine> = f.lines().map(|l| PwLine::from_str(l)).collect();
    //b.iter(|| part1(&pws));
    //}

    //#[bench]
    //fn pt2_bench(b: &mut Bencher) {
    //let f = fs::read_to_string("input.txt").unwrap();
    //let pws: Vec<PwLine> = f.lines().map(|l| PwLine::from_str(l)).collect();
    //b.iter(|| part2(&pws));
    //}
}
