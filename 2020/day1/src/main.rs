#![allow(soft_unstable)]
#![feature(test)]
extern crate test;
use std::{error, fs};

// input must be sorted
fn part1(v: &Vec<u64>) -> Option<u64> {
    for i in 0..v.len() {
        for j in (i + 1)..v.len() {
            if v[i] + v[j] == 2020 {
                return Some(v[i] * v[j]);
            }
        }
    }
    None
}

// input must be sorted
fn part2(v: &Vec<u64>) -> Option<u64> {
    for i in 0..v.len() {
        for j in (i + 1)..v.len() {
            // Early break
            let psum = v[i] + v[j];
            if psum > 2020 {
                break;
            }
            for k in (j + 1)..v.len() {
                if v[i] + v[j] + v[k] == 2020 {
                    return Some(v[i] * v[j] * v[k]);
                }
            }
        }
    }
    None
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut expenses: Vec<u64> = f.lines().map(|l| l.parse::<u64>().unwrap()).collect();
    expenses.sort();
    println!("part1: {:?}", part1(&expenses).unwrap());
    println!("part2: {:?}", part2(&expenses).unwrap());

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn pt1_test() {
        let mut v = vec![1721, 979, 366, 299, 675, 1456];
        v.sort();
        assert_eq!(part1(&v), Some(514579));
    }

    #[test]
    fn pt2_test() {
        let mut v = vec![1721, 979, 366, 299, 675, 1456];
        v.sort();
        assert_eq!(part2(&v), Some(241861950));
    }

    #[bench]
    fn pt1_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let mut expenses: Vec<u64> = f.lines().map(|l| l.parse::<u64>().unwrap()).collect();
        expenses.sort();
        b.iter(|| part1(&expenses).unwrap());
    }

    #[bench]
    fn pt2_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let mut expenses: Vec<u64> = f.lines().map(|l| l.parse::<u64>().unwrap()).collect();
        expenses.sort();
        b.iter(|| part2(&expenses).unwrap());
    }
    }
}
