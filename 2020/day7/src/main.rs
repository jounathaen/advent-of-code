#![allow(soft_unstable)]
#![feature(test)]
extern crate test;
use regex::Regex;
use std::collections::HashMap;
use std::{error, fs};

#[derive(Debug, Default, PartialEq, Eq)]
struct BagRule<'a> {
    color: &'a str,
    containments: Vec<(usize, &'a str)>,
}
impl<'a> BagRule<'a> {
    fn from_str(line: &'a str) -> Self {
        let mut split1 = line.split(" bags contain ");
        let color = split1.next().unwrap();
        let re = Regex::new(r"\s*(\d+) ([[:alpha:]]+ [[:alpha:]]+) (?:bag)|(?:bags)").unwrap();
        let containments = split1
            .next()
            .unwrap()
            .split(",")
            .filter_map(|c| {
                let caps = re.captures(&c).expect(&format!("Invalid input: {}", line));
                if caps.get(1).is_some() {
                    Some((
                        caps[1].parse::<usize>().unwrap(),
                        caps.get(2).unwrap().as_str(),
                    ))
                } else {
                    None
                }
            })
            .collect();
        BagRule {
            color,
            containments,
        }
    }
}

struct BagRules<'a>(HashMap<&'a str, Vec<(usize, &'a str)>>);
impl<'a> BagRules<'a> {
    fn from_str(inp: &'a str) -> Self {
        BagRules(
            inp.lines()
                .map(|l| {
                    let br = BagRule::from_str(l);
                    (br.color, br.containments)
                })
                .collect(),
        )
    }
    fn contains_colored_bag(&self, target_color: &str, current_color: &str) -> bool {
        if target_color == current_color {
            return true;
        }
        for (_, col) in self.0.get(current_color).unwrap() {
            if self.contains_colored_bag(target_color, col) {
                return true;
            }
        }
        false
    }
    fn contained_bags(&self, current_color: &str) -> usize {
        self.0
            .get(current_color)
            .unwrap()
            .iter()
            .map(|(qty, col)| qty * self.contained_bags(col))
            .sum::<usize>()
            + 1
    }
}

fn part1(bag_rules: &BagRules) -> usize {
    bag_rules
        .0
        .keys()
        .filter(|br| bag_rules.contains_colored_bag(&"shiny gold", br))
        .count()
        - 1
}

fn part2(bag_rules: &BagRules) -> usize {
    bag_rules.contained_bags(&"shiny gold") - 1
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let bag_rules = BagRules::from_str(&f);

    println!("Part 1: {}", part1(&bag_rules));
    println!("Part 2: {}", part2(&bag_rules));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    const TESTINP: &str = r#"light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."#;

    #[test]
    fn parse_inp_test() {
        assert_eq!(
            BagRule::from_str(&"light red bags contain 1 bright white bag, 2 muted yellow bags."),
            BagRule {
                color: "light red",
                containments: vec![(1, "bright white"), (2, "muted yellow")]
            }
        );
        assert_eq!(
            BagRule::from_str(
                &"dark orange bags contain 3 bright white bags, 4 muted yellow bags."
            ),
            BagRule {
                color: "dark orange",
                containments: vec![(3, "bright white"), (4, "muted yellow")]
            }
        );
        assert_eq!(
            BagRule::from_str(&"bright white bags contain 1 shiny gold bag."),
            BagRule {
                color: "bright white",
                containments: vec![(1, "shiny gold")]
            }
        );
        assert_eq!(
            BagRule::from_str(&"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags."),
            BagRule {
                color: "muted yellow",
                containments: vec![(2, "shiny gold"), (9, "faded blue")]
            }
        );
        assert_eq!(
            BagRule::from_str(&"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags."),
            BagRule {
                color: "shiny gold",
                containments: vec![(1, "dark olive"), (2, "vibrant plum")]
            }
        );
        assert_eq!(
            BagRule::from_str(&"faded blue bags contain no other bags."),
            BagRule {
                color: "faded blue",
                containments: Vec::new()
            }
        );
        assert_eq!(
            BagRule::from_str(&"dotted black bags contain no other bags."),
            BagRule {
                color: "dotted black",
                containments: Vec::new()
            }
        );
    }

    #[test]
    fn contains_color_test() {
        let bag_rules = BagRules::from_str(TESTINP);
        let cnt = bag_rules
            .0
            .keys()
            .filter(|br| bag_rules.contains_colored_bag(&"shiny gold", br))
            .count()
            - 1;
        assert_eq!(cnt, 4)
    }

    #[test]
    fn contained_bags_test() {
        let bag_rules = BagRules::from_str(TESTINP);
        let cnt = bag_rules.contained_bags(&"shiny gold") - 1;
        assert_eq!(cnt, 32)
    }

    #[test]
    fn contained_bags_test_2() {
        let bag_rules = BagRules::from_str(
            r#"shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."#,
        );
        let cnt = bag_rules.contained_bags(&"shiny gold") - 1;
        assert_eq!(cnt, 126)
    }

    #[bench]
    fn pt1_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let bag_rules = BagRules::from_str(&f);
        b.iter(|| part1(&bag_rules));
    }

    #[bench]
    fn pt2_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let bag_rules = BagRules::from_str(&f);
        b.iter(|| part2(&bag_rules));
    }

    #[bench]
    fn parse_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        b.iter(|| BagRules::from_str(&f));
    }
}
