#[macro_use]
extern crate lazy_static;

use std::fs;

use regex::Regex;

lazy_static! {
    static ref RE_CARDID: Regex = Regex::new(r"Card\s+(\d+)").unwrap();
    static ref RE_NR: Regex = Regex::new(r"(\d+)").unwrap();
}

#[derive(Debug, PartialEq)]
struct Card {
    id: u32,
    have_cards: Vec<u32>,
    win_cards: Vec<u32>,
}
impl Card {
    fn nr_wins(&self) -> usize {
        self.have_cards
            .iter()
            .filter_map(|nr| self.win_cards.binary_search(nr).ok())
            .count()
    }
}
impl From<&str> for Card {
    fn from(l: &str) -> Self {
        let caps = RE_CARDID.captures(l).unwrap();
        let id = caps[1].parse().unwrap();
        let mut parts = l[caps.get(1).unwrap().end()..].split('|');

        let vec1 = RE_NR
            .captures_iter(parts.next().unwrap())
            .map(|c| c[1].parse::<u32>().unwrap())
            .collect();
        let mut vec2: Vec<u32> = RE_NR
            .captures_iter(parts.next().unwrap())
            .map(|c| c[1].parse::<u32>().unwrap())
            .collect();
        vec2.sort();
        Self {
            id,
            have_cards: vec1,
            win_cards: vec2,
        }
    }
}

fn analyze_line_pt1(l: &str) -> u32 {
    let card = Card::from(l);
    let win_nr = card.nr_wins();
    if win_nr > 0 {
        0x1 << (win_nr - 1)
    } else {
        0
    }
}

fn analyze_cards_pt2(s: &str) -> u32 {
    let nr_cards = s.lines().count();
    let mut cards: Vec<u32> = Vec::with_capacity(nr_cards);
    cards.resize(nr_cards, 1);
    let mut total_cards = 0;
    for (i, l) in s.lines().enumerate() {
        total_cards += cards[i];
        let card = Card::from(l);
        let wins = card.nr_wins();
        for new_card in i + 1..i + wins + 1 {
            cards[new_card] += cards[i];
        }
    }
    total_cards
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1: u32 = f.lines().map(analyze_line_pt1).sum();
    println!("part1: {pt1}");

    let pt2: u32 = analyze_cards_pt2(&f);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    const INP1: &str = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

    #[test]
    fn winning_nrs_test() {
        let mut sol: [Card; 6] = [
            Card {
                id: 1,
                have_cards: vec![41, 48, 83, 86, 17],
                win_cards: vec![83, 86, 6, 31, 17, 9, 48, 53],
            },
            Card {
                id: 2,
                have_cards: vec![13, 32, 20, 16, 61],
                win_cards: vec![61, 30, 68, 82, 17, 32, 24, 19],
            },
            Card {
                id: 3,
                have_cards: vec![1, 21, 53, 59, 44],
                win_cards: vec![69, 82, 63, 72, 16, 21, 14, 1],
            },
            Card {
                id: 4,
                have_cards: vec![41, 92, 73, 84, 69],
                win_cards: vec![59, 84, 76, 51, 58, 5, 54, 83],
            },
            Card {
                id: 5,
                have_cards: vec![87, 83, 26, 28, 32],
                win_cards: vec![88, 30, 70, 12, 93, 22, 82, 36],
            },
            Card {
                id: 6,
                have_cards: vec![31, 18, 13, 56, 72],
                win_cards: vec![74, 77, 10, 23, 35, 67, 36, 11],
            },
        ];
        for (calc, sol) in INP1.lines().map(Card::from).zip(sol.iter_mut()) {
            sol.win_cards.sort();
            assert_eq!(&calc, sol);
        }
    }

    #[test]
    fn analyze_line_pt1_test() {
        let sol = [8, 2, 2, 1, 0, 0];
        for (calc, sol) in INP1.lines().map(analyze_line_pt1).zip(sol.iter()) {
            assert_eq!(&calc, sol);
        }
    }

    #[test]
    fn analyze_cards_pt2_test() {
        assert_eq!(analyze_cards_pt2(INP1), 30);
    }
}
