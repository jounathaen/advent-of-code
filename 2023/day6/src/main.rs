#[macro_use]
extern crate lazy_static;

use std::fs;

use regex::Regex;

lazy_static! {
    static ref RE_NR: Regex = Regex::new(r"(\d+)").unwrap();
}

#[derive(Debug, PartialEq)]
struct Race {
    time: u64,
    distance: u64,
}
impl Race {
    // Calculates the min and max acceleration times
    fn record_beating_times(&self) -> (u64, u64) {
        // The acceleration times needed to get the given distance in the race is actually the
        // solution of the quadratic equation:
        // t_acc^2 - t * t_acc + d = 0
        // with t beeing the time of the race and d the race distance.
        // This yields two solutions, and all acceleration times in between these numbers would
        // beat the distance.
        let a = 1.0;
        let b = 0.0 - self.time as f64;
        let c = self.distance as f64 + 1.0; // we need to be faster than the record, thus the + 1.0
        let sol_a = (-b + f64::sqrt(b * b - 4.0 * a * c)) / 2.0 * a;
        let sol_b = (-b - f64::sqrt(b * b - 4.0 * a * c)) / 2.0 * a;
        (sol_b.ceil() as u64, sol_a.floor() as u64)
    }
}

fn parse_race_document_pt1(l: &str) -> Vec<Race> {
    let mut lines = l.lines();
    let time_line = lines.next().unwrap();
    assert_eq!(&time_line[0..5], "Time:");
    let times: Vec<u64> = RE_NR
        .captures_iter(time_line)
        .map(|c| c[1].parse::<u64>().unwrap())
        .collect();

    let distance_line = lines.next().unwrap();
    assert_eq!(&distance_line[0..9], "Distance:");
    let distances: Vec<u64> = RE_NR
        .captures_iter(distance_line)
        .map(|c| c[1].parse::<u64>().unwrap())
        .collect();

    times
        .iter()
        .zip(distances.iter())
        .map(|(t, d)| Race {
            time: *t,
            distance: *d,
        })
        .collect()
}

fn parse_race_document_pt2(l: &str) -> Race {
    let mut lines = l.lines();
    let mut time_line = lines.next().unwrap().to_string();
    assert_eq!(&time_line[0..5], "Time:");
    time_line.retain(|c| c != ' ');
    let time: u64 = time_line.split(':').nth(1).unwrap().parse().unwrap();

    let mut distance_line = lines.next().unwrap().to_string();
    assert_eq!(&distance_line[0..9], "Distance:");
    distance_line.retain(|c| c != ' ');
    let distance: u64 = distance_line.split(':').nth(1).unwrap().parse().unwrap();
    Race { time, distance }
}

fn nr_of_ways(min_accel: u64, max_accel: u64) -> u64 {
    max_accel - min_accel + 1
}

fn analyze_race_times_pt1(l: &str) -> u64 {
    let races = parse_race_document_pt1(l);
    let times: Vec<(u64, u64)> = races.iter().map(|r| r.record_beating_times()).collect();
    times
        .iter()
        .map(|(min, max)| nr_of_ways(*min, *max))
        .product()
}

fn analyze_race_times_pt2(l: &str) -> u64 {
    let race = parse_race_document_pt2(l);
    let (min, max) = race.record_beating_times();
    nr_of_ways(min, max)
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1: u64 = analyze_race_times_pt1(&f);
    println!("part1: {pt1}");

    let pt2: u64 = analyze_race_times_pt2(&f);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    const INP1: &str = "Time:      7  15   30
Distance:  9  40  200";

    #[test]
    fn parse_race_test_pt1() {
        let sol = vec![
            Race {
                time: 7,
                distance: 9,
            },
            Race {
                time: 15,
                distance: 40,
            },
            Race {
                time: 30,
                distance: 200,
            },
        ];
        let races = parse_race_document_pt1(&INP1);
        dbg!(&races);
        assert_eq!(races, sol);
    }

    #[test]
    fn test_race_beat_accels() {
        let sol = vec![(2, 5), (4, 11), (11, 19)];
        let races = parse_race_document_pt1(&INP1);
        let times: Vec<(u64, u64)> = races.iter().map(|r| r.record_beating_times()).collect();
        assert_eq!(times, sol);
    }

    #[test]
    fn analyze_race_times_pt1_test() {
        assert_eq!(analyze_race_times_pt1(&INP1), 288)
    }

    #[test]
    fn parse_race_test_pt2() {
        let sol = Race {
            time: 71530,
            distance: 940200,
        };
        let race = parse_race_document_pt2(&INP1);
        dbg!(&race);
        assert_eq!(race, sol);
    }

    #[test]
    fn analyze_race_times_pt2_test() {
        assert_eq!(analyze_race_times_pt2(&INP1), 71503)
    }
}
