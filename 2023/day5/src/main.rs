use std::fs;
use day5::*;

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1 = part1(&f);
    println!("part1: {pt1}");

    //let pt2 = part2(&f);
    let pt2 = part2_3(&f);
    println!("part2: {pt2}");
}

