use rayon::prelude::*;

#[derive(Debug, PartialEq)]
struct Mapping {
    start: i64,
    end: i64,
    offset: i64,
}
impl Mapping {
    fn from_str(s: &str) -> Self {
        //println!("Create mapping from {s}");
        let mut parsed_line = s.split(' ').filter_map(|pts| {
            //println!("pts: {pts}");
            pts.parse::<i64>().ok()
        });
        let dest_start = parsed_line.next().unwrap();
        let source_start = parsed_line.next().unwrap();
        let length = parsed_line.next().unwrap();
        Self {
            start: source_start,
            end: source_start + length,
            offset: dest_start - source_start,
        }
    }

    fn map(&self, inp: i64) -> Option<i64> {
        if inp >= self.start && inp < self.end {
            Some(inp + self.offset)
        } else {
            None
        }
    }
}

#[derive(Debug, Default, PartialEq)]
struct MappingList {
    list: Vec<Mapping>,
}
impl MappingList {
    fn map(&self, inp: i64) -> i64 {
        for m in self.list.iter() {
            if let Some(res) = m.map(inp) {
                return res;
            }
        }
        inp
    }
}

enum CurrentParse {
    Seeds,
    SeedToSoil,
    SoilToFertilizer,
    FertilizerToWater,
    WaterToLight,
    LightToTemp,
    TempToHumidity,
    HumidityToLocation,
}

#[derive(Debug, Default, PartialEq)]
pub struct Almanac {
    seeds: Vec<i64>,
    seed_to_soil_map: MappingList,
    soil_to_fertilizer_map: MappingList,
    fertilizer_to_water_map: MappingList,
    water_to_light_map: MappingList,
    light_to_temp_map: MappingList,
    temp_to_humidity_map: MappingList,
    humidity_to_location_map: MappingList,
}
impl Almanac {
    fn apply_mappings(&self, seed: i64) -> i64 {
        let tmp = self.seed_to_soil_map.map(seed);
        let tmp = self.soil_to_fertilizer_map.map(tmp);
        let tmp = self.fertilizer_to_water_map.map(tmp);
        let tmp = self.water_to_light_map.map(tmp);
        let tmp = self.light_to_temp_map.map(tmp);
        let tmp = self.temp_to_humidity_map.map(tmp);
        self.humidity_to_location_map.map(tmp)
    }

    fn get_locations(&self) -> Vec<i64> {
        self.seeds.iter().map(|&s| self.apply_mappings(s)).collect()
    }

    fn extend_seed_nrs_pt2(&mut self) {
        let mut new_seeds = Vec::new();

        let mut i = 0;
        let mut vec_len = 0;
        while i < self.seeds.len() {
            vec_len += self.seeds[i+1];
            i += 2;
        }
        new_seeds.reserve(vec_len as usize);

        let mut i = 0;
        while i < self.seeds.len() {
            let seed_start = self.seeds[i];
            let seed_len = self.seeds[i + 1];
            for s in seed_start..seed_start + seed_len {
                new_seeds.push(s)
            }
            i += 2;
        }
        self.seeds = new_seeds;
    }
}
impl From<&str> for Almanac {
    fn from(s: &str) -> Self {
        let mut current_parse = CurrentParse::Seeds;
        let mut a = Almanac::default();
        for l in s.lines() {
            if l.is_empty() {
                continue;
            }
            match &l[0..6] {
                "seeds:" => l
                    .split(' ')
                    .filter_map(|s| s.parse::<i64>().ok())
                    .for_each(|seed| a.seeds.push(seed)),
                "seed-t" => current_parse = CurrentParse::SeedToSoil,
                "soil-t" => current_parse = CurrentParse::SoilToFertilizer,
                "fertil" => current_parse = CurrentParse::FertilizerToWater,
                "water-" => current_parse = CurrentParse::WaterToLight,
                "light-" => current_parse = CurrentParse::LightToTemp,
                "temper" => current_parse = CurrentParse::TempToHumidity,
                "humidi" => current_parse = CurrentParse::HumidityToLocation,
                _ => {
                    let m = Mapping::from_str(l);
                    match current_parse {
                        CurrentParse::Seeds => {
                            panic!("Seeds have no mapping")
                        }
                        CurrentParse::SeedToSoil => a.seed_to_soil_map.list.push(m),
                        CurrentParse::SoilToFertilizer => a.soil_to_fertilizer_map.list.push(m),
                        CurrentParse::FertilizerToWater => a.fertilizer_to_water_map.list.push(m),
                        CurrentParse::WaterToLight => a.water_to_light_map.list.push(m),
                        CurrentParse::LightToTemp => a.light_to_temp_map.list.push(m),
                        CurrentParse::TempToHumidity => a.temp_to_humidity_map.list.push(m),
                        CurrentParse::HumidityToLocation => a.humidity_to_location_map.list.push(m),
                    }
                }
            }
        }
        a
    }
}

pub fn part1(f: &str) -> i64 {
    let a = Almanac::from(f);
    //println!("nr_seeds: {}", a.seeds.len());
    *a.get_locations().iter().min().unwrap()
}

pub fn part2(f: &str) -> i64 {
    // This is the brute-force approach. A better way would be to invert the mappings, and then
    // combine a seed range with a inverted mapping to get the minimum that mapping would generate.

    let mut a = Almanac::from(f);
    a.extend_seed_nrs_pt2();
    //println!("nr_seeds: {}", a.seeds.len());
    *a.get_locations().iter().min().unwrap()
}

pub fn part2_2(f: &str) -> i64 {
    let a = Almanac::from(f);
    let mut i = 0;
    let mut min_locs = Vec::new();
    while i < a.seeds.len() {
        //println!("i: {}/{}", i, a.seeds.len());
        let seed_start = a.seeds[i];
        let seed_len = a.seeds[i + 1];
        min_locs.push(
            (seed_start..seed_start + seed_len)
                .map(|s| a.apply_mappings(s))
                .min()
                .unwrap(),
        );
        i += 2;
    }
    *min_locs.iter().min().unwrap()
}

pub fn part2_3(f: &str) -> i64 {
    let a = Almanac::from(f);
    let mut i = 0;
    let mut min_locs = Vec::new();
    while i < a.seeds.len() {
        //println!("i: {}/{}", i, a.seeds.len());
        let seed_start = a.seeds[i];
        let seed_len = a.seeds[i + 1];
        min_locs.push(
            (seed_start..seed_start + seed_len)
                .into_par_iter()
                .map(|s| a.apply_mappings(s))
                .min()
                .unwrap(),
        );
        i += 2;
    }
    *min_locs.iter().min().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    const INP: &str = "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";

    #[test]
    fn mapping_test() {
        let m1 = Mapping::from_str("50 98 2");
        assert_eq!(
            m1,
            Mapping {
                start: 98,
                end: 100,
                offset: -48
            }
        );
        assert_eq!(m1.map(97), None);
        assert_eq!(m1.map(98), Some(50));
        assert_eq!(m1.map(99), Some(51));
        assert_eq!(m1.map(100), None);
        let m2 = Mapping::from_str("52 50 48");
        assert_eq!(
            m2,
            Mapping {
                start: 50,
                end: 98,
                offset: 2
            }
        );
        assert_eq!(m2.map(49), None);
        assert_eq!(m2.map(50), Some(52));
        assert_eq!(m2.map(51), Some(53));
        assert_eq!(m2.map(97), Some(99));
        assert_eq!(m2.map(98), None);

        let m = MappingList { list: vec![m1, m2] };
        assert_eq!(m.map(0), 0);
        assert_eq!(m.map(1), 1);
        assert_eq!(m.map(49), 49);
        assert_eq!(m.map(50), 52);
        assert_eq!(m.map(51), 53);
        assert_eq!(m.map(96), 98);
        assert_eq!(m.map(97), 99);
        assert_eq!(m.map(98), 50);
        assert_eq!(m.map(99), 51);
    }

    #[test]
    fn create_mapping_test() {
        let a = Almanac::from(INP);
        dbg!(&a);
        assert_eq!(a.seeds, vec![79, 14, 55, 13]);
        assert_eq!(
            a.seed_to_soil_map.list[0],
            Mapping {
                start: 98,
                end: 100,
                offset: -48
            }
        );
        assert_eq!(a.seed_to_soil_map.list.len(), 2);
        assert_eq!(a.soil_to_fertilizer_map.list.len(), 3);
        assert_eq!(a.fertilizer_to_water_map.list.len(), 4);
        assert_eq!(a.humidity_to_location_map.list.len(), 2);
    }

    #[test]
    fn seed_mapping_test() {
        let a = Almanac::from(INP);
        assert_eq!(a.get_locations(), vec![82, 43, 86, 35]);
    }

    #[test]
    fn extend_seeds_test() {
        let mut a = Almanac::from(INP);
        a.extend_seed_nrs_pt2();
        assert_eq!(a.seeds.len(), 27);
        assert_eq!(a.seeds[0..3], [79, 80, 81]);
    }
}
