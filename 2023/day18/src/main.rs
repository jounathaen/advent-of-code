use std::{fmt::Debug, fs};

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Direction {
    Right,
    Down,
    Left,
    Up,
}
impl From<char> for Direction {
    fn from(value: char) -> Self {
        match value {
            '0' => Self::Right,
            '1' => Self::Down,
            '2' => Self::Left,
            '3' => Self::Up,
            _ => panic!("invalid direction input: {value}"),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Line {
    start: (isize, isize),
    end: (isize, isize),
    direction: Direction,
}

fn parse_input_pt1(inp: &str) -> Vec<Line> {
    let mut lines = Vec::new();
    let mut cur_position: (isize, isize) = (0, 0);
    for l in inp.lines() {
        let mut parts = l.split(' ');
        let dir = parts.next().unwrap().chars().next().unwrap();
        let length: isize = parts.next().unwrap().parse().unwrap();
        let _color = parts.next().unwrap()[2..=7].to_string();
        let direction = match dir {
            'R' => Direction::Right,
            'L' => Direction::Left,
            'U' => Direction::Up,
            'D' => Direction::Down,
            _ => panic!("Invalid direction: {dir}"),
        };

        let end = match direction {
            Direction::Up => (cur_position.0, cur_position.1 - length),
            Direction::Down => (cur_position.0, cur_position.1 + length),
            Direction::Right => (cur_position.0 + length, cur_position.1),
            Direction::Left => (cur_position.0 - length, cur_position.1),
        };

        lines.push(Line {
            start: cur_position,
            end,
            direction,
        });
        cur_position = end;
    }

    lines
}

fn parse_input_pt2(inp: &str) -> Vec<Line> {
    let mut lines = Vec::new();
    let mut cur_position: (isize, isize) = (0, 0);
    for l in inp.lines() {
        let mut parts = l.split(' ');
        let _dir = parts.next();
        let _meters = parts.next();
        let hex_dir = parts.next().unwrap();
        let length: isize = {
            hex_dir
                .chars()
                .skip(2)
                .take(5)
                .enumerate()
                .map(|(i, h)| (h.to_digit(16).unwrap() as isize) * 16_isize.pow(4 - i as u32))
                .sum()
        };
        let direction = Direction::from(hex_dir.chars().nth(7).unwrap());

        let end = match direction {
            Direction::Up => (cur_position.0, cur_position.1 - length),
            Direction::Down => (cur_position.0, cur_position.1 + length),
            Direction::Right => (cur_position.0 + length, cur_position.1),
            Direction::Left => (cur_position.0 - length, cur_position.1),
        };

        lines.push(Line {
            start: cur_position,
            end,
            direction,
        });
        cur_position = end;
    }

    lines
}

fn analyze_lines(lines: &[Line]) -> isize {
    let mut h_lines: Vec<Line> = lines
        .iter()
        .filter(|l| l.direction == Direction::Right || l.direction == Direction::Left)
        .copied()
        .collect();
    let mut v_lines: Vec<Line> = lines
        .iter()
        .filter(|l| l.direction == Direction::Up || l.direction == Direction::Down)
        .copied()
        .collect();

    h_lines.sort_by_key(|l| l.start.1);
    v_lines.sort_by_key(|l| l.start.0);

    let mut segments: Vec<(isize, isize)> = Vec::with_capacity(h_lines.len() * 2);
    h_lines.dedup_by(|a, b| a.start.1 == b.start.1);
    for i in 0..h_lines.len() - 1 {
        segments.push((h_lines[i].start.1, h_lines[i].start.1 + 1));
        if h_lines[i].start.1 + 1 != h_lines[i + 1].start.1 {
            segments.push((h_lines[i].start.1 + 1, h_lines[i + 1].start.1));
        }
    }
    segments.push((
        h_lines[h_lines.len() - 1].start.1,
        h_lines[h_lines.len() - 1].start.1 + 1,
    ));

    let mut total_vol = 0;

    for (s_start, s_end) in segments {
        let seg_v_lines: Vec<&Line> = v_lines
            .iter()
            .filter(|l| {
                (l.start.1 <= s_start && l.end.1 >= s_start)
                    || (l.end.1 <= s_start && l.start.1 >= s_start)
            })
            .collect();

        let mut dist = 0;

        let mut s = 0;
        'outer: while s < seg_v_lines.len() - 1 {
            let entry_line = seg_v_lines[s];

            let mut exit_index;
            if entry_line.start.1 != s_start
                && entry_line.end.1 != s_start
                && entry_line.start.1 != s_end - 1
                && entry_line.end.1 != s_end - 1
            {
                // Simple Case on entry
                exit_index = s + 1;
            } else {
                let second_entry = seg_v_lines[s + 1];

                if entry_line.direction == second_entry.direction {
                    // Step
                    exit_index = s + 2;
                } else {
                    // U - Shape on entry
                    let add_dist = second_entry.start.0 - entry_line.start.0 + 1;
                    dist += add_dist;
                    s += 2;
                    continue 'outer;
                }
            }

            loop {
                let mut exit_line = seg_v_lines[exit_index];

                if exit_line.start.1 != s_start
                    && exit_line.end.1 != s_start
                    && exit_line.start.1 != s_end - 1
                    && exit_line.end.1 != s_end - 1
                {
                    // Simple Case on exit
                    let add_dist = exit_line.start.0 - entry_line.start.0 + 1;
                    dist += add_dist;
                    s = exit_index + 1;
                    continue 'outer;
                } else {
                    exit_index += 1;
                    let second_exit = seg_v_lines[exit_index];

                    if exit_line.direction == second_exit.direction {
                        // Step
                        exit_line = second_exit;
                        let add_dist = exit_line.start.0 - entry_line.start.0 + 1;
                        dist += add_dist;
                        s = exit_index + 1;
                        continue 'outer;
                    } else {
                        // U - Shape on exit
                        exit_index += 1;
                    }
                }
            }
        }

        let seg_height = s_end - s_start;
        total_vol += dist * seg_height;
    }
    total_vol
}

fn part1(inp: &str) -> isize {
    let lines = parse_input_pt1(inp);
    //dbg!(&lines);
    analyze_lines(&lines)
}

fn part2(inp: &str) -> isize {
    let lines = parse_input_pt2(inp);
    //dbg!(&lines);
    analyze_lines(&lines)
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1 = part1(&f);
    println!("part1: {pt1}");

    let pt2 = part2(&f);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP: &str = r"R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)";

    #[test]
    fn parse_test() {
        let lines = parse_input_pt2(INP);
        dbg!(&lines);
        assert_eq!(
            lines[0],
            Line {
                start: (0, 0),
                end: (461937, 0),
                direction: Direction::Right,
            }
        );
        assert_eq!(
            lines[1],
            Line {
                start: (461937, 0),
                end: (461937, 56407),
                direction: Direction::Down,
            }
        );
    }

    #[test]
    fn pt1_test() {
        assert_eq!(part1(INP), 62);
    }

    #[test]
    fn pt1_test2() {
        // +---+  +--+
        // |   |  |  |
        // |   +--+  |
        // +---------+
        let inp = "R 4 #(123456)
D 2 #(123456)
R 3 #(123456)
U 2 #(123456)
R 3 #(123456)
D 3 #(123456)
L 10 #(123456)
U 3 #(123456)";
        assert_eq!(part1(inp), 11 * 4 - 4);
    }

    #[test]
    fn pt2_test() {
        assert_eq!(part2(INP), 952408144115);
    }
}
