use std::fs;

fn analyze_line_pt1(l: &str) -> u32 {
    let mut nums: Vec<char> = Vec::new();
    let mut l_pos = 0;
    let mut substr = &l[l_pos..];
    while let Some(pos) = substr.find(char::is_numeric) {
        nums.push(substr.chars().nth(pos).unwrap());
        l_pos += pos + 1;
        substr = &l[l_pos..];
    }
    nums[0].to_digit(10).unwrap() * 10 + nums[nums.len() - 1].to_digit(10).unwrap()
}

fn analyze_line_pt2(l: &str) -> u32 {
    let mut nums: Vec<u32> = Vec::new();
    let mut i = 0;
    let chararr: Vec<char> = l.chars().collect();
    'line_loop: while i < chararr.len() {
        match chararr[i] {
            '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                nums.push(chararr[i].to_digit(10).unwrap());
                i += 1;
                continue 'line_loop;
            }
            _ => {}
        }
        let string_nums = [
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
        ];
        for (val, s) in string_nums.iter().enumerate() {
            let adv = s.len();
            if l.len() >= i + adv && l[i..(i + adv)] == **s {
                nums.push(val as u32);
                i += adv - 1;
                continue 'line_loop;
            }
        }
        i += 1;
    }
    nums[0] * 10 + nums[nums.len() - 1]
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1: u32 = f.lines().map(analyze_line_pt1).sum();
    println!("part1: {pt1}");

    let pt2: u32 = f.lines().map(analyze_line_pt2).sum();
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    #[test]
    fn parse_test() {
        let inps = [
            ("1abc2", 12),
            ("pqr3stu8vwx", 38),
            ("a1b2c3d4e5f", 15),
            ("treb7uchet", 77),
        ];
        for (l, n) in inps {
            assert_eq!(analyze_line_pt1(l), n);
        }
        assert_eq!(inps.iter().map(|(s, _)|analyze_line_pt1(s)).sum::<u32>(), 142);
    }

    #[test]
    fn parse_test_pt2() {
        let inps = [
            ("two1nine", 29),
            ("eightwothree", 83),
            ("abcone2threexyz", 13),
            ("xtwone3four", 24),
            ("4nineeightseven2", 42),
            ("zoneight234", 14),
            ("7pqrstsixteen", 76),
        ];
        for (l, n) in inps {
            assert_eq!(analyze_line_pt2(l), n);
        }
        assert_eq!(inps.iter().map(|(s, _)|analyze_line_pt2(s)).sum::<u32>(), 281);
    }
}
