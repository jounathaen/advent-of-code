use std::{collections::VecDeque, fs};

fn hash(inp: &[u8]) -> u8 {
    let mut cur_val: u64 = 0;
    for b in inp {
        if *b == b'\n' {
            continue;
        }
        cur_val += *b as u64;
        cur_val *= 17;
        cur_val %= 256;
    }
    cur_val as u8
}

fn part1(inp: &str) -> usize {
    inp.split(',').map(|s| hash(s.as_bytes()) as usize).sum()
}

fn part2(inp: &str) -> usize {
    let mut boxes: [VecDeque<(&str, usize)>; 256] = [(); 256].map(|_| VecDeque::new());

    for lens_str in inp.split(',') {
        let mut str_split = lens_str.split_inclusive(|c| c == '-' || c == '=');
        let label_str = str_split.next().unwrap();
        let label = &label_str[0..label_str.len() - 1];
        let op = label_str.chars().nth(label_str.len() - 1).unwrap();
        let focussing_power = if let Some(ch) = str_split.next() {
            ch.chars().next()
        } else {
            None
        };
        let lensbox = hash(label.as_bytes()) as usize;

        let content = &mut boxes[lensbox];
        match op {
            '-' => {
                content.retain(|(len, _foc)| len != &label);
            }
            '=' => {
                if let Some((_lens, foc)) = content.iter_mut().find(|(len, _foc)| len == &label) {
                    *foc = focussing_power
                        .expect("Malformed input: - without focussing power")
                        .to_digit(10)
                        .unwrap() as usize;
                } else {
                    content.push_back((
                        label,
                        focussing_power
                            .expect("Malformed input: = without focussing power")
                            .to_digit(10)
                            .unwrap() as usize,
                    ));
                }
            }
            _ => panic!("Invalid operation {op}"),
        }
    }

    boxes
        .iter()
        .enumerate()
        .map(|(j, b)| {
            b.iter()
                .enumerate()
                .map(|(i, (_len, fp))| (j + 1) * (i + 1) * fp)
                .sum::<usize>()
        })
        .sum()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1 = part1(&f);
    println!("part1: {pt1}");

    let pt2 = part2(&f);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP: &str = "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";

    #[test]
    fn hash_test() {
        assert_eq!(hash(b"HASH"), 52);

        let sol = [30, 253, 97, 47, 14, 180, 9, 197, 48, 214, 231];

        for (inp, sol) in INP.split(',').zip(sol.iter()) {
            assert_eq!(hash(inp.as_bytes()), *sol);
        }
    }

    #[test]
    fn pt1_test() {
        assert_eq!(part1(INP), 1320);
    }

    #[test]
    fn pt2_test() {
        assert_eq!(part2(INP), 145);
    }
}
