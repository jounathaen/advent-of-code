#[macro_use]
extern crate lazy_static;

use regex::Regex;
use std::{cmp, collections::HashMap, fs};

lazy_static! {
    static ref RE_WORKFLOW: Regex = Regex::new(r"([[:alpha:]]+)\{([[:alnum:]<>,:]+)\}").unwrap();
    static ref RE_RULE: Regex = Regex::new(r"([[:alpha:]]+)([<>])([0-9]+):([[:alpha:]]+)").unwrap();
    static ref RE_PART: Regex = Regex::new(r"\{x=(\d+),m=(\d+),a=(\d+),s=(\d+)\}").unwrap();
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Operator {
    Less,
    Greater,
}
impl From<char> for Operator {
    fn from(value: char) -> Self {
        match value {
            '<' => Operator::Less,
            '>' => Operator::Greater,
            _ => panic!("invalid operator {value}"),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Category {
    X,
    M,
    A,
    S,
}
impl From<char> for Category {
    fn from(value: char) -> Self {
        match value {
            'x' => Category::X,
            'm' => Category::M,
            'a' => Category::A,
            's' => Category::S,
            _ => panic!("invalid category {value}"),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct Rule<'a> {
    cat: Category,
    op: Operator,
    comparator: i64,
    dest: &'a str,
}
impl<'a> Rule<'a> {
    fn apply(&'a self, part: &Part) -> Option<&'a str> {
        let p_val = match self.cat {
            Category::X => part.x,
            Category::M => part.m,
            Category::A => part.a,
            Category::S => part.s,
        };
        match self.op {
            Operator::Less => {
                if p_val < self.comparator {
                    Some(self.dest)
                } else {
                    None
                }
            }
            Operator::Greater => {
                if p_val > self.comparator {
                    Some(self.dest)
                } else {
                    None
                }
            }
        }
    }

    fn calc_condition_and_inverse_condition(&self) -> (Condition, Condition) {
        let mut cond = Condition::default();
        let mut cond_inv = Condition::default();
        let range = match self.op {
            Operator::Less => (0, self.comparator - 1),
            Operator::Greater => (self.comparator + 1, 4000),
        };
        let range_inv = match self.op {
            Operator::Less => (self.comparator, 4000),
            Operator::Greater => (0, self.comparator),
        };
        match self.cat {
            Category::X => {
                cond.x = vec![range];
                cond_inv.x = vec![range_inv]
            }
            Category::M => {
                cond.m = vec![range];
                cond_inv.m = vec![range_inv]
            }
            Category::A => {
                cond.a = vec![range];
                cond_inv.a = vec![range_inv]
            }
            Category::S => {
                cond.s = vec![range];
                cond_inv.s = vec![range_inv]
            }
        }
        (cond, cond_inv)
    }
}
impl<'a> From<&'a str> for Rule<'a> {
    fn from(value: &'a str) -> Self {
        let caps = RE_RULE.captures(value).unwrap();
        Rule {
            cat: Category::from(caps.get(1).unwrap().as_str().chars().next().unwrap()),
            op: Operator::from(caps.get(2).unwrap().as_str().chars().next().unwrap()),
            comparator: caps.get(3).unwrap().as_str().parse().unwrap(),
            dest: caps.get(4).unwrap().as_str(),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Workflow<'a> {
    rules: Vec<Rule<'a>>,
    else_dest: &'a str,
}
impl<'a> Workflow<'a> {
    fn apply(&self, part: &Part) -> &str {
        for r in self.rules.iter() {
            if let Some(dest) = r.apply(part) {
                return dest;
            }
        }
        self.else_dest
    }

    fn from_line(l: &'a str) -> (&'a str, Self) {
        let caps = RE_WORKFLOW.captures(l).unwrap();
        let dest = caps.get(1).unwrap().as_str();
        let mut rules = Vec::new();
        let splits: Vec<&str> = caps.get(2).unwrap().as_str().split(',').collect();
        for s in splits.iter().take(splits.len() - 1) {
            rules.push((*s).into());
        }

        (
            dest,
            Self {
                rules,
                else_dest: splits[splits.len() - 1],
            },
        )
    }

    fn invert_to_conditions(&self) -> Vec<(&'a str, Condition)> {
        let mut conds = Vec::new();
        let mut pre_cond = Condition::default();
        for r in self.rules.iter() {
            let (cur_cond, inv_cond) = r.calc_condition_and_inverse_condition();
            // Combine with existing pre conditions
            let cur_cond_inters = cur_cond.intersect(&pre_cond);
            conds.push((r.dest, cur_cond_inters));

            // Add current condition inverted to preconditions
            pre_cond = pre_cond.intersect(&inv_cond);
        }
        // Handle else condition
        conds.push((self.else_dest, pre_cond));
        conds
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct Part {
    x: i64,
    m: i64,
    a: i64,
    s: i64,
}
impl From<&str> for Part {
    fn from(s: &str) -> Self {
        let caps = RE_PART.captures(s).unwrap();
        Part {
            x: caps.get(1).unwrap().as_str().parse().unwrap(),
            m: caps.get(2).unwrap().as_str().parse().unwrap(),
            a: caps.get(3).unwrap().as_str().parse().unwrap(),
            s: caps.get(4).unwrap().as_str().parse().unwrap(),
        }
    }
}

type Workflows<'a> = HashMap<&'a str, Workflow<'a>>;

fn parse_inp(inp: &str) -> (Workflows, Vec<Part>) {
    let mut wfs = HashMap::new();
    let mut parts = Vec::new();
    let mut parse_parts = false;
    for l in inp.lines() {
        if l.is_empty() {
            parse_parts = true;
        } else if parse_parts {
            parts.push(Part::from(l));
        } else {
            let (wf_name, wf) = Workflow::from_line(l);
            wfs.insert(wf_name, wf);
        }
    }
    (wfs, parts)
}

fn apply_workflows<'a>(p: &'a Part, wfs: &'a Workflows<'a>) -> &'a str {
    let mut cur_wf = wfs.get("in").unwrap();
    loop {
        let next_wf = cur_wf.apply(p);
        if next_wf == "A" || next_wf == "R" {
            return next_wf;
        }
        cur_wf = wfs.get(next_wf).unwrap();
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Condition {
    x: Vec<(i64, i64)>,
    m: Vec<(i64, i64)>,
    a: Vec<(i64, i64)>,
    s: Vec<(i64, i64)>,
}
impl Condition {
    fn intersect(&self, cond_b: &Condition) -> Condition {
        Condition {
            x: ranges_intersection(&self.x, &cond_b.x),
            m: ranges_intersection(&self.m, &cond_b.m),
            a: ranges_intersection(&self.a, &cond_b.a),
            s: ranges_intersection(&self.s, &cond_b.s),
        }
    }

    fn possible_conditions(&self) -> i64 {
        self.x.iter().map(|(a, b)| b - a + 1).sum::<i64>()
            * self.m.iter().map(|(a, b)| b - a + 1).sum::<i64>()
            * self.a.iter().map(|(a, b)| b - a + 1).sum::<i64>()
            * self.s.iter().map(|(a, b)| b - a + 1).sum::<i64>()
    }
}
impl Default for Condition {
    fn default() -> Self {
        Self {
            x: vec![(1, 4000)],
            m: vec![(1, 4000)],
            a: vec![(1, 4000)],
            s: vec![(1, 4000)],
        }
    }
}

fn ranges_intersection(ranges_a: &Vec<(i64, i64)>, ranges_b: &Vec<(i64, i64)>) -> Vec<(i64, i64)> {
    let mut comb_range = Vec::new();

    if ranges_a.is_empty() || ranges_b.is_empty() {
        return vec![];
    }

    let mut a_iter = ranges_a.iter();
    let mut b_iter = ranges_b.iter();
    let mut a = a_iter.next().unwrap();
    let mut b = b_iter.next().unwrap();

    loop {
        // Non overlapping cases
        if a.1 < b.0 {
            if let Some(a_next) = a_iter.next() {
                a = a_next;
                continue;
            } else {
                break;
            }
        }
        if b.1 < a.0 {
            if let Some(b_next) = b_iter.next() {
                b = b_next;
                continue;
            } else {
                break;
            }
        }
        // Overlap

        let ol_range = (cmp::max(a.0, b.0), cmp::min(a.1, b.1));
        comb_range.push(ol_range);

        if let (Some(a_next), Some(b_next)) = (a_iter.next(), b_iter.next()) {
            a = a_next;
            b = b_next;
        } else {
            break;
        }
    }

    comb_range
}

type WorkflowGraph<'a> = HashMap<&'a str, Vec<(&'a str, Condition)>>;

fn build_workflow_graph<'a>(wfs: &'a Workflows) -> WorkflowGraph<'a> {
    // Storing the nodes and a list of nodes that can link towards them respectively
    let mut wf_graph: WorkflowGraph = HashMap::new();
    for (orig, wf) in wfs.iter() {
        for (dest, cond) in wf.invert_to_conditions() {
            wf_graph.entry(dest).or_default().push((orig, cond));
        }
    }
    wf_graph
}

fn create_workflow_paths<'a>(
    wf_graph: &'a WorkflowGraph,
    node: &'a str,
) -> Vec<Vec<(&'a str, Condition)>> {
    let mut paths = Vec::new();

    let cur_node_neighbours = wf_graph.get(node).unwrap();
    for (neighbour_name, neighbour_cond) in cur_node_neighbours.iter() {
        if *neighbour_name == "in" {
            paths.push(vec![(*neighbour_name, neighbour_cond.clone())]);
            continue;
        }
        let mut sub_paths = create_workflow_paths(wf_graph, neighbour_name);
        for p in sub_paths.iter_mut() {
            p.insert(0, (neighbour_name, neighbour_cond.clone()));
        }
        paths.append(&mut sub_paths);
    }

    paths
}

fn reduce_paths<'a>(paths: &[Vec<(&'a str, Condition)>]) -> Vec<(Vec<&'a str>, Condition)> {
    paths
        .iter()
        .map(|v| {
            let mut path_cond = Condition::default();
            let mut path_names = Vec::new();
            for (step_name, step_cond) in v {
                path_cond = path_cond.intersect(step_cond);
                path_names.push(*step_name);
            }
            (path_names, path_cond)
        })
        .collect()
}

fn part1(wfs: &Workflows, pts: &[Part]) -> i64 {
    pts.iter()
        .map(|p| (p, apply_workflows(p, wfs)))
        .filter_map(|(p, res)| {
            if res == "A" {
                Some(p.x + p.m + p.a + p.s)
            } else {
                None
            }
        })
        .sum()
}

fn part2(wfs: &Workflows) -> i64 {
    let wf_graph = build_workflow_graph(wfs);
    let paths = create_workflow_paths(&wf_graph, "A");
    let reduced_paths = reduce_paths(&paths);
    reduced_paths
        .iter()
        .map(|(_names, cond)| cond.possible_conditions())
        .sum()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let (wfs, pts) = parse_inp(&f);
    let pt1 = part1(&wfs, &pts);
    println!("part1: {pt1}");
    let pt2 = part2(&wfs);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP: &str = "px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}";

    #[test]
    fn parse_rule_test() {
        let r = Rule::from("a<2006:qkq");
        let sol = Rule {
            cat: Category::A,
            op: Operator::Less,
            comparator: 2006,
            dest: "qkq",
        };
        assert_eq!(r, sol);
    }

    #[test]
    fn parse_part_test() {
        let p = Part::from("{x=787,m=2655,a=1222,s=2876}");
        let sol = Part {
            x: 787,
            m: 2655,
            a: 1222,
            s: 2876,
        };
        assert_eq!(p, sol);
    }

    #[test]
    fn parse_workflow_test() {
        let (wf_name, wf) = Workflow::from_line(INP.lines().next().unwrap());
        assert_eq!(wf.rules.len(), 2);
        assert_eq!(wf.else_dest, "rfg");
        assert_eq!(wf_name, "px");
    }

    #[test]
    fn parse_input_test() {
        let (wf_name, wf) = Workflow::from_line(INP.lines().next().unwrap());
        assert_eq!(wf.rules.len(), 2);
        assert_eq!(wf.else_dest, "rfg");
        assert_eq!(wf_name, "px");
    }

    #[test]
    fn apply_part_test() {
        let (wfs, pts) = parse_inp(INP);
        assert_eq!(wfs.len(), 11);
        assert_eq!(pts.len(), 5);
    }

    #[test]
    fn apply_workflows_test() {
        let (wfs, pts) = parse_inp(INP);
        let res: Vec<&str> = pts.iter().map(|p| apply_workflows(p, &wfs)).collect();
        let sols = vec!["A", "R", "A", "R", "A"];
        assert_eq!(res, sols);
    }

    #[test]
    fn pt1_test() {
        let (wfs, pts) = parse_inp(INP);
        assert_eq!(part1(&wfs, &pts), 19114);
    }

    #[test]
    fn ranges_intersection_test() {
        let r1_1 = vec![(0, 5)];
        let r1_2 = vec![(2, 7)];
        let s1 = vec![(2, 5)];
        assert_eq!(ranges_intersection(&r1_1, &r1_2), s1);

        let r2_1 = vec![(0, 5)];
        let r2_2 = vec![(7, 10)];
        let s2 = vec![];
        assert_eq!(ranges_intersection(&r2_1, &r2_2), s2);

        let r3_1 = vec![(0, 5), (8, 11)];
        let r3_2 = vec![(7, 10), (13, 15)];
        let s3 = vec![(8, 10)];
        assert_eq!(ranges_intersection(&r3_1, &r3_2), s3);
    }

    #[test]
    fn invert_conditions_test() {
        let (_wf_name, wf) = Workflow::from_line(INP.lines().next().unwrap());
        let cond = wf.invert_to_conditions();
        let sol = vec![
            (
                "qkq",
                Condition {
                    x: vec![(1, 4000)],
                    m: vec![(1, 4000)],
                    a: vec![(1, 2005)],
                    s: vec![(1, 4000)],
                },
            ),
            (
                "A",
                Condition {
                    x: vec![(1, 4000)],
                    m: vec![(2091, 4000)],
                    a: vec![(2006, 4000)],
                    s: vec![(1, 4000)],
                },
            ),
            (
                "rfg",
                Condition {
                    x: vec![(1, 4000)],
                    m: vec![(1, 2090)],
                    a: vec![(2006, 4000)],
                    s: vec![(1, 4000)],
                },
            ),
        ];
        assert_eq!(cond, sol);
    }

    #[test]
    fn wf_graph_test() {
        let (wfs, _pts) = parse_inp(INP);
        let wf_graph = build_workflow_graph(&wfs);

        assert_eq!(wf_graph.get("in"), None);
        assert_eq!(wf_graph.len(), 10 + 2); // A and R are now also in the graph
        assert_eq!(wf_graph.get("A").unwrap().len(), 9);
    }

    #[test]
    fn part2_test() {
        let (wfs, _pts) = parse_inp(INP);
        assert_eq!(part2(&wfs), 167409079868000);
    }
}
