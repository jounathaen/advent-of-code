#[macro_use]
extern crate lazy_static;

use std::fs;

use regex::Regex;

lazy_static! {
    static ref RE_GAME_ID: Regex = Regex::new(r"Game (\d+)").unwrap();
    static ref RE_DICE: Regex = Regex::new(r"(\d+) (red|green|blue)").unwrap();
}

#[derive(Debug, PartialEq)]
struct Draw {
    red: u32,
    green: u32,
    blue: u32,
}
impl From<&str> for Draw {
    fn from(s: &str) -> Self {
        let mut draw = Self {
            red: 0,
            green: 0,
            blue: 0,
        };
        let caps = RE_DICE.captures_iter(s);
        for c in caps {
            match &c[2] {
                "green" => draw.green = c[1].parse().unwrap(),
                "red" => draw.red = c[1].parse().unwrap(),
                "blue" => draw.blue = c[1].parse().unwrap(),
                _ => panic!("Invalid input"),
            }
        }
        draw
    }
}

#[derive(Debug, PartialEq)]
struct Game {
    id: u32,
    draws: Vec<Draw>,
}
impl Game {
    fn is_possible_pt1(&self) -> bool {
        for d in self.draws.iter() {
            if d.red > 12 || d.green > 13 || d.blue > 14 {
                return false;
            }
        }
        true
    }

    fn power(&self) -> u32 {
        let (mut max_r, mut max_g, mut max_b) = (0, 0, 0);
        for d in self.draws.iter() {
            if d.red > max_r {
                max_r = d.red;
            }
            if d.green > max_g {
                max_g = d.green;
            }
            if d.blue > max_b {
                max_b = d.blue;
            }
        }
        max_r * max_g * max_b
    }
}

fn parse_game_line(l: &str) -> Game {
    let caps = RE_GAME_ID.captures(l).unwrap();
    let id = caps[1].parse().unwrap();

    let mut g = Game { id, draws: vec![] };
    for drawtext in l.split(';') {
        let d = Draw::from(drawtext);
        g.draws.push(d);
    }
    g
}

fn analyze_line_pt1(l: &str) -> u32 {
    let g = parse_game_line(l);
    if g.is_possible_pt1() {
        g.id
    } else {
        0
    }
}

fn analyze_line_pt2(l: &str) -> u32 {
    let g = parse_game_line(l);
    g.power()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1: u32 = f.lines().map(analyze_line_pt1).sum();
    println!("part1: {pt1}");

    let pt2: u32 = f.lines().map(analyze_line_pt2).sum();
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    const INP1: &str = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

    #[test]
    fn parse_test() {
        let sols = [
            Game {
                id: 1,
                draws: vec![
                    Draw {
                        blue: 3,
                        red: 4,
                        green: 0,
                    },
                    Draw {
                        red: 1,
                        green: 2,
                        blue: 6,
                    },
                    Draw {
                        green: 2,
                        blue: 0,
                        red: 0,
                    },
                ],
            },
            Game {
                id: 2,
                draws: vec![
                    Draw {
                        blue: 1,
                        red: 0,
                        green: 2,
                    },
                    Draw {
                        green: 3,
                        blue: 4,
                        red: 1,
                    },
                    Draw {
                        green: 1,
                        blue: 1,
                        red: 0,
                    },
                ],
            },
            Game {
                id: 3,
                draws: vec![
                    Draw {
                        green: 8,
                        blue: 6,
                        red: 20,
                    },
                    Draw {
                        blue: 5,
                        red: 4,
                        green: 13,
                    },
                    Draw {
                        green: 5,
                        red: 1,
                        blue: 0,
                    },
                ],
            },
            Game {
                id: 4,
                draws: vec![
                    Draw {
                        green: 1,
                        red: 3,
                        blue: 6,
                    },
                    Draw {
                        green: 3,
                        red: 6,
                        blue: 0,
                    },
                    Draw {
                        green: 3,
                        blue: 15,
                        red: 14,
                    },
                ],
            },
            Game {
                id: 5,
                draws: vec![
                    Draw {
                        red: 6,
                        blue: 1,
                        green: 3,
                    },
                    Draw {
                        blue: 2,
                        red: 1,
                        green: 2,
                    },
                ],
            },
        ];

        for (l, sol) in INP1.lines().zip(sols.iter()) {
            assert_eq!(&parse_game_line(l), sol);
        }
    }

    #[test]
    fn pt1_test() {
        assert_eq!(INP1.lines().map(analyze_line_pt1).sum::<u32>(), 8)
    }

    #[test]
    fn power_test() {
        let sols = [48, 12, 1560, 630, 36];
        for (l, sol) in INP1.lines().zip(sols.iter()) {
            assert_eq!(&parse_game_line(l).power(), sol);
        }
    }
}
