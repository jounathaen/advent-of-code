use std::fs;

struct Pattern {
    f: Vec<Vec<char>>,
}
impl Pattern {
    fn find_vertical_symmetry(&self) -> Option<usize> {
        'outer: for x in 1..self.f[0].len() {
            for l in self.f.iter() {
                for (a, b) in l[0..x].iter().rev().zip(l[x..].iter()) {
                    if a != b {
                        continue 'outer;
                    }
                }
            }
            return Some(x);
        }
        None
    }

    fn find_horizontal_symmetry(&self) -> Option<usize> {
        'outer: for y in 1..self.f.len() {
            for (a, b) in self.f[0..y].iter().rev().zip(self.f[y..].iter()) {
                if a != b {
                    continue 'outer;
                }
            }
            return Some(y);
        }
        None
    }

    fn find_vertical_symmetry_smudged(&self) -> Option<usize> {
        'outer: for x in 1..self.f[0].len() {
            let mut smudges = 0;
            for l in self.f.iter() {
                for (a, b) in l[0..x].iter().rev().zip(l[x..].iter()) {
                    if a != b {
                        smudges += 1;
                    }
                    if smudges > 1 {
                        continue 'outer;
                    }
                }
            }
            if smudges == 1 {
                return Some(x);
            }
        }
        None
    }

    fn find_horizontal_symmetry_smudged(&self) -> Option<usize> {
        'outer: for y in 1..self.f.len() {
            let mut smudges = 0;
            for (a, b) in self.f[0..y].iter().rev().zip(self.f[y..].iter()) {
                for (a1, b1) in a.iter().zip(b.iter()) {
                    if a1 != b1 {
                        smudges += 1;
                    }
                    if smudges > 1 {
                        continue 'outer;
                    }
                }
            }
            if smudges == 1 {
                return Some(y);
            }
        }
        None
    }
}

fn parse_patterns(inp: &str) -> Vec<Pattern> {
    let mut v = Vec::new();
    let mut current_pattern = Pattern { f: Vec::new() };
    for l in inp.lines() {
        if l.is_empty() {
            v.push(current_pattern);
            current_pattern = Pattern { f: Vec::new() };
        } else {
            current_pattern.f.push(l.chars().collect::<Vec<char>>())
        }
    }
    v.push(current_pattern);
    v
}

fn part1(inp: &str) -> usize {
    let pats = parse_patterns(inp);
    let v_sum: usize = pats.iter().filter_map(|p| p.find_vertical_symmetry()).sum();
    let h_sum: usize = pats
        .iter()
        .filter_map(|p| p.find_horizontal_symmetry())
        .sum();
    h_sum * 100 + v_sum
}

fn part2(inp: &str) -> usize {
    let pats = parse_patterns(inp);
    let v_sum: usize = pats
        .iter()
        .filter_map(|p| p.find_vertical_symmetry_smudged())
        .sum();
    let h_sum: usize = pats
        .iter()
        .filter_map(|p| p.find_horizontal_symmetry_smudged())
        .sum();
    h_sum * 100 + v_sum
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1 = part1(&f);
    println!("part1: {pt1}");

    let pt2 = part2(&f);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP: &str = "#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";

    #[test]
    fn parse_test() {
        let pats = parse_patterns(INP);
        assert_eq!(pats.len(), 2);
        assert_eq!(pats[0].f.len(), 7);
        assert_eq!(pats[1].f.len(), 7);
    }

    #[test]
    fn vertical_symmetry_test() {
        let pats = parse_patterns(INP);
        assert_eq!(pats[0].find_vertical_symmetry(), Some(5));
        assert_eq!(pats[1].find_vertical_symmetry(), None);
    }

    #[test]
    fn horizontal_symmetry_test() {
        let pats = parse_patterns(INP);
        assert_eq!(pats[0].find_horizontal_symmetry(), None);
        assert_eq!(pats[1].find_horizontal_symmetry(), Some(4));
    }

    #[test]
    fn pt1_test() {
        assert_eq!(part1(INP), 405)
    }

    #[test]
    fn vertical_symmetry_smudged_test() {
        let pats = parse_patterns(INP);
        assert_eq!(pats[0].find_vertical_symmetry_smudged(), None);
        assert_eq!(pats[1].find_vertical_symmetry_smudged(), None);
    }

    #[test]
    fn horizontal_symmetry_smudged_test() {
        let pats = parse_patterns(INP);
        assert_eq!(pats[0].find_horizontal_symmetry_smudged(), Some(3));
        assert_eq!(pats[1].find_horizontal_symmetry_smudged(), Some(1));
    }

    #[test]
    fn pt2_test() {
        assert_eq!(part2(INP), 400)
    }
}
