use colored::*;
use rayon::prelude::*;
use std::{
    collections::{HashSet, VecDeque},
    fmt::Debug,
    fs,
};

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
struct Beam {
    cur_pos: (isize, isize),
    dir: Direction,
}

#[derive(Clone)]
struct Contraption {
    f: Vec<char>,
    width: usize,
    height: usize,
    enlighted: HashSet<(isize, isize)>,
}
impl Contraption {
    fn get(&self, pos: (isize, isize)) -> char {
        self.f[pos.1 as usize * self.width + pos.0 as usize]
    }

    fn light_all(&mut self, start_beam: Beam) {
        use Direction::*;

        self.enlighted = HashSet::with_capacity(self.width * self.height);

        let mut beams: VecDeque<Beam> = VecDeque::new();
        let mut visited: HashSet<Beam> = HashSet::with_capacity(self.width * self.height);
        beams.push_back(start_beam);

        while let Some(b) = beams.pop_front() {
            if !self.is_in_contraption(&b.cur_pos) || visited.get(&b).is_some() {
                continue;
            }

            self.light(&b.cur_pos);
            visited.insert(b);

            let new_beams = match self.get(b.cur_pos) {
                '.' | '#' => {
                    let new_pos = match b.dir {
                        Up => (b.cur_pos.0, b.cur_pos.1 - 1),
                        Down => (b.cur_pos.0, b.cur_pos.1 + 1),
                        Left => (b.cur_pos.0 - 1, b.cur_pos.1),
                        Right => (b.cur_pos.0 + 1, b.cur_pos.1),
                    };
                    vec![Beam {
                        cur_pos: new_pos,
                        dir: b.dir,
                    }]
                }
                '/' => {
                    let (mirr_pos, mirr_dir) = match b.dir {
                        Up => ((b.cur_pos.0 + 1, b.cur_pos.1), Right),
                        Down => ((b.cur_pos.0 - 1, b.cur_pos.1), Left),
                        Left => ((b.cur_pos.0, b.cur_pos.1 + 1), Down),
                        Right => ((b.cur_pos.0, b.cur_pos.1 - 1), Up),
                    };
                    vec![Beam {
                        cur_pos: mirr_pos,
                        dir: mirr_dir,
                    }]
                }
                '\\' => {
                    let (mirr_pos, mirr_dir) = match b.dir {
                        Up => ((b.cur_pos.0 - 1, b.cur_pos.1), Left),
                        Down => ((b.cur_pos.0 + 1, b.cur_pos.1), Right),
                        Left => ((b.cur_pos.0, b.cur_pos.1 - 1), Up),
                        Right => ((b.cur_pos.0, b.cur_pos.1 + 1), Down),
                    };
                    vec![Beam {
                        cur_pos: mirr_pos,
                        dir: mirr_dir,
                    }]
                }
                '|' => match b.dir {
                    Up => vec![Beam {
                        cur_pos: (b.cur_pos.0, b.cur_pos.1 - 1),
                        dir: Up,
                    }],
                    Down => vec![Beam {
                        cur_pos: (b.cur_pos.0, b.cur_pos.1 + 1),
                        dir: Down,
                    }],
                    Left | Right => {
                        vec![
                            Beam {
                                cur_pos: (b.cur_pos.0, b.cur_pos.1 + 1),
                                dir: Down,
                            },
                            Beam {
                                cur_pos: (b.cur_pos.0, b.cur_pos.1 - 1),
                                dir: Up,
                            },
                        ]
                    }
                },
                '-' => match b.dir {
                    Left => vec![Beam {
                        cur_pos: (b.cur_pos.0 - 1, b.cur_pos.1),
                        dir: Left,
                    }],
                    Right => vec![Beam {
                        cur_pos: (b.cur_pos.0 + 1, b.cur_pos.1),
                        dir: Right,
                    }],
                    Up | Down => {
                        vec![
                            Beam {
                                cur_pos: (b.cur_pos.0 - 1, b.cur_pos.1),
                                dir: Left,
                            },
                            Beam {
                                cur_pos: (b.cur_pos.0 + 1, b.cur_pos.1),
                                dir: Right,
                            },
                        ]
                    }
                },
                _ => vec![],
            };

            for b in new_beams {
                if self.is_in_contraption(&b.cur_pos) {
                    beams.push_back(b);
                }
            }
        }
    }

    fn is_in_contraption(&self, b: &(isize, isize)) -> bool {
        b.0 >= 0 && b.1 >= 0 && b.0 < self.width as isize && b.1 < self.height as isize
    }
    fn light(&mut self, pos: &(isize, isize)) {
        self.enlighted.insert(*pos);
    }
}
impl From<&str> for Contraption {
    fn from(s: &str) -> Self {
        let width = s.lines().next().unwrap().as_bytes().len();
        let height = s.lines().count();
        let mut f = Vec::with_capacity(width * height);
        s.lines().for_each(|l| f.extend(l.chars()));
        Self {
            f,
            width,
            height,
            enlighted: HashSet::with_capacity(width * height),
        }
    }
}
impl Debug for Contraption {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Field")
            .field("width", &self.width)
            .field("height", &self.height)
            .finish()?;
        writeln!(f, "   x")?;
        write!(f, "   ")?;
        for x in 0..self.width {
            if x <= 9 {
                write!(f, "{x}")?;
            } else {
                write!(f, "v")?;
            }
        }
        writeln!(f)?;
        for y in 0..self.height {
            if y <= 9 {
                if y == 0 {
                    write!(f, "y{y} ")?;
                } else {
                    write!(f, " {y} ")?;
                }
            } else {
                write!(f, " > ")?;
            }
            for x in 0..self.width {
                let pos = (x as isize, y as isize);
                if self.enlighted.get(&pos).is_some() {
                    let s = format!("{}", self.get(pos));
                    write!(f, "{}", s.on_yellow())?;
                } else {
                    write!(f, "{}", self.get(pos))?;
                }
            }
            writeln!(f)?;
        }
        write!(f, "")
    }
}

fn part1(inp: &str) -> usize {
    let mut contr = Contraption::from(inp);
    let start_beam = Beam {
        cur_pos: (0, 0),
        dir: Direction::Right,
    };
    contr.light_all(start_beam);
    contr.enlighted.len()
}

fn part2(inp: &str) -> usize {
    let contr = Contraption::from(inp);
    let mut light_amount = Vec::with_capacity(contr.width * 2 + contr.height * 2);
    light_amount.push(
        (0..contr.width as isize)
            .into_par_iter()
            .map(|x| {
                let mut contr = contr.clone();
                let start_beam = Beam {
                    cur_pos: (x, 0),
                    dir: Direction::Down,
                };
                contr.light_all(start_beam);
                let l1 = contr.enlighted.len();
                let start_beam = Beam {
                    cur_pos: (x, contr.height as isize - 1),
                    dir: Direction::Up,
                };
                contr.light_all(start_beam);
                let l2 = contr.enlighted.len();
                if l1 > l2 {
                    l1
                } else {
                    l2
                }
            })
            .max()
            .unwrap(),
    );
    light_amount.push(
        (0..contr.height as isize)
            .into_par_iter()
            .map(|y| {
                let mut contr = contr.clone();
                let start_beam = Beam {
                    cur_pos: (0, y),
                    dir: Direction::Right,
                };
                contr.light_all(start_beam);
                let l1 = contr.enlighted.len();
                let start_beam = Beam {
                    cur_pos: (contr.width as isize - 1, y),
                    dir: Direction::Down,
                };
                contr.light_all(start_beam);
                let l2 = contr.enlighted.len();
                if l1 > l2 {
                    l1
                } else {
                    l2
                }
            })
            .max()
            .unwrap(),
    );
    *light_amount.iter().max().unwrap()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1 = part1(&f);
    println!("part1: {pt1}");

    let pt2 = part2(&f);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP: &str = r".|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....";

    #[test]
    fn parse_test() {
        let contr = Contraption::from(INP);
        assert_eq!(contr.get((0, 0)), '.');
        assert_eq!(contr.get((0, 1)), '|');
        assert_eq!(contr.get((0, 2)), '.');
        assert_eq!(contr.get((5, 0)), '\\');
    }

    #[test]
    fn mirror_test() {
        let mut contr = Contraption::from(INP);
        let start_beam = Beam {
            cur_pos: (0, 0),
            dir: Direction::Right,
        };
        contr.light_all(start_beam);
        assert_eq!(contr.enlighted.len(), 46);
    }

    #[test]
    fn pt2_test() {
        assert_eq!(part2(INP), 51);
    }
}
