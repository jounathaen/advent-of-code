use std::collections::HashMap;
use std::fmt::Debug;
use std::fs;

#[derive(PartialEq, Clone, Hash, Eq)]
struct Platform {
    f: Vec<Vec<char>>,
}
impl Debug for Platform {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f)?;
        for l in self.f.iter() {
            for c in l.iter() {
                write!(f, "{c}")?;
            }
            writeln!(f)?;
        }
        write!(f, "")
    }
}

impl Platform {
    fn tilt_north(&mut self) {
        for l in 1..self.f.len() {
            for c in 0..self.f[0].len() {
                if self.f[l][c] == 'O' {
                    let mut max_dist = 0;
                    'inner: while max_dist < l {
                        max_dist += 1;
                        if self.f[l - max_dist][c] != '.' {
                            max_dist -= 1;
                            break 'inner;
                        }
                    }
                    if max_dist > 0 {
                        self.f[l - max_dist][c] = 'O';
                        self.f[l][c] = '.';
                    }
                }
            }
        }
    }

    fn tilt_south(&mut self) {
        self.f.reverse();
        self.tilt_north();
        self.f.reverse();
    }

    fn tilt_west(&mut self) {
        for l in 0..self.f.len() {
            //println!("l: {:?}", self.f[l]);
            for c in 0..self.f[0].len() {
                if self.f[l][c] == 'O' {
                    //println!("l: {l}, c: {c}");
                    let mut max_dist = 0;
                    'inner: while max_dist < c {
                        max_dist += 1;
                        if self.f[l][c - max_dist] != '.' {
                            max_dist -= 1;
                            break 'inner;
                        }
                    }
                    //println!("max_dist: {max_dist}");
                    if max_dist > 0 {
                        self.f[l][c - max_dist] = 'O';
                        self.f[l][c] = '.';
                    }
                }
            }
        }
    }

    fn tilt_east(&mut self) {
        for l in 0..self.f.len() {
            for c in (0..self.f[0].len()).rev() {
                if self.f[l][c] == 'O' {
                    let mut max_dist = 0;
                    'inner: while c + max_dist < self.f[0].len() - 1 {
                        max_dist += 1;
                        if self.f[l][c + max_dist] != '.' {
                            max_dist -= 1;
                            break 'inner;
                        }
                    }
                    if max_dist > 0 {
                        self.f[l][c + max_dist] = 'O';
                        self.f[l][c] = '.';
                    }
                }
            }
        }
    }

    fn calc_load(&self) -> usize {
        let mut load = 0;
        for (i, l) in self.f.iter().rev().enumerate() {
            load += l.iter().filter(|&c| c == &'O').count() * (i + 1);
        }
        load
    }

    fn rotate_cycle(&mut self) {
        self.tilt_north();
        self.tilt_west();
        self.tilt_south();
        self.tilt_east();
    }
}
impl From<&str> for Platform {
    fn from(inp: &str) -> Self {
        let mut p = Self { f: Vec::new() };
        for l in inp.lines() {
            p.f.push(l.chars().collect::<Vec<char>>())
        }
        p
    }
}

fn part1(inp: &str) -> usize {
    let mut plat = Platform::from(inp);
    plat.tilt_north();
    plat.calc_load()
}

fn part2(inp: &str, cycles: usize) -> usize {
    let mut plat = Platform::from(inp);
    let mut cache = HashMap::new();
    cache.insert(plat.clone(), 0);
    for i in 1..=cycles {
        plat.rotate_cycle();

        if let Some(x) = cache.get(&plat) {
            // Found a cycle
            let cycle_len = i - x;
            let div = (cycles - i) / (cycle_len);
            let skip_cycles = (cycle_len * div);
            let rem = cycles - i - skip_cycles;
            for _ in 0..rem {
                plat.rotate_cycle();
            }
            break;
        }
        cache.insert(plat.clone(), i);
    }
    plat.calc_load()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1 = part1(&f);
    println!("part1: {pt1}");

    let pt2 = part2(&f, 1_000_000_000);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP: &str = "O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....";

    const SOL: &str = "OOOO.#.O..
OO..#....#
OO..O##..O
O..#.OO...
........#.
..#....#.#
..O..#.O.O
..O.......
#....###..
#....#....";

    #[test]
    fn parse_test() {
        let plat = Platform::from(INP);
        assert_eq!(plat.f[0][0], 'O');
        assert_eq!(plat.f[1][0], 'O');
        assert_eq!(plat.f[0][1], '.');
    }

    #[test]
    fn tilt_test() {
        let mut plat = Platform::from(INP);
        plat.tilt_north();
        let sol = Platform::from(SOL);
        assert_eq!(plat, sol);
    }

    #[test]
    fn load_test() {
        let mut plat = Platform::from(INP);
        plat.tilt_north();
        assert_eq!(plat.calc_load(), 136);
    }

    #[test]
    fn cycle_test() {
        let mut plat = Platform::from(INP);
        //dbg!(&plat);
        plat.tilt_north();
        plat.tilt_west();
        plat.tilt_south();
        plat.tilt_east();
        let sol = Platform::from(
            ".....#....
....#...O#
...OO##...
.OO#......
.....OOO#.
.O#...O#.#
....O#....
......OOOO
#...O###..
#..OO#....",
        );
        assert_eq!(plat, sol);

        plat.rotate_cycle();
        let sol = Platform::from(
            ".....#....
....#...O#
.....##...
..O#......
.....OOO#.
.O#...O#.#
....O#...O
.......OOO
#..OO###..
#.OOO#...O",
        );
        assert_eq!(plat, sol);
    }

    #[test]
    fn part_2_test() {
        assert_eq!(part2(INP, 1000000000), 64);
    }
}
