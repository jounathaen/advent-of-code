use std::{
    collections::{HashSet, VecDeque},
    fmt::Debug,
    fs,
};

#[derive(Copy, Clone, PartialEq)]
enum TileType {
    Vertical,
    Horizontal,
    NeBind,
    NwBind,
    SwBind,
    SeBind,
    Ground,
    Start,
    Outside,
}
impl From<u8> for TileType {
    fn from(b: u8) -> Self {
        match b {
            b'|' => Self::Vertical,
            b'-' => Self::Horizontal,
            b'L' => Self::NeBind,
            b'J' => Self::NwBind,
            b'7' => Self::SwBind,
            b'F' => Self::SeBind,
            b'.' => Self::Ground,
            b'S' => Self::Start,
            _ => panic!("Invalid input char {}", b as char),
        }
    }
}
impl Debug for TileType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match self {
            TileType::Vertical => '║',
            TileType::Horizontal => '═',
            TileType::NeBind => '╚',
            TileType::NwBind => '╝',
            TileType::SwBind => '╗',
            TileType::SeBind => '╔',
            TileType::Ground => ' ',
            TileType::Start => '█',
            TileType::Outside => '░',
        };
        write!(f, "{c}")
    }
}

#[derive(Debug, PartialEq)]
struct Tile {
    t: TileType,
    pos: (usize, usize),
}

#[derive(Clone)]
struct Field {
    f: Vec<TileType>,
    width: usize,
    height: usize,
    start: (usize, usize),
}
impl Field {
    fn get(&self, x: usize, y: usize) -> TileType {
        self.f[y * self.width + x]
    }
    fn set(&mut self, pos: (usize, usize), t: TileType) {
        self.f[pos.1 * self.width + pos.0] = t;
    }
    fn position(&self, index: usize) -> (usize, usize) {
        let x = index % self.width;
        let y = index / self.width;
        (x, y)
    }

    fn get_connected_pipes(&self, pos: (usize, usize)) -> (Tile, Tile) {
        use TileType::*;
        let (x, y) = (pos.0, pos.1);
        let ((new_x1, new_y1), (new_x2, new_y2)) = match self.get(pos.0, pos.1) {
            Vertical => ((x, y - 1), (x, y + 1)),
            Horizontal => ((x - 1, y), (x + 1, y)),
            NeBind => ((x, y - 1), (x + 1, y)),
            NwBind => ((x, y - 1), (x - 1, y)),
            SwBind => ((x - 1, y), (x, y + 1)),
            SeBind => ((x + 1, y), (x, y + 1)),
            Ground | Outside => unreachable!(),
            Start => {
                let pos_tiles = [
                    if y > 0 {
                        match self.get(x, y - 1) {
                            SwBind | SeBind | Vertical => Some((x, y - 1)),
                            _ => None,
                        }
                    } else {
                        None
                    },
                    if x > 0 {
                        match self.get(x - 1, y) {
                            SeBind | NeBind | Horizontal => Some((x - 1, y)),
                            _ => None,
                        }
                    } else {
                        None
                    },
                    if x < self.width {
                        match self.get(x + 1, y) {
                            SwBind | NwBind | Horizontal => Some((x + 1, y)),
                            _ => None,
                        }
                    } else {
                        None
                    },
                    if y < self.height {
                        match self.get(x, y + 1) {
                            NwBind | NeBind | Vertical => Some((x, y + 1)),
                            _ => None,
                        }
                    } else {
                        None
                    },
                ];
                let valid_pos: Vec<(usize, usize)> = pos_tiles.iter().filter_map(|p| *p).collect();
                assert_eq!(valid_pos.len(), 2);
                (valid_pos[0], valid_pos[1])
            }
        };
        (
            Tile {
                t: self.get(new_x1, new_y1),
                pos: (new_x1, new_y1),
            },
            Tile {
                t: self.get(new_x2, new_y2),
                pos: (new_x2, new_y2),
            },
        )
    }

    fn walk_path(&self) -> HashSet<(usize, usize)> {
        let mut visited_pos = HashSet::from([self.start]);
        let mut cur_pos = self.get_connected_pipes(self.start).0.pos;
        while visited_pos.get(&cur_pos).is_none() {
            visited_pos.insert(cur_pos);
            let (pot_pos1, pot_pos2) = self.get_connected_pipes(cur_pos);
            cur_pos = if visited_pos.get(&pot_pos1.pos).is_none() {
                pot_pos1.pos
            } else {
                pot_pos2.pos
            };
        }
        visited_pos
    }

    fn remove_not_loop(&mut self, path: &HashSet<(usize, usize)>) {
        for (i, t) in self.f.iter_mut().enumerate() {
            //let (x, y) = self.position(i);
            let x = i % self.width;
            let y = i / self.width;

            if !path.contains(&(x, y)) {
                *t = TileType::Ground
            }
        }
    }

    fn inflate(&self) -> Self {
        use TileType::*;

        let mut inflated = Self {
            f: Vec::with_capacity(self.width * self.height * 4),
            width: self.width * 2,
            height: self.height * 2,
            start: (self.start.0 * 2, self.start.1 * 2),
        };
        inflated
            .f
            .resize(self.width * self.height * 4, TileType::Ground);

        for (i, t) in self.f.iter().enumerate() {
            let (x, y) = self.position(i);
            let (inf_x, inf_y) = (x * 2, y * 2);
            inflated.set((inf_x, inf_y), *t);
            match t {
                Horizontal => inflated.set((inf_x + 1, inf_y), Horizontal),
                Vertical => inflated.set((inf_x, inf_y + 1), Vertical),
                NeBind => inflated.set((inf_x + 1, inf_y), Horizontal),
                //NwBind => {}
                SwBind => inflated.set((inf_x, inf_y + 1), Vertical),
                SeBind => {
                    inflated.set((inf_x + 1, inf_y), Horizontal);
                    inflated.set((inf_x, inf_y + 1), Vertical)
                }
                Start => {
                    match self.get(x + 1, y) {
                        Horizontal | SwBind | NwBind => {
                            inflated.set((inf_x + 1, inf_y), Horizontal)
                        }
                        _ => {}
                    }
                    match self.get(x, y + 1) {
                        Vertical | NwBind | NeBind => inflated.set((inf_x, inf_y + 1), Vertical),
                        _ => {}
                    }
                }
                _ => {}
            }
        }

        inflated
    }

    fn flood_from_outside(&mut self) {
        let mut candidates = VecDeque::new();
        for x in 0..self.width {
            candidates.push_back((x, 0));
            candidates.push_back((x, self.height - 1));
        }
        for y in 1..self.height - 2 {
            candidates.push_back((0, y));
            candidates.push_back((self.width - 1, y));
        }

        while candidates.len() != 0 {
            let c = candidates.pop_front().unwrap();
            if self.get(c.0, c.1) == TileType::Ground {
                self.set(c, TileType::Outside);
                let (x, y) = c;

                if y > 0 && self.get(x, y - 1) == TileType::Ground {
                    candidates.push_back((x, y - 1))
                }
                if x > 0 && self.get(x - 1, y) == TileType::Ground {
                    candidates.push_back((x - 1, y))
                }
                if x < self.width - 1 && self.get(x + 1, y) == TileType::Ground {
                    candidates.push_back((x + 1, y))
                }
                if y < self.height - 1 && self.get(x, y + 1) == TileType::Ground {
                    candidates.push_back((x, y + 1))
                }
            }
        }
    }

    fn deflate(&self) -> Self {
        let mut deflated = Self {
            f: Vec::with_capacity(self.width * self.height / 4),
            width: self.width / 2,
            height: self.height / 2,
            start: (self.start.0 / 2, self.start.1 / 2),
        };

        for y in 0..deflated.height {
            for x in 0..deflated.width {
                deflated.f.push(self.get(x * 2, y * 2));
            }
        }

        deflated
    }
}
impl From<&str> for Field {
    fn from(s: &str) -> Self {
        let width = s.lines().next().unwrap().as_bytes().len();
        let height = s.lines().count();
        let mut f = Vec::with_capacity(width * height);
        let mut start = None;
        for (i, l) in s.lines().enumerate() {
            l.as_bytes()
                .iter()
                .copied()
                .map(TileType::from)
                .enumerate()
                .for_each(|(j, t)| {
                    if t == TileType::Start {
                        assert!(start.is_none());
                        start = Some((j, i))
                    }
                    f.push(t)
                })
        }
        Self {
            f,
            width,
            height,
            start: start.unwrap(),
        }
    }
}
impl Debug for Field {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Field")
            .field("width", &self.width)
            .field("height", &self.height)
            .field("start", &self.start)
            .finish()?;
        writeln!(f, "\nfield:")?;
        writeln!(f, "   x")?;
        write!(f, "   ")?;
        let nr_cols = if self.width <= 9 { self.width } else { 9 };
        for x in 0..nr_cols {
            write!(f, "{x}")?;
        }
        writeln!(f, "")?;
        for y in 0..self.height {
            if y <= 9 {
                if y == 0 {
                    write!(f, "y{y} ")?;
                } else {
                    write!(f, " {y} ")?;
                }
            } else {
                write!(f, "   ")?;
            }
            for x in 0..self.width {
                write!(f, "{:?}", self.get(x, y))?;
            }
            writeln!(f, "")?;
        }
        write!(f, "")
    }
}

fn count_inside(mut f: Field) -> usize {
    let path = f.walk_path();
    f.remove_not_loop(&path);
    //dbg!(&f);

    let mut new_field = f.inflate();
    new_field.flood_from_outside();
    //dbg!(&new_field);
    let deflated = new_field.deflate();
    //dbg!(&deflated);
    deflated
        .f
        .iter()
        .filter(|&t| *t == TileType::Ground)
        .count()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let field = Field::from(f.as_str());
    let path = field.walk_path();
    println!("part1: {}", path.len() / 2);

    let pt2 = count_inside(field);
    println!("part2: {}", pt2);
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    const INP1: &str = "-L|F7
7S-7|
L|7||
-L-J|
L|-JF";
    const INP2: &str = "..F7.
.FJ|.
SJ.L7
|F--J
LJ...";

    #[test]
    fn parse_test() {
        let field = Field::from(INP1);
        dbg!(&field);
        assert_eq!(field.start, (1, 1));
        assert_eq!(field.get(2, 0), TileType::Vertical);
        assert_eq!(field.get(3, 0), TileType::SeBind);
        assert_eq!(field.get(0, 1), TileType::SwBind);
        assert_eq!(field.get(1, 1), TileType::Start);
        assert_eq!(field.get(2, 1), TileType::Horizontal);

        let field = Field::from(INP2);
        dbg!(&field);
        assert_eq!(field.start, (0, 2));
        assert_eq!(field.get(0, 0), TileType::Ground);
        assert_eq!(field.get(3, 0), TileType::SwBind);
        assert_eq!(field.get(1, 1), TileType::SeBind);
        assert_eq!(field.get(0, 2), TileType::Start);
        assert_eq!(field.get(0, 4), TileType::NeBind);
    }

    #[test]
    fn get_connected_pipes_test() {
        use TileType::*;
        let field = Field::from(INP1);
        dbg!(&field);
        assert_eq!(
            field.get_connected_pipes((2, 1)),
            (
                Tile {
                    t: Start,
                    pos: (1, 1)
                },
                Tile {
                    t: SwBind,
                    pos: (3, 1)
                }
            )
        );
        assert_eq!(
            field.get_connected_pipes((1, 1)),
            (
                Tile {
                    t: Horizontal,
                    pos: (2, 1)
                },
                Tile {
                    t: Vertical,
                    pos: (1, 2)
                }
            )
        );
        assert_eq!(
            field.get_connected_pipes((1, 2)),
            (
                Tile {
                    t: Start,
                    pos: (1, 1)
                },
                Tile {
                    t: NeBind,
                    pos: (1, 3)
                }
            )
        );
    }

    #[test]
    fn walk_path_test() {
        let field = Field::from(INP1);
        dbg!(&field);
        assert_eq!(field.walk_path().len() / 2, 4);
    }
    #[test]
    fn walk_path_test2() {
        let field = Field::from(INP2);
        dbg!(&field);
        assert_eq!(field.walk_path().len() / 2, 8);
    }

    #[test]
    fn count_test() {
        let field = Field::from(INP1);
        assert_eq!(count_inside(field), 1);

        let inp2 = "FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L";

        let field = Field::from(inp2);
        assert_eq!(count_inside(field), 10);
    }
}
