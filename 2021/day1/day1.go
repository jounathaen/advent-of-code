package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func increase_cnt(nrs []int) int {
	prev_nr := nrs[0]
	increases := 0
	for _, i := range nrs[1:] {
		if i > prev_nr {
			increases += 1
		}
		prev_nr = i
	}
	return increases
}

func sum_slice(input ...int) int {
	sum := 0
	for _, i := range input {
		sum += i
	}
	return sum
}

func sliding_increase_cnt(nrs []int) int {
	prev_slice := nrs[0:3]
	increases := 0
	for i := range nrs[1:len(nrs)-2] {
		prev_sum := sum_slice(prev_slice...)
		//fmt.Println("prev_slice: ", prev_slice, " prev_slice sum: ", prev_sum)
		new_slice := nrs[i + 1 : i+4]
		new_sum := sum_slice(new_slice...)
		//fmt.Println("new slice: ", new_slice, " new_slice sum: ", new_sum)

		if new_sum > prev_sum {
			increases += 1
		}
		prev_slice = new_slice
	}
	return increases
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	lines := make([]int, 0)

	for scanner.Scan() {
		line := scanner.Text()
		i, err := strconv.Atoi(line)
		if err != nil {
			log.Fatal(err)
			os.Exit(2)
		}
		lines = append(lines, i)
	}

	increases := increase_cnt(lines)

	fmt.Println("solution 1: ", increases)
	sliding_increases := sliding_increase_cnt(lines)
	fmt.Println("solution 2: ", sliding_increases)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
