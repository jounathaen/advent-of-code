use std::fs;

#[derive(Default, Debug, Clone)]
struct DigitMapping {
    //  aaaa
    // b    c
    // b    c
    //  dddd
    // e    f
    // e    f
    //  gggg
    a: Vec<char>,
    b: Vec<char>,
    c: Vec<char>,
    d: Vec<char>,
    e: Vec<char>,
    f: Vec<char>,
    g: Vec<char>,
}
impl DigitMapping {
    fn new() -> Self {
        let full_set = vec!['a', 'b', 'c', 'd', 'e', 'f', 'g'];
        Self {
            a: full_set.clone(),
            b: full_set.clone(),
            c: full_set.clone(),
            d: full_set.clone(),
            e: full_set.clone(),
            f: full_set.clone(),
            g: full_set,
        }
    }

    fn get_pos_mut(&mut self, pos: char) -> &mut Vec<char> {
        match pos {
            'a' => &mut self.a,
            'b' => &mut self.b,
            'c' => &mut self.c,
            'd' => &mut self.d,
            'e' => &mut self.e,
            'f' => &mut self.f,
            'g' => &mut self.g,
            _ => panic!("invalid char"),
        }
    }

    fn get_pos(&self, pos: char) -> &Vec<char> {
        match pos {
            'a' => &self.a,
            'b' => &self.b,
            'c' => &self.c,
            'd' => &self.d,
            'e' => &self.e,
            'f' => &self.f,
            'g' => &self.g,
            _ => panic!("invalid char"),
        }
    }

    /// Returns (pos, candidate)
    fn get_try_candidates(&self) -> Vec<(char, char)> {
        let mut v_pos = Vec::new();
        for part in ['a', 'b', 'c', 'd', 'e', 'f', 'g'] {
            let part_v = self.get_pos(part);
            if part_v.len() != 1 {
                for c in part_v {
                    v_pos.push((part, *c));
                }
            }
        }
        v_pos
    }

    fn perform_mapping(&self, w: &str) -> [bool; 7] {
        ['a', 'b', 'c', 'd', 'e', 'f', 'g'].map(|c| {
            let pos_vec = self.get_pos(c);
            assert_eq!(pos_vec.len(), 1, "Inambigous Input");
            w.contains(pos_vec[0])
        })
    }

    fn remove_possibility(&mut self, pos: char, c_rem: char) {
        let v = self.get_pos_mut(pos);
        v.retain(|c| *c != c_rem);
    }

    fn add_info(&mut self, w: &str) {
        //  aaaa
        // b    c
        // b    c
        //  dddd
        // e    f
        // e    f
        //  gggg
        let pos = match w.len() {
            2 => &['a', 'b', 'd', 'e', 'g'], // 1
            3 => &['b', 'd', 'e', 'g'],      // 7
            4 => &['a', 'e', 'g'],           // 4
            _ => &[],
        };
        for p in pos {
            for c in w.chars() {
                self.remove_possibility(p, c);
            }
        }
    }

    fn try_out(&self, pos: char, candidate: char) -> Self {
        let mut new_digit = self.clone();
        assert!(new_digit.get_pos_mut(pos).contains(&candidate));
        for part in ['a', 'b', 'c', 'd', 'e', 'f', 'g'] {
            new_digit.get_pos_mut(part).retain(|c| *c != candidate);
        }
        *new_digit.get_pos_mut(pos) = vec![candidate];
        new_digit.reduce();
        new_digit
    }

    fn reduce(&mut self) {
        let mut changes = 0;
        loop {
            for part in ['a', 'b', 'c', 'd', 'e', 'f', 'g'] {
                if self.get_pos_mut(part).len() == 1 {
                    let uniq = self.get_pos_mut(part)[0];
                    for not_part in ['a', 'b', 'c', 'd', 'e', 'f', 'g']
                        .iter()
                        .filter(|c| **c != part)
                    {
                        let not_part_v = self.get_pos_mut(*not_part);
                        if not_part_v.contains(&uniq) {
                            changes += 1;
                        }
                        not_part_v.retain(|c| *c != uniq);
                    }
                }
            }
            if changes == 0 {
                break;
            }
            changes = 0;
        }
    }

    fn is_reduced(&self) -> bool {
        self.a.len() == 1
            && self.b.len() == 1
            && self.c.len() == 1
            && self.d.len() == 1
            && self.e.len() == 1
            && self.f.len() == 1
            && self.g.len() == 1
    }

    fn is_valid(&self) -> bool {
        self.a.len() != 0
            && self.b.len() != 0
            && self.c.len() != 0
            && self.d.len() != 0
            && self.e.len() != 0
            && self.f.len() != 0
            && self.g.len() != 0
    }
}

fn digit_by_num(s: &str) -> Option<u8> {
    match s.len() {
        2 => Some(1),
        3 => Some(7),
        4 => Some(4),
        7 => Some(8),
        _ => None,
    }
}

fn read_digit(inp: [bool; 7]) -> Option<u8> {
    //  aaaa
    // b    c
    // b    c
    //  dddd
    // e    f
    // e    f
    //  gggg
    match inp {
        // A      B      C      D      E      F      G
        [true, true, true, false, true, true, true] => Some(0),
        [false, false, true, false, false, true, false] => Some(1),
        [true, false, true, true, true, false, true] => Some(2),
        [true, false, true, true, false, true, true] => Some(3),
        [false, true, true, true, false, true, false] => Some(4),
        [true, true, false, true, false, true, true] => Some(5),
        [true, true, false, true, true, true, true] => Some(6),
        [true, false, true, false, false, true, false] => Some(7),
        [true, true, true, true, true, true, true] => Some(8),
        [true, true, true, true, false, true, true] => Some(9),
        _ => None,
    }
}

fn digits_to_num(inp: Vec<u8>) -> u64 {
    let mut outp = 0;
    for (i, d) in inp.iter().rev().enumerate() {
        outp += *d as u64 * 10u64.pow(i as u32);
    }
    outp
}

fn deduce_line(line: &str) -> u64 {
    let (inputs, outputs) = split_line(line);
    let testwords: Vec<&str> = inputs.split(' ').collect();
    let mut dm = DigitMapping::new();
    for w in testwords.iter() {
        dm.add_info(w);
    }
    let mapping = try_recursive(&dm, &testwords, 0).unwrap();
    digits_to_num(
        outputs
            .split(' ')
            .map(|w| read_digit(mapping.perform_mapping(w)).unwrap())
            .collect(),
    )
}

fn try_recursive(dm: &DigitMapping, testwords: &Vec<&str>, level: usize) -> Option<DigitMapping> {
    let candidates = dm.get_try_candidates();
    for (pos, c) in candidates {
        let map_try = dm.try_out(pos, c);
        if !map_try.is_valid() {
            continue;
        }
        if map_try.is_reduced() {
            let mut valid = true;
            for w in testwords {
                if read_digit(map_try.perform_mapping(w)).is_none() {
                    valid = false;
                    break;
                }
            }
            if valid {
                return Some(map_try);
            }
        } else {
            if let Some(mapping) = try_recursive(&map_try, testwords, level + 1) {
                return Some(mapping);
            }
        }
    }
    None
}

fn split_line(l: &str) -> (&str, &str) {
    let pos = l.find('|').unwrap();
    (&l[0..pos - 1], &l[pos + 2..])
}

fn count_digits_in_output(s: &str) -> usize {
    s.lines()
        .flat_map(|l| split_line(l).1.split(' ').filter_map(|w| digit_by_num(w)))
        .count()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();
    println!("part1: {}", count_digits_in_output(&f));

    let sum: u64 = f.lines().map(|l| deduce_line(l)).sum();
    println!("part2: {}", sum);
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP1: &str =
        "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

    #[test]
    fn split_line_test() {
        let (l1, l2) = split_line(INP1.lines().next().unwrap());
        assert_eq!(
            l1,
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb"
        );
        assert_eq!(l2, "fdgacbe cefdb cefbgd gcbe");
    }

    #[test]
    fn digit_by_num_test() {
        assert_eq!(digit_by_num("ab"), Some(1));
        assert_eq!(digit_by_num("abcd"), Some(4));
        assert_eq!(digit_by_num("asdfg"), None);
    }
    #[test]
    fn count_valid_output() {
        assert_eq!(count_digits_in_output(INP1), 26);
    }

    #[test]
    fn test_remove_poss() {
        let mut dm = DigitMapping::new();
        dm.remove_possibility('a', 'b');
        dm.remove_possibility('a', 'c');
        dm.remove_possibility('a', 'd');
        assert_eq!(dm.a, ['a', 'e', 'f', 'g']);
    }

    #[test]
    fn test_info() {
        let line = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
cdfeb fcadb cdfeb cdbaf";
        assert_eq!(deduce_line(line), 5353);

        let mut lines = INP1.lines();
        assert_eq!(deduce_line(lines.next().unwrap()), 8394);
        assert_eq!(deduce_line(lines.next().unwrap()), 9781);
        assert_eq!(deduce_line(lines.next().unwrap()), 1197);
        assert_eq!(deduce_line(lines.next().unwrap()), 9361);
        assert_eq!(deduce_line(lines.next().unwrap()), 4873);
        assert_eq!(deduce_line(lines.next().unwrap()), 8418);
        assert_eq!(deduce_line(lines.next().unwrap()), 4548);
        assert_eq!(deduce_line(lines.next().unwrap()), 1625);
        assert_eq!(deduce_line(lines.next().unwrap()), 8717);
        assert_eq!(deduce_line(lines.next().unwrap()), 4315);
    }
}
