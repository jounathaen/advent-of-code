use std::fs;
//use std::collections::Vec;

#[derive(Debug, Eq, PartialEq)]
enum Movement {
    Forward(i64),
    Down(i64),
    Up(i64),
}
impl From<&str> for Movement {
    fn from(s: &str) -> Self {
        let pos = s.find(' ').unwrap();
        let i: i64 = s[(pos + 1)..].parse().unwrap();
        match &s[0..pos] {
            "forward" => Movement::Forward(i),
            "down" => Movement::Down(i),
            "up" => Movement::Up(i),
            _ => panic!("Invalid input {}", s),
        }
    }
}

struct Submarine {
    pos: i64,
    depth: i64,
    aim: i64,
}
impl Submarine {
    fn new() -> Self {
        Self {
            pos: 0,
            depth: 0,
            aim: 0,
        }
    }

    fn move_sub(&mut self, mov: Movement) {
        match mov {
            Movement::Forward(x) => self.pos += x,
            Movement::Down(x) => self.depth += x,
            Movement::Up(x) => self.depth -= x,
        }
    }

    fn move_aim(&mut self, mov: Movement) {
        match mov {
            Movement::Forward(x) => {
                self.pos += x;
                self.depth += self.aim * x
            }
            Movement::Down(x) => self.aim += x,
            Movement::Up(x) => self.aim -= x,
        }
    }
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();
    let mut sub = Submarine::new();
    for l in f.lines() {
        sub.move_sub(Movement::from(l));
    }
    println!("part1: {}", sub.pos * sub.depth);
    let mut sub = Submarine::new();
    for l in f.lines() {
        sub.move_aim(Movement::from(l));
    }
    println!("part2: {}", sub.pos * sub.depth);
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    //#[test]
    //fn pt1_test() {
    //let testinp = [
    //35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
    //576,
    //];
    //assert_eq!(part1(&testinp, 5), 127)
    //}

    const INP1: &str = "forward 5
down 5
forward 8
up 3
down 8
forward 2";

    #[test]
    fn parse_test() {
        let movments: Vec<Movement> = INP1.lines().map(|l| Movement::from(l)).collect();
        assert_eq!(movments[0], Movement::Forward(5));
        assert_eq!(movments[1], Movement::Down(5));
        assert_eq!(movments[2], Movement::Forward(8));
        assert_eq!(movments[3], Movement::Up(3));
    }

    #[test]
    fn test_movement() {
        let mut sub = Submarine::new();
        for l in INP1.lines() {
            sub.move_sub(Movement::from(l));
        }
        assert_eq!(sub.pos, 15);
        assert_eq!(sub.depth, 10);
    }

    #[test]
    fn test_aim_movement() {
        let mut sub = Submarine::new();
        for l in INP1.lines() {
            sub.move_aim(Movement::from(l));
        }
        assert_eq!(sub.pos, 15);
        assert_eq!(sub.depth, 60);
    }
}
